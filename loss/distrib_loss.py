import torch
from torch import nn
from torch.functional import Tensor


def rms_loss(
    x, y, loss_metric=nn.KLDivLoss(reduction="batchmean", log_target=True)
) -> Tensor:
    """
    KL divergence loss across the target: x, and the prediction: y
    for the RMS of each quantity
    """
    lx = torch.sqrt(
        torch.mean(x ** 2, dim=(-2, -3)) - torch.mean(x, dim=(-2, -3))**2
    ).log()
    ly = torch.sqrt(
        torch.mean(y ** 2, dim=(-2, -3)) - torch.mean(y, dim=(-2, -3))**2
    ).log()
    return loss_metric(ly, lx)


def distrib_loss(
    x, y, loss_metric=nn.KLDivLoss(reduction="batchmean", log_target=True)
) -> Tensor:
    """
    KL divergence loss across the target: x, and the prediction: y
    for the distributions of x, and y as a function of the position in the
    channel
    """
    profile_x = torch.mean(x, dim=(-2, -3)).log()
    profile_y = torch.mean(y, dim=(-2, -3)).log()

    return loss_metric(profile_y, profile_x)


def squared_loss(
    x, y, loss_metric=nn.KLDivLoss(reduction="batchmean", log_target=True)
) -> Tensor:
    xsquared = torch.mean(torch.square(x), dim=(-2, -3)).log()
    ysquared = torch.mean(torch.square(y), dim=(-2, -3)).log()
    return loss_metric(ysquared, xsquared)


# def gradient_loss(
#     x,
#     y,
#     coord,
#     loss_metric=nn.KLDivLoss(reduction="batchmean", log_target=True)
# ) -> Tensor:
#     gradx = torch.gradient(x, spacing=coord, edge_order=2)
#     grady = torch.gradient(y, spacing=coord, edge_order=2)
#     loss_out = loss_metric(grady[0].log(), gradx[0].log())
#     for gx, gy in zip(gradx[1:], grady[1:]):
#         loss_out = loss_out + loss_metric(gy.log(), gx.log())
#     return loss_out

def squared_gradient_loss(
    x,
    y,
    coord,
    loss_metric=nn.KLDivLoss(reduction="batchmean", log_target=True)
) -> Tensor:

    gradx = torch.gradient(x, spacing=coord, edge_order=2)
    grady = torch.gradient(y, spacing=coord, edge_order=2)

    sqgrdx = [torch.mean(gx.square(), dim=(-2, -3)).log() for gx in gradx]
    sqgrdy = [torch.mean(gy.square(), dim=(-2, -3)).log() for gy in grady]

    loss_i = loss_metric(sqgrdy[0], sqgrdx[0])
    loss_j = loss_metric(sqgrdy[1], sqgrdx[1])
    loss_k = loss_metric(sqgrdy[2], sqgrdx[2])

    return loss_i+loss_j+loss_k
