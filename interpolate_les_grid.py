import numpy as np
import interp as tc
from glob import glob
from utils.data_loading import read_p_thermo
from utils.utils import load_fortran_to_c
import os
from os.path import join
from natsort import natsorted
from typing import Tuple
from tqdm import tqdm


def read_temperature(path: str, shape: Tuple[float, float, float]) -> np.array:
    """
    Read the 3D LES temperature fields from the path and formats it into the
    right shape in 3D
    """

    R_AIR: float = 287.058
    p_th_path = path.split(".lata.0.RHO")[0]
    path_rho = p_th_path + ".lata.0.RHO"
    rho = load_fortran_to_c(path_rho, shape)

    p_th = read_p_thermo(p_th_path, dtype=np.float32)
    T = p_th/(R_AIR*rho)
    # print(p_th_path.split("/")[-1], path_rho.split("/")[-1])

    return T.reshape(*shape)


# ndns = (384, 384, 266)
nles = (48, 48, 52)

path = "/mnt/beegfs/home/zatout/solaire/unet/dataset"
path_to = os.path.join(path, "interpolated", "Ac03-As03_c4_c2")
coord_from = natsorted(
    glob(os.path.join(path, "dns_lata_1.sauv.lata.grid_geom2.coord*")))
coord_from = [np.fromfile(c) for c in coord_from]
coord_from_64 = [c.astype("float64") for c in coord_from]

coord_to = natsorted(glob(os.path.join(path, "coord_aim", "coord_elem*")))
coord_to = [np.fromfile(c) for c in coord_to]
coord_to_64 = [c.astype("float64") for c in coord_to]

les_3 = np.meshgrid(*coord_to, indexing="ij")
les_3 = np.concatenate([ss.ravel()[np.newaxis]
                       for ss in les_3]).T.astype("float64")


images = natsorted(
    glob(os.path.join(path, "Ac03-As03_c4_c2", "dns_lata_*.sauv.lata.0.RHO")))
# p_th = natsorted(glob(os.path.join(path, "Ac03-As03_c4_c2", "dns_lata_*.sauv")))

for i in tqdm(images):
    T = read_temperature(i, nles).astype("float64")
    spline = tc.csp3d_open(*coord_from_64, *nles, T.reshape(-1))
    T_interp = tc.eval_all_spline_3d(
        les_3, *coord_from_64, nles, T.reshape(-1), spline).reshape(*nles)
    number = i.split("dns_lata_")
    number = int(number[1].split(".sauv")[0])
    path_save = join(path_to, f"T_amd{number}.npy")
    T_interp.tofile(path_save)
