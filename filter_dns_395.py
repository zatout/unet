import numpy as np
import interp as tc
import torch
from glob import glob
from utils.data_loading import read_p_thermo, middle_point
from utils.blur import convolve_box_kernel_regular
from utils.utils import load_fortran_to_c

ndns = (768, 512, 512)
nles = (96, 64, 100)

path = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_395/snapshots/"
path_filtered_dns = path+"../../"+"les_395/"
path_coords = glob(path+"dns_lata_*.sauv.lata.grid_geom2.coord*")
path_coords.sort()
path_p_thermo = sorted(glob(f"{path}/dns_lata_*.sauv"))
p_thermo = [read_p_thermo(c) for c in path_p_thermo]
path_rho = sorted(glob(path+"dns_lata_*.sauv.lata.0.RHO"))

coord_vertex = [np.fromfile(g, dtype=np.float32) for g in path_coords]
coord = [middle_point(c) for c in coord_vertex]
steps = [np.diff(c) for c in coord]
steps = [torch.tensor(steps[0][0]), torch.tensor(steps[1][0]), steps[2]]
steps[-1] = torch.from_numpy(steps[-1])
coord_les = [np.linspace(i.min(), i.max(), n)
             for i, n in zip(coord, nles)]
steps_les = [np.linspace(i.min(), i.max(), n, retstep=True)
             for i, n in zip(coord, nles)]
steps_les = [s[1] for s in steps_les]
les_3 = np.meshgrid(*coord_les, indexing="ij")
les_3 = np.concatenate([ss.ravel()[np.newaxis]
                       for ss in les_3]).T
dns_3 = np.meshgrid(*coord, indexing="ij")
dns_3 = np.concatenate([ss.ravel()[np.newaxis]
                       for ss in dns_3]).T
coord_float64 = [c.astype("float64") for c in coord]

filter_size = 3
filter_size_torch = torch.tensor(filter_size)

for p_th, rho_path in zip(p_thermo, path_rho):
    rho = load_fortran_to_c(rho_path, ndns)
    r_air = 287.058
    T_dns = p_th/(rho*r_air)
    T_dns = T_dns.numpy().astype(np.float64)
    spline = tc.csp3d_open(*coord_float64, *ndns, T_dns)

    T_dns_on_les_field = tc.eval_all_spline_3d(
        les_3, *coord_float64, ndns, T_dns, spline).reshape(*nles)

    filter_size = 3
    filter_size_torch = torch.tensor(filter_size)
    T_filtered_LES = convolve_box_kernel_regular(
        T_dns_on_les_field, filter_size=filter_size_torch, spacing=steps_les
    )
    number = rho_path.split("dns_lata_")
    number = int(number[1].split(".sauv")[0])
    T_filtered_LES.tofile(path_filtered_dns+f"T_{number}.npy")
