from typing import List, Tuple, Union, Callable, Type
import os
import argparse
import logging
from pathlib import Path
from tqdm import tqdm

from torchvision.transforms import Compose
from torchvision.utils import make_grid
import torch
from torch import nn

import numpy as np

from torch.utils.data import random_split, DataLoader

from utils.model import Unet
from utils.crop import PairCrop
from utils.flip import RandomHorizontalFlip, RandomVerticalFlip, \
    RandomDepthFlip
from utils.rotate import Random90Rotation
from utils.data_loading import (
    DnsTemperatureLoader, DnsTemperatureLoaderInterpolated, load_dns, load_les
)

from loss.distrib_loss import (
    distrib_loss,
    rms_loss,
    squared_loss,
    squared_gradient_loss
)
from loss.general_purpose_loss import general_purpose_distrib_loss

from datahandling.error_quantification import (
    compute_rms, compute_gradient_error
)

from tensorboardX import SummaryWriter
from time import gmtime, strftime

from scipy.interpolate import RegularGridInterpolator


def get_args():
    parser = argparse.ArgumentParser(
        description='Train the UNet on images and target masks')
    parser.add_argument('--epochs', '-e', metavar='E',
                        type=int, default=150, help='Number of epochs')
    parser.add_argument('--batch-size', '-b', dest='batch_size',
                        metavar='B', type=int, default=10, help='Batch size')
    parser.add_argument('--learning-rate', '-l', metavar='LR', type=float,
                        default=1e-2,
                        help='Learning rate', dest='lr')
    parser.add_argument('--normalize', '-n',
                        action="store_false", help='Normalizing images')
    parser.add_argument('--train', '-t', dest='train', type=int,
                        default=40,
                        help='Percent of the data that '
                        'is used as validation (0-100)')
    parser.add_argument('--validation', '-v', dest='val', type=int,
                        default=10,
                        help='Number of validation snapshots'
                        )
    parser.add_argument(
        '--ndns-train', '-dns-train', nargs="+", type=int,
        default=[384, 384, 266],
        help="Number of points in the DNS training mesh"
    )
    parser.add_argument(
        '--nles-train', nargs="+", type=int,
        default=[48, 48, 50],
        help="Number of points in the LES training mesh"
    )
    parser.add_argument(
        '--ndns-test', nargs="+", type=int,
        default=[786, 512, 512],
        help="Number of points in the DNS test mesh"
    )
    parser.add_argument(
        '--nles-test', '-les-test', nargs="+", type=int,
        default=[96, 64, 100],
        help="Number of points in the LES test mesh"
    )
    parser.add_argument(
        '--crop-size', '-cs', nargs="+", type=int,
        default=[16, 16, 16],
        help="Size of the crops"
    )
    parser.add_argument(
        '--gpu-training', '-gpu',
        action='store_true'
    )
    parser.add_argument(
        '--re-tau-train', '-re-train',
        type=float,
        default=180,
        help="Friction reynolds in training"
    )
    parser.add_argument(
        '--re-tau-test', '-re-test',
        type=float,
        default=395,
        help="Friction reynolds in test"
    )
    parser.add_argument(
        '--distribution-loss', '-d-l',
        action="store_true",
    )
    parser.add_argument(
        '--rms-loss', '-rms-l',
        action="store_true",
    )
    parser.add_argument(
        '--squared-loss', '-sq-l',
        action="store_true",
    )
    parser.add_argument(
        '--no-mse-loss', '-n-mse-l',
        action="store_false"
    )

    return parser.parse_args()


def get_transforms(
    crop: bool = True,
    flip: bool = True,
    rotate: bool = True,
    *,
    crop_size: Union[int, Tuple] = (16, 16, 16),
    flip_prob: float = 1/3.,
    n_rot: int = 4
) -> Type[Compose]:
    """
    Function that creates transforms: Cropping, flipping and rorating a 3D
    image
    Parameters:
    ----------
    crop: bool
        Whether to crop or not
    flip:bool
        Whether to flip or not
    rotate
        Whether to rotate or not

    kwargs:
    crop_size: Union[int, Tuple]
        Size of the crop taken in 3D
    flip_prob: float
        The probability to flip your image
    n_rot: int
        Number of maximum rotations to take
    Returns:
    -------
    scripted: Callable
    """
    transform_list = []
    if crop:
        transform_list.append(PairCrop(crop_size))
    if flip:
        transform_list.append(RandomHorizontalFlip(flip_prob))
        transform_list.append(RandomVerticalFlip(flip_prob))
        transform_list.append(RandomDepthFlip(flip_prob))
    if rotate:
        transform_list.append(Random90Rotation(n_rot))

    return Compose(transform_list)


def evaluate(net, dataloader, device, criterion, n_val):
    net.eval()
    num_val_batches = n_val
    validation_loss = 0

    # iterate over the validation set
    for batch in tqdm(dataloader,
                      total=num_val_batches,
                      desc='Validation',
                      unit='batch',
                      leave=False):
        dns, les = batch
        # move images and labels to correct device and type
        dns = dns.to(device)
        les = les.to(device)

        with torch.no_grad():
            prediction = net(les) + les
            validation_loss += criterion(dns, prediction).item()
    net.train()
    return validation_loss, prediction, dns


def write_summary_loss_and_images(phase, loss, images, writer, crop_size, epoch):
    grid_x = make_grid(images[..., crop_size[0]//2, :, :])
    grid_y = make_grid(images[..., :, crop_size[1]//2, :])
    grid_z = make_grid(images[..., crop_size[1]//2])
    writer.add_scalar(phase+"loss",
                      loss, epoch)
    writer.add_image(phase+"image_i",
                     grid_x, epoch)
    writer.add_image(phase+"image_j",
                     grid_y, epoch)
    writer.add_image(phase+"image_k",
                     grid_z, epoch)


def write_summary_images(phase, images, writer, crop_size, epoch):
    grid_x = make_grid(images[..., crop_size[0]//2, :, :])
    grid_y = make_grid(images[..., :, crop_size[1]//2, :])
    grid_z = make_grid(images[..., crop_size[1]//2])
    writer.add_image(phase+"image_i",
                     grid_x, epoch)
    writer.add_image(phase+"image_j",
                     grid_y, epoch)
    writer.add_image(phase+"image_k",
                     grid_z, epoch)


def test_net(
    net, dns_path, dns_shape, les_path, les_shape, tmin, tmax, savepath
):
    T_dns, coord_dns = load_dns(dns_path, dns_shape)
    T_les_interpolated, coord_les, steps_les = load_les(
        les_path, coord_dns, les_shape)
    T_dns_rms = compute_rms(T_dns)
    T_dns_gradient = np.gradient(T_dns, *coord_dns, edge_order=2)

    # T_les_rms = compute_rms(T_les_interpolated)
    #
    # T_les_gradient_lin_interp = np.gradient(
    #     T_les_interpolated, *coord_les, edge_order=2)
    # error_profile_lin_interp = np.mean(
    #     np.abs(T_dns - T_les_interpolated)/tmax, (0, 1))
    # rms_profile_lin_interp = np.abs(T_dns_rms - T_les_rms)
    # gradient_profile_lin_interp = [np.mean(
    #     np.abs(tgdns - tgles)/T_dns_gradient.max(), (0, 1))
    #     for tgdns, tgles in zip(T_dns_gradient, T_les_gradient_lin_interp)]
    T_les_torch = torch.from_numpy(
        T_les_interpolated).unsqueeze(0).unsqueeze(0)
    reconstructed = (T_les_torch + net(T_les_torch)) * (tmax-tmin) + tmin

    rms_reconstructed = compute_rms(reconstructed[0, 0].numpy())
    gradient_reconstructed = np.gradient(
        reconstructed[0, 0].numpy(), *coord_dns, edge_order=2)
    gradient_reconstructed = np.gradient(
        reconstructed[0, 0].numpy(), *coord_dns, edge_order=2)

    error_profile = np.mean(
        np.abs(T_dns - reconstructed[0, 0].numpy()), (0, 1))/T_dns.max()
    rms_profile = np.abs(T_dns_rms - rms_reconstructed)/T_dns_rms.max()
    gradient_profile = np.array([
        np.mean(
            np.abs(tgdns - tgles)
        )/T_dns_gradient.max()
        for tgdns, tgles in zip(T_dns_gradient, gradient_reconstructed)
    ])
    error_profile.tofile(os.path.join(savepath, "error_profile"))
    rms_profile.tofile(os.path.join(savepath, "rms_profile"))
    gradient_profile.tofile(os.path.join(savepath, "gradient_profile"))


def train(
        model,
        epochs=300,
        transforms=None,
        criterion=nn.MSELoss(),
        batch_size: int = 40,
        learning_rate: float = 1e-2,
        scheduler_coef=.8,
        n_train: int = 40,
        n_val: int = 10,
        n_test: int = 15,
        device: str = "cpu",
        crop_size: Union[Tuple[int, int, int], int] = (16, 16, 16),
        images_dir: Union[str, List, Path] =
        "/home/zatout/Documents/These_yanis/Reunion_10012022/",
        save_checkpoint: bool = True,
        dir_checkpoint="./checkpoint",
        *,
        ndns_train=(384, 384, 266),
        nles_train=(48, 48, 50),
        ndns_test=(768, 512, 512),
        nles_test=(96, 64, 100),
        tmin=586,
        tmax=293,
        re_tau_train=180,
        re_tau_test=395,
        suffix=None
):
    current_time = strftime("%H:%M:%S_%d-%m-%Y", gmtime())
    if suffix:
        writer = SummaryWriter(os.path.join(
            "u_net_training_losses", current_time+suffix))
        dir_checkpoint = os.path.join(dir_checkpoint, suffix)
    else:
        writer = SummaryWriter(os.path.join(
            "u_net_training_losses", current_time))
    dns_dir = os.path.join(images_dir, "dns_180")
    les_dir = os.path.join(images_dir, "les_interpolated_180")

    test_dns_dir = os.path.join(images_dir, "dns_395")
    test_les_dir = os.path.join(images_dir, "les_interpolated_395")

    dir_checkpoint = Path(os.path.join(dir_checkpoint, current_time))

    optim = torch.optim.Adam(model.parameters(), lr=learning_rate)
    # Scheduler is to use every 10 epochs to reduce the
    # Learning rate by 20%
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim,
        lr_lambda=lambda epoch: scheduler_coef
    )

    dataset = DnsTemperatureLoaderInterpolated(
        dir_dns=dns_dir,
        dir_les=les_dir,
        transform=transforms,
        device=device,
        re_tau=re_tau_train,
        nles=nles_train,
        normalize=True,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )
    train_set, val_set = random_split(
        dataset, [n_train, n_val],
        generator=torch.Generator().manual_seed(42)
    )

    loader_args = dict(batch_size=batch_size, num_workers=0, pin_memory=True)

    train_loader = DataLoader(train_set, shuffle=True, **loader_args)
    val_loader = DataLoader(val_set, shuffle=False,
                            **loader_args)

    logging.info(f'''Starting training:
            Epochs:          {epochs}
            Batch size:      {batch_size}
            Learning rate:   {learning_rate}
            Training size:   {n_train}
            Validation size: {n_val}
            Checkpoints:     {save_checkpoint}
            Images crops:    {crop_size}
        ''')

    net = model
    net.to(device)
    global_step = 0
    for epoch in range(1, epochs+1):
        net.train()
        epoch_loss = 0
        with tqdm(
            total=n_train, desc=f'Epoch {epoch}/{epochs}', unit='Snapshots'
        ) as pbar:
            for batch in train_loader:
                optim.zero_grad()
                dns, les = batch

                dns = dns.to(device=device)
                les = les.to(device=device)

                prediction = net(les)

                dns_recon = les + prediction

                loss = criterion(dns, dns_recon)
                loss.backward()

                optim.step()

                pbar.update(dns.shape[0])
                global_step += 1
                epoch_loss += loss.item()
                pbar.set_postfix(**{'loss (batch)': loss.item()})

        # After each epoch, we log the loss, and some images
        writer.add_scalar(
            "Training/loss",
            epoch_loss,
            epoch
        )
        write_summary_images(
            "Training/NN", dns_recon, writer, crop_size, epoch)

        write_summary_images(
            "Training/TRUE", dns, writer, crop_size, epoch)

        validation_loss, prediction_eval, dns_eval = evaluate(
            net, val_loader, device, criterion, n_val)

        writer.add_scalar("Evaluation/loss", validation_loss, epoch)
        write_summary_images(
            "Evaluation/NN/", prediction_eval, writer, crop_size,
            epoch)

        write_summary_images(
            "Evaluation/TRUE/", dns_eval, writer, crop_size,
            epoch)

        if epoch % 10 == 9:
            scheduler.step()
        if epoch % 15 == 9:
            if save_checkpoint:
                Path(dir_checkpoint).mkdir(
                    parents=True, exist_ok=True)
                torch.save(net.state_dict(), str(dir_checkpoint /
                           'checkpoint_epoch{}.pth'.format(epoch)))
                logging.info(f'Checkpoint {epoch} saved!')
    # test_scores = test_net(net, test_dns_dir, test_les_dir)

    # Setting it to the max number
    epoch = epochs
    if save_checkpoint:
        Path(dir_checkpoint).mkdir(
            parents=True, exist_ok=True)
        torch.save(net.state_dict(), str(dir_checkpoint /
                   'checkpoint_epoch{}.pth'.format(epoch)))
        logging.info('Last epoch saved')


def main():
    args = get_args()
    kernel_size = 3
    padding = "same"
    maxpool_kernel = 2
    crop_size = args.crop_size
    flip_prob = 1/3.
    n_rot = 4
    lr = args.lr
    epochs = args.epochs
    batch_size = args.batch_size
    scheduler_coef = .8
    model = Unet(kernel_size, padding, maxpool_kernel)
    n_train = args.train
    n_val = args.val
    n_test = 15
    if args.gpu_training:
        device = "cuda" if torch.cuda.is_available() else "cpu"
    else:
        device = "cpu"
    images_dir = Path(
        "/mnt/beegfs/home/zatout/solaire/unet"
    )
    save_checkpoint = True
    checkpoint_dir = "./checkpoint"
    transforms = get_transforms(
        crop=True, flip=True, rotate=True,
        crop_size=crop_size,
        flip_prob=flip_prob,
        n_rot=n_rot
    )

    def criterion(x, y): return general_purpose_distrib_loss(
        x, y, mse=args.no_mse_loss,
        distrib=args.distribution_loss,
        rms=args.rms_loss,
        squared=args.squared_loss
    )
    suffix = ""

    if not args.no_mse_loss:
        suffix += "_mse"
    if args.rms_loss:
        suffix += "_rms"
    if args.squared_loss:
        suffix += "_squared"

    train(
        model,
        epochs=epochs,
        transforms=transforms,
        criterion=criterion,
        batch_size=batch_size,
        learning_rate=lr,
        scheduler_coef=scheduler_coef,
        n_train=n_train,
        n_val=n_val,
        n_test=n_test,
        device=device,
        crop_size=crop_size,
        images_dir=images_dir,
        save_checkpoint=save_checkpoint,
        dir_checkpoint=checkpoint_dir,
        ndns_train=args.ndns_train,
        nles_train=args.nles_train,
        ndns_test=args.ndns_test,
        nles_test=args.nles_test,
        tmin=586,
        tmax=293,
        re_tau_train=args.re_tau_train,
        re_tau_test=args.re_tau_test,
        suffix=suffix
    )


if __name__ == "__main__":
    main()
