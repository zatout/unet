from scipy.interpolate import CubicSpline
from glob import glob
import utils.training as ut
from os import makedirs as mkdir
import argparse

from torch.utils.data import random_split, DataLoader
import os
import numpy as np
import torch

from utils.model import Unet
from utils.data_loading import DnsTemperatureLoaderCoarse
from utils.transforms import (get_transforms_no_y_sampling,
                              get_transforms_no_depth_flip,
                              get_transforms_and_coord)

from utils.data_loading import middle_point


def get_args():
    parser = argparse.ArgumentParser(
        description='Train the UNet on images and target masks')
    parser.add_argument('--epochs', '-e', metavar='E',
                        type=int, default=150, help='Number of epochs')
    parser.add_argument('--steps-per-epoch', '-spe', default=500, type=int)
    parser.add_argument('--y-crop', '-y', action="store_true",
                        help="Crop in the y direction")
    parser.add_argument('--no-save', '-nos', action="store_true",
                        help="Whether to save your model after training or not")
    parser.add_argument('--seed', '-s', type=int, default=0)
    parser.add_argument('--reproducibility', action="store_true")
    parser.add_argument('--crop-training', '-ct', action='store_true',
                        help='Training with (16, 16, 16) crops')
    parser.add_argument('--no-y-crop', action='store_true',
                        help='Training with (16, 16, X) with X growing')
    parser.add_argument('--statistics', action='store_true',
                        help="Learn only on statistical profiles")
    parser.add_argument('-lr', '--learning-rate', type=float, default=1e-2,
                        help='Beggining learning rate')
    parser.add_argument('-lrs', '--lr-scheduler', type=float, default=0.8,
                        help='Value for lr scheduler')

    parser.add_argument('--post-treatement', action='store_true',
                        help='Whether to do post treatement.')
    return parser.parse_args()


def train_no_y_crop(
    epochs=150, steps_per_epoch=100, save=True, reproducibility=True,
    seed=0
):
    device = "cuda"
    import socket
    if socket.gethostname() == "compta68":
        images_dir = "/home/zatout/Documents/These_yanis/Reunion_10012022/"
    else:
        images_dir = "/mnt/beegfs/home/zatout/solaire/unet"

    dns_dir = os.path.join(images_dir, "dns_coarse_180/48_52_48")
    les_dir = os.path.join(images_dir, "les_180")

    tmin, tmax = 293, 586
    dataset = DnsTemperatureLoaderCoarse(
        dir_dns=dns_dir,
        dir_les=les_dir,
        transform=None,
        device="cuda",
        re_tau=180,
        nles=[48, 48, 52],
        normalize=True,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )

    seed = seed
    if reproducibility:
        train_set, val_set = random_split(
            dataset, [13, 4],
            generator=torch.Generator().manual_seed(seed)
        )
        print(f"seed: {seed}")
        import random
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
    else:
        train_set, val_set = random_split(dataset, [13, 4])

    train_batch_size = 13
    validation_batch_size = 4
    loader_args_train = dict(batch_size=train_batch_size, num_workers=0)
    loader_args_val = dict(batch_size=validation_batch_size, num_workers=0)

    train_loader = DataLoader(train_set, shuffle=True, **loader_args_train)
    val_loader = DataLoader(val_set, shuffle=False, **loader_args_val)
    for batch in train_loader:
        batch = tuple(batch)
        break
    for val in val_loader:
        val = tuple(val)
        break

    kernel_size = 3
    padding = "same"
    maxpool_kernel = 2
    flip_prob = 1/3.
    n_rot = 4
    net = Unet(kernel_size, padding, maxpool_kernel,
               last_act="tanh").float().to("cuda")
    transforms = get_transforms_no_y_sampling(
        crop=True, flip=True, rotate=True, crop_size=16, flip_prob=flip_prob,
        n_rot=n_rot)
    device = "cuda"
    lr = learning_rate = 1e-2
    optim = torch.optim.Adam(net.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim, lr_lambda=lambda epoch: .8)

    net.to(device)

    tloss, vloss, net = ut.curriculum_learning_growing_domain(
        train_loader, val_loader, net, lr, scheduler, steps_per_epoch,
        transforms, device, criterion=ut.RMSELoss, epochs=epochs, crop_size=16,
        save=save, additional_save_string="_no_y_sampling")
    return tloss, vloss, net


def train_growing_sampling_space(
    epochs=150, steps_per_epoch=100, save=True, reproducibility=True,
    seed=0
):
    device = "cuda"
    import socket
    if socket.gethostname() == "compta68":
        images_dir = "/home/zatout/Documents/These_yanis/Reunion_10012022/"
    else:
        images_dir = "/mnt/beegfs/home/zatout/solaire/unet"

    dns_dir = os.path.join(images_dir, "dns_coarse_180/48_52_48")
    les_dir = os.path.join(images_dir, "les_180")

    tmin, tmax = 293, 586
    dataset = DnsTemperatureLoaderCoarse(
        dir_dns=dns_dir,
        dir_les=les_dir,
        transform=None,
        device="cuda",
        re_tau=180,
        nles=[48, 48, 52],
        normalize=True,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )

    seed = seed
    if reproducibility:
        train_set, val_set = random_split(
            dataset, [13, 4],
            generator=torch.Generator().manual_seed(seed)
        )
        print(f"seed: {seed}")
        import random
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
    else:
        train_set, val_set = random_split(dataset, [13, 4])

    train_batch_size = 13
    validation_batch_size = 4
    loader_args_train = dict(batch_size=train_batch_size, num_workers=0)
    loader_args_val = dict(batch_size=validation_batch_size, num_workers=0)

    train_loader = DataLoader(train_set, shuffle=True, **loader_args_train)
    val_loader = DataLoader(val_set, shuffle=False, **loader_args_val)
    for batch in train_loader:
        batch = tuple(batch)
        break
    for val in val_loader:
        val = tuple(val)
        break

    kernel_size = 3
    padding = "same"
    maxpool_kernel = 2
    flip_prob = 1/3.
    n_rot = 4
    net = Unet(kernel_size, padding, maxpool_kernel,
               last_act="tanh").float().to("cuda")
    transforms = get_transforms_no_depth_flip(
        crop=True, flip=True, rotate=True, crop_size=16, flip_prob=flip_prob,
        n_rot=n_rot)
    device = "cuda"
    lr = learning_rate = 1e-2
    optim = torch.optim.Adam(net.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim, lr_lambda=lambda epoch: .8)

    net.to(device)

    tloss, vloss, net = ut.curriculum_learning_growing_domain(
        train_loader, val_loader, net, lr, scheduler, steps_per_epoch,
        transforms, device, criterion=ut.RMSELoss, epochs=epochs, crop_size=16,
        save=save, additional_save_string="_growing_domain_sampling")
    return tloss, vloss, net


def train_on_statistics_only(
    epochs=150, steps_per_epoch=100, reproducibility=True, seed=0, lr=1e-2, lr_scheduler_value=.95
):
    # device = "cuda"
    device = "cuda" if torch.cuda.is_available() else "cpu"
    import socket
    if socket.gethostname() == "compta68":
        images_dir = "/home/zatout/Documents/These_yanis/Reunion_10012022/"
        path = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_180/"
        dns_dir = os.path.join(images_dir, "dns_coarse_180/48_52_48")
        les_dir = os.path.join(images_dir, "les_180")
    else:
        images_dir = "/mnt/beegfs/home/zatout/solaire/unet"
        path = "/mnt/beegfs/home/zatout/solaire/unet/dns_180"
        dns_dir = "/mnt/beegfs/projects/solaire/unet/dns_coarse_180/48_52_48_nsamples45"
        les_dir = "/mnt/beegfs/projects/solaire/unet/les_180/48_52_48_nsamples45"
    # /mnt/beegfs/projects/solaire/unet/dns_coarse_180/48_52_48_nsamples45
    # print(os.listdir(images_dir))
    # print(f"length: {len(os.listdir(dns_dir))}, {os.listdir(dns_dir)}")
    # print(f"length: {len(os.listdir(les_dir+'/48_52_48'))}, {os.listdir(les_dir+'/48_52_48')}")
    ndns = (384, 384, 266)
    nles = (48, 48, 52)

    path_coords = sorted(glob(path+"/dns_lata_1.sauv.lata.grid_geom2.coord*"))
    coord_vertex = [np.fromfile(g, dtype=np.float32) for g in path_coords]
    coord = [middle_point(c) for c in coord_vertex]
    steps = [np.diff(c) for c in coord]
    steps = [torch.tensor(steps[0][0]), torch.tensor(steps[1][0]), steps[2]]
    cs = CubicSpline(np.arange(ndns[-1]+1)/ndns[-1], coord_vertex[-1])
    coord_face_interp = cs(np.arange(nles[-1]+1)/nles[-1])
    coord_vertex_les = [
        np.linspace(coord_vertex[0].min(), coord_vertex[0].max(), nles[0]+1),
        np.linspace(coord_vertex[1].min(), coord_vertex[1].max(), nles[1]+1),
        coord_face_interp
    ]

    coord_les = [
        middle_point(c) for c in coord_vertex_les
    ]

    tmin, tmax = 293, 586

    dns_dir_val = os.path.join(images_dir, "dns_coarse_180/48_52_48")
    les_dir_val = os.path.join(images_dir, "les_180/48_52_48")
    # dns_dir = os.path.join(images_dir, "dns_180/extra/coarse")
    # les_dir = os.path.join(images_dir, "dns_180/extra/les")

    def normalize(t, tmin, tmax):
        return ((t - tmin)/(tmax-tmin) - 0.5)*2

    def unnormalize(t, tmin, tmax):
        return ((t / 2) + 0.5) * (tmax - tmin) + tmin
    dataset = DnsTemperatureLoaderCoarse(
        dir_dns=dns_dir,
        dir_les=les_dir,
        transform=None,
        device=device,
        re_tau=180,
        nles=[48, 48, 52],
        normalize=normalize,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )

    otherdataset = DnsTemperatureLoaderCoarse(
        dir_dns=dns_dir_val,
        dir_les=les_dir_val,
        transform=None,
        device=device,
        re_tau=180,
        nles=[48, 48, 52],
        normalize=normalize,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )
    seed = seed
    if reproducibility:
        train_set, _ = random_split(
            dataset, [45, 0],
            generator=torch.Generator().manual_seed(seed)
        )
        _, val_set = random_split(
            otherdataset, [0, 17], generator=torch.Generator().manual_seed(seed))
        print(f"seed: {seed}")
        import random
        random.seed(seed)
        np.random.seed(seed)
        # torch.manual_seed(seed)
    else:
        train_set, val_set = random_split(dataset, [45, 0])
        _, val_set = random_split(otherdataset, [0, 17])
    train_batch_size = 45
    validation_batch_size = 17

    loader_args_train = dict(batch_size=train_batch_size, num_workers=0)
    loader_args_val = dict(batch_size=validation_batch_size, num_workers=0)

    train_loader = DataLoader(train_set, shuffle=True, **loader_args_train)
    val_loader = DataLoader(val_set, shuffle=False, **loader_args_val)
    for batch in train_loader:
        batch = tuple(batch)
        break
    for val in val_loader:
        val = tuple(val)
        break
    kernel_size = 3
    padding = "same"
    maxpool_kernel = 2
    flip_prob = 1/3.
    n_rot = 4
    net = Unet(kernel_size, padding, maxpool_kernel,
               last_act="tanh").float().to(device)
    transforms = get_transforms_and_coord(crop=True, flip=True, rotate=True, crop_size=16,
                                          flip_prob=flip_prob, coord_elem=coord_les[-1], coord_face=coord_vertex_les[-1], n_rot=n_rot)
    learning_rate = lr
    optim = torch.optim.Adam(net.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim, lr_lambda=lambda epoch: lr_scheduler_value)
    net.to(device)

    coord_elem = coord_les[-1]
    coord_face = coord_vertex_les[-1]
    t, v, n = ut.curriculum_learning_growing_domain_gradients(
        train_loader,
        val_loader,
        coord_elem,
        coord_face,
        net,
        lr,
        scheduler,
        steps_per_epoch,
        transforms,
        device,
        criterion=ut.loss_grad_rms,
        epochs=epochs,
        crop_size=16,
        mlr=.8,
        save=True,
        debug=False
    )
    return t, v, n


if __name__ == "__main__":
    tmin, tmax = 293, 586
    args = get_args()
    epochs = args.epochs
    steps_per_epoch = args.steps_per_epoch
    seed = args.seed
    reproducibility = args.reproducibility
    lr = args.learning_rate
    lr_scheduler_value = args.lr_scheduler
    post_treatement = args.post_treatement
    if args.no_save:
        save = False
    else:
        save = True
    print(save)
    if not args.y_crop:
        additional_save_string = "_no_y_sampling"
        print("We don't crop in the wall-normal direction")
        tloss, vloss, net = train_no_y_crop(
            epochs=epochs, steps_per_epoch=steps_per_epoch, save=save,
            reproducibility=reproducibility, seed=seed
        )
    elif args.statistics:
        additional_save_string = f"statistics_epochs_{epochs}_spe_{steps_per_epoch}_reproducibility_{reproducibility}"
        tloss, vloss, net = train_on_statistics_only(
            epochs=epochs, steps_per_epoch=steps_per_epoch,
            reproducibility=reproducibility, seed=seed, lr=lr,
            lr_scheduler_value=lr_scheduler_value
        )
        tloss = np.array(tloss)
        vloss = np.array(vloss)
    else:
        additional_save_string = "_growing_domain_sampling"
        print("We crop in the wall-normal direction")
        tloss, vloss, net = train_growing_sampling_space(
            epochs=epochs, steps_per_epoch=steps_per_epoch, save=save,
            reproducibility=reproducibility, seed=seed
        )
    if save:
        from time import strftime, gmtime
        current_time = strftime("%S-%M-%H_%d-%m-%Y", gmtime())
        mkdir(f"./checkpoint/{additional_save_string}_{current_time}", exist_ok=True)
        save_path_train = f"./checkpoint/{additional_save_string}_{current_time}/train_loss.npy"
        save_path_val = f"./checkpoint/{additional_save_string}_{current_time}/valid_loss.npy"
        np.array(tloss).tofile(save_path_train)
        np.array(vloss).tofile(save_path_val)
        save_path = \
            f"./checkpoint/{additional_save_string}_{current_time}/weights_.pth"
        torch.save(net.state_dict(), save_path)
    if post_treatement:
        from utils.post_treatement import post_treat_on_validation
        post_treat_on_validation(
            net,
            tloss,
            vloss,
            seed=seed,
            path_prefix="./checkpoint"
        )
