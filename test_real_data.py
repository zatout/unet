from typing import Tuple, Any, Union, Callable
import socket
from pathlib import Path
import os
from torch.utils.data import DataLoader, Dataset
import torch
from utils.utils import load_fortran_to_c
from utils.data_loading import (
    read_p_thermo,
    normalize,
    unnormalize
)
import numpy as np
from utils.model import Unet
from glob import glob
from tqdm import tqdm
from natsort import natsorted

SHAPE_DATA = (48, 48, 52)
R_AIR: float = 287.058
TMIN, TMAX = 293, 586


def read_temperature(path: str, shape: Tuple[float, float, float]) -> np.array:
    """
    Read the 3D LES temperature fields from the path and formats it into the
    right shape in 3D
    """
    p_th_path = path.split(".lata.0.RHO")[0]
    path_rho = p_th_path + ".lata.0.RHO"
    rho = load_fortran_to_c(path_rho, shape)

    p_th = read_p_thermo(p_th_path, dtype=np.float32)
    T = p_th/(R_AIR*rho)

    return T.reshape(*SHAPE_DATA)


class TestLoader(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        normalize: Callable[
            [np.typing.ArrayLike, float, float],
            np.typing.ArrayLike
        ] = lambda x, tmin, tmax: (x-tmin)/(tmax-tmin),
        device: Union[str, torch.device] = "cpu",
        r_air: float = R_AIR,
        tmin: float = TMIN,
        tmax: float = TMAX,
        *,
        shape: Tuple[float, float, float] = SHAPE_DATA
    ) -> None:
        self.dir_dns = dir_dns
        self.images = natsorted(
            glob(os.path.join(dir_dns, "dns_lata_*.sauv.lata.0.RHO")))
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmin = tmin
        self.tmax = tmax
        self.shape = shape

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index: Any):
        imagepath = self.images[index]
        example = read_temperature(imagepath, self.shape)
        return torch.from_numpy(self.normalize(example, self.tmin, self.tmax)).float().to(self.device).unsqueeze(0)


class TestLoaderTemperature(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        normalize: Callable[
            [np.typing.ArrayLike, float, float],
            np.typing.ArrayLike
        ] = lambda x, tmin, tmax: (x-tmin)/(tmax-tmin),
        device: Union[str, torch.device] = "cpu",
        r_air: float = R_AIR,
        tmin: float = TMIN,
        tmax: float = TMAX,
        *,
        shape: Tuple[float, float, float] = SHAPE_DATA
    ) -> None:
        self.dir_dns = dir_dns
        self.images = natsorted(glob(os.path.join(dir_dns, "T_*")))
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmin = tmin
        self.tmax = tmax
        self.shape = shape

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index: Any):
        imagepath = self.images[index]
        example = np.fromfile(imagepath).reshape(*SHAPE_DATA)
        return torch.from_numpy(self.normalize(example, self.tmin, self.tmax)).float().to(self.device).unsqueeze(0) # , imagepath.split("/")[-1].split(".npy")[-1]


@torch.no_grad()
def evaluate(net, images, tmin=TMIN, tmax=TMAX):
    recon = net(images) + images
    return unnormalize(recon, tmin, tmax)


def main() -> None:
    kernel_size = 3
    padding = "same"
    maxpool_kernel = 2
    batch_size = 20
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    net = Unet(
        kernel_size,
        padding,
        maxpool_kernel,
        last_act="tanh"
    ).float().to(device)

    path_amd = str(None)
    path_scalesim = str(None)

    path_amd = "/mnt/beegfs/home/zatout/solaire/unet/dataset/interpolated/Ac03-As03_c4_c2"
    path_scalesim = "/mnt/beegfs/home/zatout/solaire/unet/dataset/interpolated/Sc1-_c4_c2"
    net.load_state_dict(
        torch.load(
            "/mnt/beegfs/home/zatout/solaire/unet/submissions/checkpoint/"
            "100_steps_per_epochs/"
            "weights_growing_domain_sampling_38-17-16_03-03-2023_100_steps_per_epoch.pth"
        )
    )
    net.eval()

    dataloader_amd = TestLoaderTemperature(
        dir_dns=path_amd,
        normalize=normalize,
        device=device,
        r_air=R_AIR,
        tmin=TMIN,
        tmax=TMAX,
        shape=SHAPE_DATA
    )

    dataloader_scalesim = TestLoaderTemperature(
        path_scalesim,
        normalize=normalize,
        device=device,
        r_air=R_AIR,
        tmin=TMIN,
        tmax=TMAX,
        shape=SHAPE_DATA
    )

    amd = DataLoader(
        dataloader_amd,
        batch_size=batch_size,
        num_workers=0
    )
    scalesim = DataLoader(
        dataloader_scalesim,
        batch_size=batch_size,
        num_workers=0

    )
    print(len(amd))
    print(len(scalesim))
    amd_reconstruction = []
    scalesim_reconstruction = []
    print("Reconstruction Functional model")
    for x in tqdm(amd):
        e = evaluate(net, x, TMIN, TMAX).cpu().numpy()
        amd_reconstruction.append(e)

    print("Reconstruction Structural model")
    for x in tqdm(scalesim):
        e = evaluate(net, x, TMIN, TMAX).cpu().numpy()
        scalesim_reconstruction.append(e)

    amd_reconstruction = np.array(amd_reconstruction).reshape(-1, *SHAPE_DATA)
    scalesim_reconstruction = np.array(
        scalesim_reconstruction).reshape(-1, *SHAPE_DATA)

    os.makedirs("/mnt/beegfs/home/zatout/solaire/unet/submissions/tests", exist_ok=True)
    os.makedirs("/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/amd", exist_ok=True)
    os.makedirs("/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/scalesim", exist_ok=True)

    for num, img in enumerate(amd_reconstruction):
        print(f"/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/amd/T_amd_reconstruction_nn_{num}.npy")
        img.tofile(f"/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/amd/T_amd_reconstruction_nn_{num}.npy")

    for num, img in enumerate(scalesim_reconstruction):
        print(f"/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/scalesim/T_scalesim_reconstruction_nn_{num}.npy")
        img.tofile(f"/mnt/beegfs/home/zatout/solaire/unet/submissions/tests/scalesim/T_scalesim_reconstruction_nn_{num}.npy")


if __name__ == '__main__':
    main()
