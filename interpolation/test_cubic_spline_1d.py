from .tricubic import csp1d, NATSPLN
import numpy as np
import matplotlib.pyplot as plt


def testmain1d():
    n = 12

    x, f, s = np.empty(n, dtype=np.double), np.empty(
        n, dtype=np.double), np.empty(n, dtype=np.double),

    x[0] = 0.
    # /*
    # * To get proper first derivative at center of splash, use a point close
    # * to edge
    # */
    x[1] = 0.1
    for i in range(2, n):
        x[i] = i-1.

    for i in range(n):
        r = np.pi*np.sqrt(x[i]*x[i])/2.
        if r != 0.:
            yy = np.sin(r)/r
        else:
            yy = 1.
        f[i] = yy

    s = csp1d(x, f, n, NATSPLN, NATSPLN)
    k = 0
    out = np.empty(101, dtype=np.float64)
    xxx = np.empty(101, dtype=np.float64)
    for j in range(101):
        xx = j / 10.
        while k > 0 and xx < x[k]:
            k -= 1
        while k < n-2 and xx > x[k+1]:
            k += 1
        dx = x[k+1]-x[k]
        b = (xx - x[k])/dx
        a = 1.-b
        g = dx*dx/6.
        c = g*(a*a*a - a)
        d = g*(b*b*b - b)
        spl = a*f[k] + b*f[k+1] + c*s[k] + d*s[k+1]
        r = np.pi*np.sqrt(xx*xx)/2.
        # if r != 0.:
        #     e = np.sin(r)/r
        # else:
        #     e = 1
        out[j] = spl
        xxx[j] = xx

    return x, f, xxx, out


x, f, xxx, out = testmain1d()

plt.figure()
plt.plot(x, f, "-o")
plt.plot(xxx, out, "-x")

plt.show()
