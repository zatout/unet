from numba import double, njit, prange, cfunc
import numba
from numba.types import int64
import numpy as np
from numpy import float64

# double = numba.types.double
intc = int64

MAXLIM = .99e30

NATSPLN = 1.e30

csp1d_types = double[:](
    double[:],
    double[:],
    intc,
    double,
    double,
)


@cfunc(csp1d_types)
def csp1d(x, y, n, ypl, ypr):
    """
       Purpose

         Calculate cubic spline of a tabulated smooth function of one variable

       Reference

         Adapted from http://oceancolor.gsfc.nasa.gov/staff/norman/
            seawifs_image_cookbook/faux_shuttle/spline.c
            by Norman Kuring 31 Mar 1999:

       Comments from Kuring

         This code is based on the cubic spline interpolation code presented in
         Numerical Recipes in C: The Art of Scientific Computing by
         William H. Press, Brian P. Flannery, Saul A. Teukolsky, and
         William T. Vetterling, 1988 (and 1992 for the 2nd edition).

         This version assumes zero-offset arrays instead of the unit-offset
         arrays suggested by the authors.  You may style me rebel or conformist
         depending on your point of view.

       Revised

         Robert Strickland September 21, 2015: Added comments and return values

       Input

         double x[]  independent variable x coordinates, strictly
         increasing order
         double y[]  value of function at each x
         int n       Length of x and y
         ypl         First derivative at left end, x[0], or >= 1.e30 to signal
                     to use natural spline (2nd derivative zero)
         ypr         First derivative at right end, x[n-1], or >= 1.e30 to
                     signal to use natural spline

       Returns

         y2[]        The second derivative of spline at the nodes. See below.
                     This changes linearly between the nodes.


       Discussion

         Calculate cubic spline versus x through n points (x[i], y[i]). The
         independent variable must be in strictly increasing order,
         x[i+1] > x[i].  The first and second derivatives of the resulting
         spline will be continous. To evaluate the spline at xx, find the
         interval i such that x[i] <= xx < x[i+1]. Then let

           dx = x[i+1] - x[i]
           a = (x[i+1] - xx)/h
           b = 1. - a

         The value of the cubic spline s(xx) at the point xx is

           f(xx) = a*y[i] + b*y[i+1] +
                  ((a*a*a - a)*y2[i] + (b*b*b - b)*y2[i+1])*(dx*dx)/6.
      /
    """
    u = np.empty(n-1, dtype=np.double)
    y2 = np.empty(n, dtype=np.double)
    if ypl > MAXLIM:  # Use natural spline at left end
        y2[0] = 0.
        u[0] = 0.
    else:  # User provided 1st derivative at left end
        y2[0] = -0.5
        u[0] = (3./(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-ypl)
    for i in range(1, n-1):
        dx = x[i] - x[i-1]
        sig = dx/(x[i+1] - x[i-1])
        p = sig*y2[i-1] + 2.
        y2[i] = (sig - 1.)/p
        u[i] = (y[i+1] - y[i])/(x[i+1] - x[i]) - (y[i] - y[i-1])/dx
        u[i] = (6.*u[i]/(x[i+1] - x[i-1]) - sig*u[i-1])/p
    if ypr > MAXLIM:  # Use natural spline at right end
        qn = 0.
        un = 0.
    else:  # User provided 1st derivative at right end
        qn = 0.5
        un = (3./(x[n-1] - x[n-2]))*(ypr - (y[n-1] - y[n-2])/(x[n-1] - x[n-2]))
    y2[n-1] = (un - qn*u[n-2])/(qn*y2[n-2] + 1.)
    for k in prange(n-2, -1, -1):
        y2[k] = y2[k]*y2[k+1] + u[k]
    return y2


@cfunc(
    numba.void(
        double[:], double[:], double[:],
        intc, intc, intc,
        double[:], double[:]
    )
)
def sx_spline(x, w, f, nx, ny, nz, sx, s):
    for j in prange(ny):
        for k in prange(nz):
            for i in prange(nx):
                w[i] = f[(i*ny+j)*nz+k]
            s = csp1d(x, w, nx, NATSPLN, NATSPLN)
            for i in prange(nx):
                sx[(i*ny+j)*nz+k] = s[i]

    # return sx


@cfunc(
    numba.void(
        double[:], double[:], double[:],
        intc, intc, intc,
        double[:], double[:], double[:], double[:]
    ))
def sy_sxy(y, w, f, nx, ny, nz, sx, sy, sxy, s):
    for i in prange(nx):
        for k in prange(nz):
            for j in prange(ny):
                w[j] = f[(i*ny+j)*nz+k]
            s = csp1d(y, w, ny, NATSPLN, NATSPLN)
            for j in prange(ny):
                sy[(i*ny+j)*nz+k] = s[j]

            for j in prange(ny):
                w[j] = sx[(i*ny+j)*nz+k]
            s = csp1d(y, w, ny, NATSPLN, NATSPLN)
            for j in prange(ny):
                sxy[(i*ny+j)*nz+k] = s[j]
    # return sy, sxy


@cfunc(
    numba.void(
        double[:], double[:], double[:],
        intc, intc, intc,
        double[:], double[:], double[:], double[:],
        double[:], double[:], double[:], double[:]
    )
)
def sz_sxz_syz_sxyz(z, w, f, nx, ny, nz, sx, sy, sz, sxy, sxz, syz, sxyz, s):
    for i in prange(nx):
        for j in prange(ny):
            for k in prange(nz):
                w[k] = f[(i*ny+j)*nz+k]
            s = csp1d(z, w, nz, NATSPLN, NATSPLN)
            for k in prange(nz):
                sz[(i*ny+j)*nz+k] = s[k]

            for k in prange(nz):
                w[k] = sx[(i*ny+j)*nz+k]
            s = csp1d(z, w, nz, NATSPLN, NATSPLN)
            for k in prange(nz):
                sxz[(i*ny+j)*nz+k] = s[k]

            for k in prange(nz):
                w[k] = sy[(i*ny+j)*nz+k]
            s = csp1d(z, w, nz, NATSPLN, NATSPLN)
            for k in prange(nz):
                syz[(i*ny+j)*nz+k] = s[k]

            for k in prange(nz):
                w[k] = sxy[(i*ny+j)*nz+k]
            s = csp1d(z, w, nz, NATSPLN, NATSPLN)
            for k in prange(nz):
                sxyz[(i*ny+j)*nz+k] = s[k]


@njit(
    double[:, :](
        double[:],
        double[:],
        double[:],
        intc,
        intc,
        intc,
        double[:],
    )
)
def csp3d_open(x, y, z, nx, ny, nz, f):
    """

    Purpose

      Calculate 3-D cubic spline coefficients for a function f(x, y, z)
      of three independent variables tabulated on a 3-D rectilinear grid.

      To evaluate this 3-D spline, use csp3d_eval()

    Input

      double *xv[]  Array of 3 arrays of independent variables;
                    xv[] is the array of {x[], y[], z[]}
      int nv[]      Array of length 3 containing lengths of xv[];
                    nv[] = {nx, ny, nz}
      double f[]    f(x[i], y[j], z[k]) packed into a one-dimensional
                    array of length nx*ny*nz. The z[k] index varies the
                    fastest. That is, instead of f[i][j][k], we use
                    f[(i*ny+j)*nz+k]
    Returns

      double **splines  Spline coefficient array. This is an array of length
                        7 of arrays of length nx*ny*nz. The user passes
                        this to csp3d_eval(). Use csp3d_close(splines) to free
                        this memory.

                        On error, returns NULL. Errors is either memory
                        allocation or xv[][] not in strictly increasing order.

    Method

      This calls csp1d() to calculate a 1-D cubic spline through every
      grid point parallel to each axis in turn. The csp1d() calculates the
      second derivative at each input point. These spline coefficients are
      placed in arrays the same size as f[]. Cascaded cubic splines of these
      spline coefficeints are calculated versus directions perpendicular to
      the original independent variable. See csp3d_eval() to see how these are
      used to evaluate the spline.

    By

      Robert W. Strickland, September 21, 2015

    """

    if nx > ny:
        nw = nx
    else:
        nw = ny
    if nz > nw:
        nw = nz
    nf = nx*ny*nz
    w, s = np.empty(
        nw, dtype=np.double), np.empty(
        nw, dtype=np.double)
    splines = np.empty((7, nf), dtype=np.double)

    sx = splines[0]
    sy = splines[1]
    sz = splines[2]
    sxy = splines[3]
    sxz = splines[4]
    syz = splines[5]
    sxyz = splines[6]

    sx_spline(x, w, f, nx, ny, nz, sx, s)
    # /* Spline of: f vs y -> sy
    #  * Cascaded spline of sx vs y -> sxy
    #  */

    sy_sxy(y, w, f, nx, ny, nz, sx, sy, sxy, s)
    sz_sxz_syz_sxyz(z, w, f, nx, ny, nz, sx, sy, sz, sxy, sxz, syz, sxyz, s)

    return splines


csp3d_eval_types = double(
    double[:],
    double[:],
    double[:],
    double[:],
    double[:],
    double[:],
    intc[:],
    double[:],
    double[:, :],
    intc[:]
)
njit_csp3d_eval_types = double(
    double, double, intc, double, double, intc
)

# @cfunc(csp3d_eval_types)


@njit  # (parallel=True)
def csp3d_eval(
    xx, yy, zz,
    x, y, z,
    nx, ny, nz,
    f,
    splines,
    interval
):
    """
    Purpose

      Interpolate a smooth function of three variables tabulated on a
      rectilinear grid. Requires csp3d_open() to calculate coefficients
      stored in splines. There is just one call to csp3d_open() ahead of
      time and then multiple calls to csp3d_eval() to evaluate the spline
      at each point.

    Input

      double p[3]  The point P(x, y, z) where we want f interpolated
      double xv[3] The array of {x[], y[], z[]} where f is tabulated.
      int nv[3]    The array of {nx, ny, nz}, the lengths of the
                   {x[], y[], z[]} arrays
      double f[]   f(x[i], y[j], z[k]) packed into a one-dimensional
                   array of length nx*ny*nz. The z[k] index varies the
                   fastest. That is, instead of f[i][j][k], we use
                   f[(i*ny+j)*nz+k]
      double **splines
                   Spline coefficients returned by csp3d_open()
      int interval[3]  Work array of length 3 to keep track of interval. If
                   there are several parallel calls at the same value of p[],

    Method

      We start by finding the 3-D box in our grid that contains P(x,y,z).
      Find i, j, k such that

        x[i] < x < x[i+1]
        y[j] < y < y[j+1]
        z[k] < z < z[k+1]

      Next we interpolate along the 4 edges of the 3-D box parallel to the
      z axis to find a rectangle, eliminating the z dependence. Next we
      interpolate along the 2 edges of the rectangle parallel to the y axis
      to get a line segment, eliminating the y dependence. Finally, we
      interpolate along the line segment to get a single point and to eliminate
      the x dependence. This can be considered the process of repeatedly
      slicing perpendicular to one of the axes, reducing the dimension by one
      each time until a single point remains.

      To get a better understanding of the underlying process it might be
      best to read this code from the bottom up.

    By

      Robert W. Strickland, September 21, 2015

    """

    sx = splines[0]
    sy = splines[1]
    sz = splines[2]
    sxy = splines[3]
    sxz = splines[4]
    syz = splines[5]
    sxyz = splines[6]

    i8 = np.zeros(8, dtype=np.int64)

    nv = np.array([nx, ny, nz], dtype=np.int64)
    xv = [x, y, z]
    p = [xx, yy, zz]

    for n in range(3):
        if interval[n] < 0 or interval[n] > nv[n]-2:
            interval[n] = nv[n]//2
        while interval[n] > 0 and p[n] < xv[n][interval[n]]:
            interval[n] -= 1
        while interval[n] < nv[n]-2 and p[n] > xv[n][interval[n]+1]:
            interval[n] += 1

    i0 = interval[0]
    i1 = i0+1
    j0 = interval[1]
    j1 = j0+1
    k0 = interval[2]
    k1 = k0+1

    i8[0] = (i0*ny+j0)*nz+k0
    i8[1] = (i0*ny+j0)*nz+k1
    i8[2] = (i0*ny+j1)*nz+k0
    i8[3] = (i0*ny+j1)*nz+k1
    i8[4] = (i1*ny+j0)*nz+k0
    i8[5] = (i1*ny+j0)*nz+k1
    i8[6] = (i1*ny+j1)*nz+k0
    i8[7] = (i1*ny+j1)*nz+k1

    #  Get indices for 8 corners of box containing (xx, yy, zz). The index
    #  that goes from 0-7 can be considered a 3-bit field where the most sig-
    #  nificant bit corresponsds to the x coordinate and the least significant
    #  bit corresponds to the z coordinate

    dz = z[k1] - z[k0]
    # if dz < 0.:
    #     return 1.e30
    b: float64 = (zz - z[k0])/dz
    a: float64 = 1.-b
    g: float64 = dz*dz/6
    c: float64 = g*(a*a*a - a)
    d: float64 = g*(b*b*b - b)

    #  Interpolate in z direction for corners of a rectangular slice through
    #  z=zz plane containing point P(xx,yy,zz). We'll find it convenient to
    #  label the points on this slice as follows:
    #   y[j+1] -01-----+---11
    #            |     |    |
    #       yy --U-----P----V
    #            |     |    |
    #            |     |    |
    #     y[j] -00-----+---10
    #            |     |    |
    #          x[i]   xx  x[i+1]
    #
    #  The loop from 0 to 3 goes through these 4 points in binary counting
    #  order

    f4, sx4, sy4, sxy4 = np.empty(
        4, dtype=np.double), np.empty(
        4, dtype=np.double), np.empty(
        4, dtype=np.double), np.empty(
        4, dtype=np.double)

    for n in range(4):
        n0 = i8[2*n]
        n1 = i8[2*n+1]
        f4[n] = a * f[n0] + b * f[n1] + c * sz[n0] + d * sz[n1]
        sx4[n] = a * sx[n0] + b * sx[n1] + c * sxz[n0] + d * sxz[n1]
        sy4[n] = a * sy[n0] + b * sy[n1] + c * syz[n0] + d * syz[n1]
        sxy4[n] = a*sxy[n0] + b*sxy[n1] + c*sxyz[n0] + d*sxyz[n1]

    # Interpolation coefficients in the y direction
    dy = y[j1]-y[j0]
    # if dy <= 0.:
    #     return 1.e30
    b = (yy - y[j0])/dy
    a = 1.-b
    g = dy*dy/6.
    c = g*(a*a*a - a)
    d = g*(b*b*b - b)

    # Interpolate in y direction for f(U) and f(V)
    f0 = a*f4[0] + b*f4[1] + c*sy4[0] + d*sy4[1]
    f1 = a*f4[2] + b*f4[3] + c*sy4[2] + d*sy4[3]

    # Interpolate in y direction for sx(U) and sx(V)

    sx0 = a*sx4[0] + b*sx4[1] + c*sxy4[0] + d*sxy4[1]
    sx1 = a*sx4[2] + b*sx4[3] + c*sxy4[2] + d*sxy4[3]

    # Interpolate in x direction for value of function at P=(xx, yy)
    dx = x[i1]-x[i0]
    # if dx <= 0.:
    #     return 1.e30
    b = (xx - x[i0])/dx
    a = 1.-b
    g = dx*dx/6.
    c = g*(a*a*a - a)
    d = g*(b*b*b - b)

    return a*f0 + b*f1 + c*sx0 + d*sx1


@njit()
def eval_all_spline(les_3, x, y, z, ndns, f, spline2):
    interp = np.empty(len(les_3), dtype=np.float64)
    interval = np.array([0, 0, 0], dtype=np.int64)
    xx = les_3[:, 0]
    yy = les_3[:, 1]
    zz = les_3[:, 2]
    # x = coord_float64[0]
    # y = coord_float64[1]
    # z = coord_float64[2]
    nx = ndns[0]
    ny = ndns[1]
    nz = ndns[2]
    for i in range(len(les_3)):
        interp[i] = csp3d_eval(xx[i], yy[i], zz[i], x, y, z, nx, ny, nz, f,
                               spline2, interval)
    return interp
