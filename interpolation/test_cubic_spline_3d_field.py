# from unet.tricubic import csp3d_open, csp3d_eval

import matplotlib.pyplot as plt
from torch.nn import functional as F
from matplotlib import pyplot as plt
from glob import glob
import torch
from torch import Tensor
from os.path import abspath
from pathlib import Path
from typing import Any, Dict, Tuple, Union, Callable, Type
from numpy import typing as npt
from torchtyping import TensorType
import sys
import os
from numba import njit
from scipy.interpolate import RegularGridInterpolator
from numba import double, njit, prange, cfunc, carray, farray
import numba
from numba.types import Array, CPointer, int64
from typing import List
import numpy as np
from numpy import float64, typing as npt

double = numba.types.double
intc = int64

MAXLIM = .99e30

NATSPLN: double = 1.e30


@njit()
def eval_all_spline(les_3, x, y, z, ndns, f, spline2):
    interp = np.empty(len(les_3), dtype=np.float64)
    interval = np.array([0, 0, 0], dtype=np.int64)
    xx = les_3[:, 0]
    yy = les_3[:, 1]
    zz = les_3[:, 2]
    # x = coord_float64[0]
    # y = coord_float64[1]
    # z = coord_float64[2]
    nx = ndns[0]
    ny = ndns[1]
    nz = ndns[2]
    for i in range(len(les_3)):
        interp[i] = csp3d_eval(xx[i], yy[i], zz[i], x, y, z, nx, ny, nz, f,
                               spline2, interval)
    return interp


def middle_point(ent):
    return (ent[1:] + ent[:-1])/2


def read_p_thermo(filepath: Union[Path, str], *,
                  dtype=Union[npt.DTypeLike]):
    with open(filepath, "r") as file:
        for line in file:
            if "p_thermo_init" in line:
                return np.array(line.split()[1], dtype=dtype)


ndns = (384, 384, 266)
nles = (48, 48, 50)
path = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_180/"
path_coords = glob(path+"dns_lata_1.sauv.lata.grid_geom2.coord*")
path_coords.sort()
p_thermo = read_p_thermo(
    path+"dns_lata_0.sauv",
    dtype=np.float32
)
rho = np.fromfile(
    path+"dns_lata_1.sauv.lata.0.RHO",
    dtype=np.float32
)
r_air = 287.058
T = p_thermo/(rho*r_air)
T = T.reshape(*ndns, order="F")
f = T.copy().astype("float64").reshape(-1, order="C")
coord_vertex = [np.fromfile(g, dtype=np.float32) for g in path_coords]
coord = [middle_point(c) for c in coord_vertex]
coord_les = [np.linspace(i.min(), i.max(), n, endpoint=False,
                         # retstep=True
                         ) for i, n in zip(coord, nles)]
min_val = [c.min() for c in coord]
max_val = [c.max() for c in coord]
steps = [np.diff(c) for c in coord_vertex]
les_3 = np.meshgrid(*coord_les)
les_3 = np.concatenate([ss.ravel()[np.newaxis]
                       for ss in les_3]).T


coord_reg = [np.linspace(c.min(), c.max(), s) for c, s in zip(coord, ndns)]
small_coord = [cg20[:20] for cg20 in coord_reg]

coord_float64 = [c.astype("float64") for c in coord]


spline = csp3d_open(*coord_float64, *ndns, f).astype("float64")
interp = eval_all_spline(les_3, *coord_float64, ndns, f, spline)
T_les = interp.reshape(*nles)


interp = RegularGridInterpolator(coord, T)
T_RGI = interp(les_3).reshape(*nles)
# plt.imshow(T_interp[..., 0])
# plt.show()


# interp = RegularGridInterpolator(coord, T, "cubic")
# T_interp = interp(les_3).reshape(*nles, order="F")
ax1 = plt.subplot(221)
plt.imshow(T_les[..., 0])
ax1.set_title("Interpolated temperature field")

ax2 = plt.subplot(222)
plt.imshow(T[..., 0])
ax2.set_title("Original temperature field")
plt.title("Comparison on cold side")


ax1 = plt.subplot(223)
plt.imshow(T_les[..., -1])
ax1.set_title("Interpolated temperature field")

ax2 = plt.subplot(224)
plt.imshow(T[..., -1])
ax2.set_title("Original temperature field")
plt.title("Comparison on hot side")

plt.show()
