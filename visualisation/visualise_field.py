import torch
from typing import Union
import pyvista as pv
import numpy as np
from gc import collect
import os


def get_temperature():

    def middle_point(ent):
        return (ent[1:] + ent[:-1])/2

    def shape(x):
        return [i.shape[0] for i in x]

    def generate_grid(field, origin, coordinates):
        """
        Generates a field from data, and its import information
        """
        grid = pv.UniformGrid(
            dims=np.array([c.shape[0] - 1 for c in coordinates]) + 1
        )
        # grid.dimensions(np.array([c.shape[0] - 1 for c in coordinates]) + 1)
        grid.origin = tuple(origin)
        grid.spacing = [np.max(np.diff(o)) for o in coordinates]

        grid.cell_data["values"] = field.flatten(order="F")

        return grid

    def plot_grid(grid,  *, cmap=None, show_edges: Union[bool, None] = False):
        grid.plot(cmap=cmap, show_edges=show_edges)

    def plot(field, origin, coordinates, *, cmap=None, show_edges=None):
        grid = generate_grid(field, origin, coordinates)
        plot_grid(grid, cmap=cmap, show_edges=show_edges)

    path_180 = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_180"
    rho = os.path.join(path_180, "dns_lata_1.sauv.lata.0.RHO")
    path_P = os.path.join(path_180, "dns_lata_1.sauv.lata.0.PRESSURE")
    coord_stem = "dns_lata_1.sauv.lata.grid_geom2.coord"
    coord = []
    for direction in ["x", "y", "z"]:
        coord.append(np.fromfile(
            f"{os.path.join(path_180, coord_stem)}{direction}", dtype=np.float32))

    rho_dns = np.fromfile(rho, dtype=np.float32)
# p_dns = np.fromfile(path_P, dtype=np.float32)

    r_air = 287.058
    p_ath = 1.5e5
    T_dns = p_ath / (rho_dns * r_air)
    T_dns = T_dns.reshape([c.shape[0]-1 for c in coord], order="F")

# Release memory for rho and P
    del rho_dns
# del p_dns

    collect()
    # plot(T_dns, [c[0] for c in coord], coord)
    return torch.from_numpy(T_dns.copy()), coord
