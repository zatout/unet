import torch
from .gradient import dtdy_torch as dtdy
from .gradient import d2tdy2_torch as d2tdy2
from torch import nn


def mean_txz(x):
    return torch.mean(x, (0, -2, -3))


def rms(x):
    return torch.sqrt(mean_txz(x**2)-mean_txz(x)**2)


def loss(x, y):
    return torch.sqrt(nn.MSELoss()(x, y))


def loss_grad_rms(x, y, coord_elem, coord_face, cmax=0.029806747737097676, fmax=0.02984599955379963):
    # MSE on mean quantities
    coord_elem[-1] = coord_elem[-1]/cmax
    coord_face[-1] = coord_face[-1]/fmax
    eps = torch.exp(torch.tensor(-10))

    cf = [None, None, coord_face[-1][1:-1]]
    ce = [None, None, coord_elem[-1][1:-1]]

    norm_dns = torch.norm(mean_txz(x))
    norm_rms = torch.norm(rms(x))

    mean_quantities = loss(mean_txz(x), mean_txz(y))/(norm_dns+eps)
    rms_quantities = loss(rms(x), rms(y))/(norm_rms+eps)

    # MSE on wall-normal gradient quantities
    gx, gy = dtdy(x, coord_elem)[..., 1:-1], dtdy(y, coord_elem)[..., 1:-1]
    # GX.shape = (n, 1, 16, 16, 15)
    norm_gradient = torch.norm(mean_txz(gx))
    norm_gradient_rms = torch.norm(rms(gx))

    mean_gradient_quantities = loss(
        mean_txz(gx), mean_txz(gy))/(norm_gradient+eps)
    rms_gradient_quantities = loss(rms(gx), rms(gy))/(norm_gradient_rms+eps)

    g2x, g2y = d2tdy2(x[..., 1:-1], gx, cf)[..., 1:-
                                            1], d2tdy2(y[..., 1:-1], gy, cf)[..., 1:-1]
    # G2X.shape = (n, 1, 16, 16, )
    norm_gradient_gradient = torch.norm(g2x)
    norm_gradient_gradient_rms = torch.norm(rms(g2x))
    mean_gradient_gradient_quantities = loss(
        mean_txz(g2x), mean_txz(g2y))/(norm_gradient_gradient+eps)
    rms_gradient_gradient_quantities = loss(
        rms(g2x), rms(g2y))/(norm_gradient_gradient_rms+eps)

    return mean_quantities + rms_quantities +\
        mean_gradient_quantities + rms_gradient_quantities +\
        mean_gradient_gradient_quantities + rms_gradient_gradient_quantities


def loss_grad_torch(x, y, coord_elem, coord_face, cmax=0.029806747737097676, fmax=0.02984599955379963):
    coord_elem[-1] = coord_elem[-1]/cmax
    coord_face[-1] = coord_face[-1]/fmax

    ce = [None, None, coord_elem[-1][1:-1]]
    norm_dns = torch.norm(mean_txz(x))
    norm_rms = torch.norm(rms(x))

    mean_quantities = loss(mean_txz(x), mean_txz(y))/norm_dns
    rms_quantities = loss(rms(x), rms(y))/norm_rms

    # MSE on wall-normal gradient quantities
    gx, gy = torch.gradient(x, spacing=(coord_elem[-1],), dim=-1)[
        0][..., 1:-1], torch.gradient(y, spacing=(coord_elem[-1],), dim=-1)[0][..., 1:-1]
    # GX.shape = (n, 1, 16, 16, 15)
    norm_gradient = torch.norm(mean_txz(gx))
    norm_gradient_rms = torch.norm(rms(gx))

    mean_gradient_quantities = loss(mean_txz(gx), mean_txz(gy))/norm_gradient
    rms_gradient_quantities = loss(rms(gx), rms(gy))/norm_gradient_rms

    g2x, g2y = torch.gradient(x, spacing=(coord_elem[-1],), dim=-1)[
        0][..., 1:-1], torch.gradient(y, spacing=(coord_elem[-1],), dim=-1)[0][..., 1:-1]
    # G2X.shape = (n, 1, 16, 16, )
    norm_gradient_gradient = torch.norm(g2x)
    norm_gradient_gradient_rms = torch.norm(rms(g2x))
    mean_gradient_gradient_quantities = loss(
        mean_txz(g2x), mean_txz(g2y))/norm_gradient_gradient
    rms_gradient_gradient_quantities = loss(
        rms(g2x), rms(g2y))/norm_gradient_gradient_rms

    return mean_quantities + rms_quantities +\
        mean_gradient_quantities + rms_gradient_quantities +\
        mean_gradient_gradient_quantities + rms_gradient_gradient_quantities
