from typing import Union, Tuple
import torch
import torch.nn as nn
import os
import numpy as np


class DoubleConv(nn.Module):
    """
    Double convolution class
    """

    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 kernel_size: int = 3,
                 stride: int = 1,
                 padding: Union[int, str] = "same",
                 activation: str = "ReLU",
                 padding_mode: str = "zeros"
                 ):
        act = nn.ReLU()
        if activation.lower() == "relu":
            act = nn.ReLU()
        elif activation.lower() == "gelu":
            act = nn.GELU()
        elif activation.lower == "elu":
            act = nn.ELU()
        super(DoubleConv, self).__init__()
        self.double_conv = nn.Sequential(
            nn.Conv3d(in_channels,
                      out_channels,
                      kernel_size=kernel_size,
                      stride=stride,
                      padding=padding,
                      bias=True,
                      padding_mode=padding_mode),
            nn.BatchNorm3d(out_channels),
            act,
            nn.Conv3d(out_channels,
                      out_channels,
                      kernel_size=kernel_size,
                      stride=stride,
                      padding=padding,
                      bias=True,
                      padding_mode=padding_mode),
            nn.BatchNorm3d(out_channels),
            act,
        )

    def forward(self, x):
        return self.double_conv(x)


class DoubleConvTranspose(nn.Module):
    """
    Double convolution class
    """

    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 kernel_size: int = 3,
                 stride: int = 1,
                 padding: Union[int, str] = "same",
                 activation: str = "ReLU",
                 padding_mode: str = "zeros"
                 ):
        act = nn.ReLU()
        if activation.lower() == "relu":
            act = nn.ReLU()
        elif activation.lower() == "gelu":
            act = nn.GELU()
        elif activation.lower == "elu":
            act = nn.ELU()
        if padding == "same" and kernel_size % 2 == 0:
            raise TypeError("You can't provide an even kernel size"
                            " expecting a same padding."
                            )
        padding = int((kernel_size-1)/2)
        super(DoubleConvTranspose, self).__init__()
        self.double_conv = nn.Sequential(
            nn.ConvTranspose3d(in_channels,
                               out_channels,
                               kernel_size=kernel_size,
                               stride=stride,
                               padding=padding,
                               bias=True,
                               padding_mode=padding_mode),
            nn.BatchNorm3d(out_channels),
            act,
            nn.ConvTranspose3d(out_channels,
                               out_channels,
                               kernel_size=kernel_size,
                               stride=stride,
                               padding=padding,
                               bias=True,
                               padding_mode=padding_mode),
            nn.BatchNorm3d(out_channels),
            act,
        )

    def forward(self, x):
        return self.double_conv(x)

# filters,
# kernel_size,


class Unet(nn.Module):
    """
    Implementation of the U-Net used in Lapeyre et al.
    """

    def __init__(self,
                 kernel_size: int = 3,
                 padding: Union[int, str] = "same",
                 maxpool_kernel: Union[int, Tuple[int]] = 2,
                 last_act: str = "tanh",
                 padding_mode: str = "zeros"
                 ):
        super().__init__()
        upsampling_kernel = maxpool_kernel
        self.down1 = DoubleConv(in_channels=1,
                                out_channels=32,
                                kernel_size=kernel_size,
                                padding=padding,
                                activation="ReLU",
                                padding_mode=padding_mode
                                )
        self.down2 = DoubleConv(in_channels=32,
                                out_channels=64,
                                kernel_size=kernel_size,
                                padding=padding,
                                activation="ReLU",
                                padding_mode=padding_mode
                                )

        self.down3 = DoubleConv(in_channels=64,
                                out_channels=128,
                                kernel_size=kernel_size,
                                padding=padding,
                                activation="ReLU",
                                padding_mode=padding_mode
                                )

        self.up1 = DoubleConvTranspose(in_channels=128 + 64,
                                       out_channels=64,
                                       kernel_size=kernel_size,
                                       padding=padding,
                                       activation="ReLU",
                                       padding_mode=padding_mode
                                       )
        self.up2 = DoubleConvTranspose(in_channels=64 + 32,
                                       out_channels=32,
                                       kernel_size=kernel_size,
                                       padding=padding,
                                       activation="ReLU",
                                       padding_mode=padding_mode
                                       )

        if padding == "same" and kernel_size % 2 == 0:
            raise TypeError("You can't provide an even kernel size"
                            " expecting a same padding."
                            )
        padding = int((kernel_size-1)/2)
        if last_act.lower() == "relu":
            last_act_ = nn.ReLU()
        else:
            last_act_ = nn.Tanh()
        self.last = nn.Sequential(
            nn.ConvTranspose3d(in_channels=32,
                               out_channels=1,
                               kernel_size=kernel_size,
                               padding=padding,
                               padding_mode=padding_mode),
            last_act_
        )

        self.pooling = nn.MaxPool3d(maxpool_kernel)
        self.upsampling = nn.Upsample(scale_factor=upsampling_kernel)

    def device(self):
        return self.last[0].weight.device

    def forward(self, x):
        out_conv1 = self.down1(x)        # After 1st double conv

        x = self.pooling(out_conv1)
        out_conv2 = self.down2(x)        # After 2nd double conv

        x = self.pooling(out_conv2)
        out_conv3 = self.down3(x)        # After 3rd double conv1

        upsampled1 = self.upsampling(out_conv3)

        concat1 = torch.cat((upsampled1, out_conv2), dim=1)

        out_convtranspose1 = self.up1(concat1)
        upsampled2 = self.upsampling(out_convtranspose1)
        concat2 = torch.cat((upsampled2, out_conv1), dim=1)
        out_convtranspose2 = self.up2(concat2)
        out = self.last(out_convtranspose2)

        return out
########################################################################
#                               TESTS                                  #
########################################################################


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def count_par_test():
    net = Unet()
    print(count_parameters(net))


def test():
    # Initializing a tensor of shape (8, 8, 8) we can coarsen 3 times
    test_t = torch.randn(1, 1, 2**3, 2**3, 2**3)
    print(f"Input shape: {test_t.shape}")
    net = Unet(kernel_size=3, padding="same", maxpool_kernel=2)
    out = net(test_t)

    print(f"Output shape: {out.shape}")

    assert test_t.shape == out.shape, "Shapes aren't identical"


def test_dns():
    dns_180_path = os.path.join("/home/zatout/Documents/These_yanis/",
                                "Reunion_10012022/"
                                "dns_180/dns_lata_1.sauv.lata.0.RHO")

    sizes = (384, 384, 266)
    rho = np.fromfile(dns_180_path, dtype=np.float32)
    rho = rho.reshape(sizes, order="F")
    print(f"Rho shape: {rho.shape}")
    rho = torch.tensor(rho, dtype=torch.float32)
    rho = rho[None, None, ...]
    net = Unet(kernel_size=3, padding="same", maxpool_kernel=2)
    recon = net(rho)
    print(f"Reconstructed : {recon.shape}")


def test_16():
    sizes = (16, 16, 16)
    test_t = torch.randn(50, 1, *sizes).to("cuda")
    net = Unet().to("cuda")

    out = net(test_t)
    print(out.shape)


if __name__ == "__main__":
    count_par_test()
    # test()
    # test_dns()
    test_16()
