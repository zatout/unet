from typing import List
from scipy.interpolate import RegularGridInterpolator
from torch import Tensor
import torch


def reshape_fortran(x, shape):
    """
    Equivalent to numpy.reshape(..., order="F")
    for cartesian grids
    """
    if len(x.shape) > 0:
        x = x.permute(*reversed(range(len(x.shape))))
    return x.reshape(*reversed(shape)).permute(*reversed(range(len(shape))))


def coord(coord_: List[Tensor]) -> Tensor:
    """
    For an array of coordinates, returns a flattened matrix, with the
    coordinates of each points in 3 dims as (x, y, z)
    Parameters:
    ----------
    coord: Tensor
        coordinates tensor
    Returns:
    -------
    out Tensor
        tensor of coordinates of shape (3, x*y*z)
    """
    t = torch.meshgrid(*coord_, indexing="xy")
    t = torch.cat([i.ravel().unsqueeze(0) for i in t])
    return t.t()


def interp_dns_on_les_mesh(
    dns_field: Tensor,
    dns_coord: List[Tensor],
    les_coord: List[Tensor],
    les_shape: tuple
):
    """
    Does a regular grid interpolation on a rectinilear grid for our case using
    RegularGridInterpolator from scipy
    Parameters:
    ----------
    dns_field: (..., H, W, D) Tensor
        DNS field you want to interpolate
    dns_coord: (H, W, D) List
        list of coordinates of each point
    les_coord: (H_les, W_les, D_les) List
        desired coordinates for the LES mesh
    les_shape: (3,) tuple
        number of points of the LES mesh

    Returns:
    --------
    Out: (H_les, W_les, D_les) Tensor
        torch tensor of interpolated DNS field onto LES mesh
    """
    dns_coord_new = tuple([d.numpy() for d in dns_coord])
    les_coord_new = coord(les_coord)
    les_coord_new = tuple([d.numpy() for d in les_coord_new])
    # 2 first dimentions of a DNS field are batch and channels in pytorch
    interpolator = RegularGridInterpolator(
        dns_coord_new,
        dns_field.numpy()[0, 0],
        "cubic"
    )
    return interpolator(les_coord_new).reshape(-1, 1, *les_shape)
