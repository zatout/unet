from torch import Tensor
from torchvision.utils import _log_api_usage_once
import torch
from typing import Union


class Random90Rotation(torch.nn.Module):
    """Rotate the image by angle.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading dimensions.
    """

    def __init__(
        self,
            n_rot_max: int,
            axes: dict = {0: [-2, -1], 1: [-3, -1], 2: [-3, -2]},
    ):
        super().__init__()
        _log_api_usage_once(self)
        self.n_rot_max = n_rot_max
        self.axes = axes

    def forward(self, img):
        """
        Args:
            img (Tensor): Image to be rotated.

        Returns:
            PIL Images or Tensors: Rotated image.
        """
        self.n_rot = torch.randint(0, self.n_rot_max+1, (1,)).item()
        self.axis_flip = torch.randint(0, len(self.axes), (1,)).item()
        if isinstance(img, tuple):
            return torch.rot90(
                img[0],
                k=self.n_rot,
                dims=self.axes[self.axis_flip]
            ), torch.rot90(
                img[1],
                k=self.n_rot,
                dims=self.axes[self.axis_flip]
            )
        else:
            return torch.rot90(
                img,
                k=self.n_rot,
                dims=self.axes[self.axis_flip],
            )

    def __repr__(self) -> str:
        format_string = self.__class__.__name__ + f"(degrees={90}"
        format_string += ")"
        return format_string


class Random90RotationTemperatureSymmetry(torch.nn.Module):
    """Rotate the image by angle.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading dimensions.
    """

    def __init__(
        self,
            n_rot_max: int
    ):
        super().__init__()
        _log_api_usage_once(self)
        self.n_rot_max = n_rot_max

    def forward(self, img):
        """
        Args:
            img (Tensor): Image to be rotated.

        Returns:
            PIL Images or Tensors: Rotated image.
        """
        n_rot = torch.randint(0, self.n_rot_max+1, (1,)).item()
        # If number of rotations is congruated to 4, it's equivalent to no rotations
        if n_rot % 4 == 0:
            return img
        if isinstance(img, tuple):
            return torch.rot90(
                img[0],
                k=n_rot,
                dims=[-3, -2]
            ), torch.rot90(
                img[1],
                k=n_rot,
                dims=[-3, -2]
            )
        else:
            return torch.rot90(
                img,
                k=n_rot,
                dims=[-3, -2]
            )

    def __repr__(self) -> str:
        format_string = self.__class__.__name__ + f"(degrees={90}"
        format_string += ")"
        return format_string
