from torch.utils.data import Dataset
import torch
import os
from pathlib import Path
from glob import glob
from typing import Any, Union, Callable, Tuple
from scipy.interpolate import RegularGridInterpolator

import numpy.typing as npt
import numpy as np

from .utils import load_fortran_to_c, load_patch


def middle_point(ent):
    return (ent[1:] + ent[:-1])/2


def read_p_thermo(filepath: Union[Path, str], *, dtype=torch.float32):
    with open(filepath, "r") as file:
        for line in file:
            if "p_thermo_init" in line:
                if dtype == torch.float32:
                    return torch.tensor(float(line.split()[1]), dtype=dtype)
                else:
                    return np.array(line.split()[1], dtype=dtype)


def read_time(filepath: Union[Path, str]):
    with open(filepath, "r") as file:
        for line in file:
            if "tinit" in line:
                return line.split()[1]


def normalize(x, min, max):
    return (x - min)/(max-min)


def unnormalize(x, min, max):
    return (x * (max - min)) + min


def shape(x):
    return [i.shape for i in x]


def load_dns(path, shape_dns=(384, 384, 266)):
    # rho_path = glob(os.path.join(test_dns_path, "*RHO"))
    dir_path = path.split("dns_lata")[0]
    rho = load_fortran_to_c(path, shape_dns)
    number = path.split("_")[-1].split(".")[0]
    number = int(number)
    p_thermo = read_p_thermo(os.path.join(
        dir_path, f"dns_lata_{number}.sauv"), dtype=np.float32)
    r_air: float = 287.058
    T_dns = p_thermo/(rho*r_air)
    T_dns = T_dns.reshape(*shape_dns).astype(np.float64)

    coord_path = path.split("dns_lata")[0]
    coord_path_dns = sorted(
        glob(f"{glob(f'{coord_path}/' + '*x')[0][:-1]}*")[:3])

    coord_vertex = [
        np.fromfile(
            cp, dtype=np.float32
        )
        for cp in coord_path_dns
    ]
    coord_dns = [middle_point(c) for c in coord_vertex]

    return T_dns, coord_dns


def load_dns_coord(path):
    # rho_path = glob(os.path.join(test_dns_path, "*RHO"))
    number = path.split("_")[-1].split(".")[0]
    number = int(number)

    coord_path = path.split("dns_lata")[0]
    coord_path_dns = sorted(
        glob(f"{glob(f'{coord_path}/' + '*x')[0][:-1]}*")[:3])

    coord_vertex = [
        np.fromfile(
            cp, dtype=np.float32
        )
        for cp in coord_path_dns
    ]
    coord_dns = [middle_point(c) for c in coord_vertex]

    return coord_dns


def load_les(path, shape_les):
    T_les = np.fromfile(path, dtype=np.float64).reshape(*shape_les)
    T_out = T_les.reshape(*shape_les)

    return torch.from_numpy(T_out)  # , coord_les, steps_les


def load_coarse_les(path, coord, shape_les):
    nles = shape_les
    T_les = np.fromfile(path, dtype=np.float64).reshape(*shape_les)
    T_out = T_les.reshape(*shape_les)

    coord_les = [np.linspace(i.min(), i.max(), n)
                 for i, n in zip(coord, nles)]
    steps_les = [np.linspace(i.min(), i.max(), n, retstep=True)
                 for i, n in zip(coord, nles)]
    steps_les = [s[1] for s in steps_les]
    les_3 = np.meshgrid(*coord_les, indexing="ij")
    les_3 = np.concatenate([ss.ravel()[np.newaxis]
                           for ss in les_3]).T

    return torch.from_numpy(T_out), coord_les, steps_les


class DnsTemperatureLoader(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        dir_les: Union[str, Path],
        transform: Union[Callable, None] = None,
        *,
        re_tau: float = 180,
        nles: Tuple[int, int, int] = (48, 48, 50),
        normalize: bool = False,
        device: str = "cpu",
        r_air: float = 287.058,
        tmin: float = 293,
        tmax: float = 586
    ) -> None:
        super().__init__()
        self.re_tau = re_tau
        self.nles = nles
        self.dir_dns = dir_dns
        les_mesh_size_str = f"{nles[0]}_{nles[2]}_{nles[1]}"
        # self.dir_les = sorted(Path(images_dir).glob(
        #     f"les_{re_tau}/{les_mesh_size_str}"))[0]
        self.dir_les = os.path.join(dir_les, les_mesh_size_str)
        self.dir_les_str = str(self.dir_les)

        self.dns_examples = sorted(list(Path(self.dir_dns).glob("*.RHO")))
        self.les_examples = sorted(list(Path(self.dir_les_str).glob("T*")))

        # assert len(self.les_examples) == len(
        #     self.dns_examples), "Not the same number of snapshots"
        # This is the absolute path to coordoniates
        self.coord_path_dns = sorted(glob(
            f"{glob(f'{self.dir_dns}/' + '*x')[0][:-1]}*")[:3])
        self.coord_vertex_dns = [
            torch.tensor(np.fromfile(cp, dtype=np.float32))
            for cp in self.coord_path_dns
        ]
        self.coord_dns = [middle_point(c) for c in self.coord_vertex_dns]
        self.coord_3_by_3_dns = np.meshgrid(*self.coord_dns, indexing="ij")
        # Fancy way of creating a (n_point_dns, 3)
        # matrix with points in the (i,j,k) space
        # Necessary for linear interpolation
        self.coord_3_by_3_dns = np.concatenate(
            [
                ss.ravel()[np.newaxis]
                for ss in self.coord_3_by_3_dns
            ]
        ).T
        self.shape_dns = [c.shape[0] for c in self.coord_dns]
        self.transform = transform

        self.coord_les = [
            np.linspace(i.min(), i.max(), n)
            for i, n in zip(self.coord_dns, nles)
        ]

        self.path_p_thermo_dns = sorted(
            glob(f"{self.dir_dns}/dns_lata_*.sauv"))
        self.p_thermo = torch.tensor(
            [read_p_thermo(c) for c in self.path_p_thermo_dns]
        )
        self.delta = [torch.diff(c) for c in self.coord_dns]
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmax = tmax
        self.tmin = tmin

    def __len__(self):
        return len(self.dns_examples)

    def preprocess(self, img):
        if self.transform is not None:
            return self.transform(img)
        else:
            return img

    @staticmethod
    def load(filename: Union[str, Path], *, dtype: npt.DTypeLike = np.float32):
        return torch.tensor(np.fromfile(filename, dtype=dtype))

    def __getitem__(self, index: Any):
        # This is a list
        rho_path_dns = self.dns_examples[index]
        rho_dns = load_fortran_to_c(rho_path_dns, self.shape_dns)

        T_les_path = self.les_examples[index]
        T_les = np.fromfile(T_les_path, dtype=np.float64).reshape(*self.nles)
        # Linear interpolation
        interpolator = RegularGridInterpolator(
            self.coord_les,
            T_les,
            method="linear"
        )
        T_les_interp = interpolator(
            self.coord_3_by_3_dns
        ).reshape(
            *self.shape_dns
        )

        # Compute the temperature from pressure and rho
        T_dns = self.p_thermo[index]/(rho_dns * self.r_air)
        T_dns = T_dns.view(*self.shape_dns).float().contiguous()

        if self.normalize:
            T_dns = (T_dns - self.tmin)/(self.tmax - self.tmin)
            T_les_interp = (T_les_interp - self.tmin)/(self.tmax - self.tmin)
        T_les_interp = torch.from_numpy(
            T_les_interp).float().contiguous()
        out_dns, out_les = self.preprocess((T_dns, T_les_interp))
        return out_dns.unsqueeze(0), out_les.unsqueeze(0)


class DnsTemperatureLoaderInterpolated(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        dir_les: Union[str, Path],
        transform: Union[Callable, None] = None,
        *,
        re_tau: float = 180,
        nles: Tuple[int, int, int] = (48, 48, 50),
        normalize: bool = False,
        device: str = "cpu",
        r_air: float = 287.058,
        tmin: float = 293,
        tmax: float = 586
    ) -> None:
        super().__init__()
        self.re_tau = re_tau
        self.nles = nles
        self.dir_dns = dir_dns
        les_mesh_size_str = f"{nles[0]}_{nles[2]}_{nles[1]}"
        # self.dir_les = sorted(Path(images_dir).glob(
        #     f"les_{re_tau}/{les_mesh_size_str}"))[0]
        self.dir_les = os.path.join(dir_les, les_mesh_size_str)
        self.dir_les_str = str(self.dir_les)

        self.dns_examples = sorted(list(Path(self.dir_dns).glob("*.RHO")))
        self.les_examples = sorted(list(Path(self.dir_les_str).glob("T*")))

        # assert len(self.les_examples) == len(
        #     self.dns_examples), "Not the same number of snapshots"
        # This is the absolute path to coordoniates
        self.coord_path_dns = sorted(glob(
            f"{glob(f'{self.dir_dns}/' + '*x')[0][:-1]}*")[:3])
        self.coord_vertex_dns = [
            torch.tensor(np.fromfile(cp, dtype=np.float32))
            for cp in self.coord_path_dns
        ]
        self.coord_dns = [middle_point(c) for c in self.coord_vertex_dns]
        self.coord_3_by_3_dns = np.meshgrid(*self.coord_dns, indexing="ij")
        # Fancy way of creating a (n_point_dns, 3)
        # matrix with points in the (i,j,k) space
        # Necessary for linear interpolation
        self.coord_3_by_3_dns = np.concatenate(
            [
                ss.ravel()[np.newaxis]
                for ss in self.coord_3_by_3_dns
            ]
        ).T
        self.shape_dns = [c.shape[0] for c in self.coord_dns]
        self.transform = transform

        self.coord_les = [
            np.linspace(i.min(), i.max(), n)
            for i, n in zip(self.coord_dns, nles)
        ]

        self.path_p_thermo_dns = sorted(
            glob(f"{self.dir_dns}/dns_lata_*.sauv"))
        self.p_thermo = torch.tensor(
            [read_p_thermo(c) for c in self.path_p_thermo_dns]
        )
        self.delta = [torch.diff(c) for c in self.coord_dns]
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmax = tmax
        self.tmin = tmin

    def __len__(self):
        return len(self.dns_examples)

    def preprocess(self, img):
        if self.transform is not None:
            return self.transform(img)
        else:
            return img

    @staticmethod
    def load(filename: Union[str, Path], *, dtype: npt.DTypeLike = np.float32):
        return torch.tensor(np.fromfile(filename, dtype=dtype))

    def __getitem__(self, index: Any):
        # This is a list
        rho_path_dns = self.dns_examples[index]
        rho_dns = load_fortran_to_c(rho_path_dns, self.shape_dns)

        T_les_path = self.les_examples[index]
        T_les_interp = np.fromfile(
            T_les_path, dtype=np.float64).reshape(*self.shape_dns)
        # Linear interpolation

        # Compute the temperature from pressure and rho
        T_dns = self.p_thermo[index]/(rho_dns * self.r_air)
        T_dns = T_dns.view(*self.shape_dns).float().contiguous()

        if self.normalize:
            T_dns = (T_dns - self.tmin)/(self.tmax - self.tmin)
            T_les_interp = (T_les_interp - self.tmin)/(self.tmax - self.tmin)
        T_les_interp = torch.from_numpy(
            T_les_interp).float().contiguous()
        out_dns, out_les = self.preprocess((T_dns, T_les_interp))
        return out_dns.unsqueeze(0).to(self.device), out_les.unsqueeze(0).to(self.device)


class DnsTemperatureLoaderInterpolatedFastPatch(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        dir_les: Union[str, Path],
        transform: Union[Callable, None] = None,
        *,
        re_tau: float = 180,
        nles: Tuple[int, int, int] = (48, 48, 50),
        patch_size: Tuple[int, int, int] = (16, 16, 16),
        normalize: bool = False,
        device: str = "cpu",
        r_air: float = 287.058,
        tmin: float = 293,
        tmax: float = 586
    ) -> None:
        super().__init__()
        self.re_tau = re_tau
        self.nles = nles
        self.dir_dns = dir_dns
        self.patch_size = patch_size
        les_mesh_size_str = f"{nles[0]}_{nles[2]}_{nles[1]}"
        # self.dir_les = sorted(Path(images_dir).glob(
        #     f"les_{re_tau}/{les_mesh_size_str}"))[0]
        self.dir_les = os.path.join(dir_les, les_mesh_size_str)
        self.dir_les_str = str(self.dir_les)

        self.dns_examples = sorted(list(Path(self.dir_dns).glob("*.RHO")))
        self.les_examples = sorted(list(Path(self.dir_les_str).glob("T*")))

        # assert len(self.les_examples) == len(
        #     self.dns_examples), "Not the same number of snapshots"
        # This is the absolute path to coordoniates
        self.coord_path_dns = sorted(glob(
            f"{glob(f'{self.dir_dns}/' + '*x')[0][:-1]}*")[:3])
        self.coord_vertex_dns = [
            torch.tensor(np.fromfile(cp, dtype=np.float32))
            for cp in self.coord_path_dns
        ]
        self.coord_dns = [middle_point(c) for c in self.coord_vertex_dns]
        self.coord_3_by_3_dns = np.meshgrid(*self.coord_dns, indexing="ij")
        # Fancy way of creating a (n_point_dns, 3)
        # matrix with points in the (i,j,k) space
        # Necessary for linear interpolation
        self.coord_3_by_3_dns = np.concatenate(
            [
                ss.ravel()[np.newaxis]
                for ss in self.coord_3_by_3_dns
            ]
        ).T
        self.shape_dns = [c.shape[0] for c in self.coord_dns]
        self.ndns = self.shape_dns
        self.transform = transform

        self.coord_les = [
            np.linspace(i.min(), i.max(), n)
            for i, n in zip(self.coord_dns, nles)
        ]

        self.path_p_thermo_dns = sorted(
            glob(f"{self.dir_dns}/dns_lata_*.sauv"))
        self.p_thermo = torch.tensor(
            [read_p_thermo(c) for c in self.path_p_thermo_dns]
        )
        self.delta = [torch.diff(c) for c in self.coord_dns]
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmax = tmax
        self.tmin = tmin

    def __len__(self):
        return len(self.dns_examples)

    def preprocess(self, img):
        if self.transform is not None:
            return self.transform(img)
        else:
            return img

    @staticmethod
    def load(filename: Union[str, Path], *, dtype: npt.DTypeLike = np.float32):
        return torch.tensor(np.fromfile(filename, dtype=dtype))

    def __getitem__(self, index: Any):
        # This is a list
        rho_path_dns = self.dns_examples[index]
        i = np.random.randint(0, self.ndns[0] - self.patch_size[0])
        j = np.random.randint(0, self.ndns[1] - self.patch_size[1])
        k = np.random.randint(0, self.ndns[2] - self.patch_size[2])
        rho_patch = load_patch(
            rho_path_dns, i, j, k,
            *self.patch_size,
            *self.ndns,
            dtype=np.float32,
            order="F"
        )

        T_les_path = self.les_examples[index]
        T_les_interp = load_patch(
            T_les_path,
            i, j, k,
            *self.patch_size,
            *self.ndns,
            dtype=np.float64,
            order="C"
        )
        # Linear interpolation

        # Compute the temperature from pressure and rho
        T_dns = self.p_thermo[index]/(rho_patch * self.r_air)
        T_dns = T_dns.float().contiguous().to(self.device)

        if self.normalize:
            T_dns = (T_dns - self.tmin)/(self.tmax - self.tmin)
            T_les_interp = (T_les_interp - self.tmin)/(self.tmax - self.tmin)
        T_les_interp = torch.from_numpy(
            T_les_interp).float().contiguous().to(self.device)
        out_dns, out_les = self.preprocess((T_dns, T_les_interp))
        return out_dns.unsqueeze(0), out_les.unsqueeze(0)


class TestLoader(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        dir_les: Union[str, Path],
        dir_les_coarse: Union[str, Path],
        *,
        re_tau: float = 180,
        nles: Tuple[int, int, int] = (48, 48, 50),
        normalize: bool = False,
        device: str = "cpu",
        r_air: float = 287.058,
        tmin: float = 293,
        tmax: float = 586
    ) -> None:
        super().__init__()
        self.re_tau = re_tau
        self.nles = nles
        self.dir_dns = dir_dns
        les_mesh_size_str = f"{nles[0]}_{nles[2]}_{nles[1]}"

        self.dir_les = os.path.join(dir_les, les_mesh_size_str)
        self.dir_les_coarse = os.path.join(dir_les_coarse, les_mesh_size_str)
        self.dir_les_str = str(self.dir_les)
        self.dir_les_str_coarse = str(self.dir_les_coarse)

        self.dns_examples = sorted(list(Path(self.dir_dns).glob("*.RHO")))
        self.les_examples = sorted(
            list(Path(self.dir_les_str).glob("T_interpolated*")))
        self.les_coarse_examples = sorted(
            list(Path(self.dir_les_str_coarse).glob("T_*")))

        self.coord_path_dns = sorted(
            glob(f"{glob(f'{self.dir_dns}/' + '*x')[0][:-1]}*")[:3])
        self.coord_vertex_dns = [
            torch.tensor(np.fromfile(cp, dtype=np.float32))
            for cp in self.coord_path_dns
        ]
        self.coord_dns = [middle_point(c) for c in self.coord_vertex_dns]
        self.coord_3_by_3_dns = np.meshgrid(*self.coord_dns, indexing="ij")

        # Fancy way of creating a (n_point_dns, 3)
        # matrix with points in the (i,j,k) space
        # Necessary for linear interpolation
        self.coord_3_by_3_dns = np.concatenate(
            [
                ss.ravel()[np.newaxis]
                for ss in self.coord_3_by_3_dns
            ]
        ).T
        self.shape_dns = [c.shape[0] for c in self.coord_dns]

        self.coord_les = [
            np.linspace(i.min(), i.max(), n)
            for i, n in zip(self.coord_dns, nles)
        ]

        self.path_p_thermo_dns = sorted(
            glob(f"{self.dir_dns}/dns_lata_*.sauv"))
        self.p_thermo = torch.tensor(
            [read_p_thermo(c) for c in self.path_p_thermo_dns]
        )
        self.delta = [torch.diff(c) for c in self.coord_dns]
        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmax = tmax
        self.tmin = tmin

    def __len__(self):
        return len(self.dns_examples)

    @staticmethod
    def load(filename: Union[str, Path], *, dtype: npt.DTypeLike = np.float32):
        return torch.tensor(np.fromfile(filename, dtype=dtype))

    def __getitem__(self, index: Any):
        # This is a list
        rho_path_dns = self.dns_examples[index]
        rho_dns = load_fortran_to_c(rho_path_dns, self.shape_dns)

        T_les_path = self.les_examples[index]
        T_les_interp = np.fromfile(
            T_les_path, dtype=np.float64).reshape(*self.shape_dns)

        T_les = np.fromfile(
            self.les_coarse_examples[index], dtype=np.float64).reshape(
            *self.nles)

        # Compute the temperature from pressure and rho
        T_dns = self.p_thermo[index]/(rho_dns * self.r_air)
        T_dns = T_dns.view(*self.shape_dns).float().contiguous()

        if self.normalize:
            T_dns = (T_dns - self.tmin)/(self.tmax - self.tmin)
            T_les_interp = (T_les_interp - self.tmin)/(self.tmax - self.tmin)
            T_les = (T_les - self.tmin) / (self.tmax - self.tmin)
        T_les_interp = torch.from_numpy(
            T_les_interp).float().contiguous()
        T_les = torch.from_numpy(T_les).float().contiguous()
        out_dns, out_les, out_les_coarse = (T_dns, T_les_interp, T_les)
        return (
            out_dns.unsqueeze(0).unsqueeze(0),
            out_les.unsqueeze(0).unsqueeze(0),
            out_les_coarse.unsqueeze(0).unsqueeze(0)
        )


class DnsTemperatureLoaderCoarse(Dataset):
    def __init__(
        self,
        dir_dns: Union[str, Path],
        dir_les: Union[str, Path],
        transform: Union[Callable, None] = None,
        *,
        re_tau: float = 180,
        nles: Tuple[int, int, int] = (48, 48, 50),
        normalize: Union[Callable, None] = lambda x, tmin, tmax: (x-tmin)/(tmax-tmin),
        device: str = "cpu",
        r_air: float = 287.058,
        tmin: float = 293,
        tmax: float = 586
    ) -> None:
        super().__init__()
        self.re_tau = re_tau
        self.nles = nles
        self.dir_dns = dir_dns
        self.dir_les = dir_les
        self.dir_les_str = str(self.dir_les)

        self.dns_examples = sorted(list(Path(self.dir_dns).glob("T*")))
        self.les_examples = sorted(list(Path(self.dir_les_str).glob("T*")))

        self.transform = transform

        self.normalize = normalize
        self.device = device
        self.r_air = r_air
        self.tmax = tmax
        self.tmin = tmin

    def __len__(self):
        return len(self.dns_examples)

    def preprocess(self, img):
        if self.transform is not None:
            return self.transform(img)
        else:
            return img

    @staticmethod
    def load(filename: Union[str, Path], *, dtype: npt.DTypeLike = np.float32):
        return torch.tensor(np.fromfile(filename, dtype=dtype))

    def __getitem__(self, index: Any):
        # This is a list
        T_dns = torch.from_numpy(np.fromfile(
            self.dns_examples[index])).reshape(*self.nles)

        T_les_path = self.les_examples[index]
        T_les_interp = np.fromfile(
            T_les_path, dtype=np.float64).reshape(*self.nles)
        # Linear interpolation

        T_dns = self.normalize(T_dns, self.tmin, self.tmax)
        T_les_interp = self.normalize(T_les_interp, self.tmin, self.tmax)
        T_les_interp = torch.from_numpy(T_les_interp).float().contiguous()
        out_dns, out_les = self.preprocess((T_dns.float(), T_les_interp))
        return (out_dns.unsqueeze(0).to(self.device),
                out_les.unsqueeze(0).to(self.device))
