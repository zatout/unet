# from utils.laplace import pad_global_field
# from fast_interp.fast_interp import interp3d
from typing import Callable
from torch.nn import functional as F
from matplotlib import pyplot as plt
import numpy as np
from glob import glob

import torch
from torch import Tensor
from os.path import abspath
from pathlib import Path
from glob import glob
from typing import Any, Dict, Tuple, Union, Callable, Type

import numpy as np

from numpy import typing as npt
from torchtyping import TensorType

import sys
import os
import numba
from numba import njit


from scipy.interpolate import RegularGridInterpolator


def sjit(func):
    return numba.njit(func, parallel=False)


def pjit(func):
    return numba.njit(func, parallel=True)


sys.path.append("/home/zatout/Documents/These_yanis/")


def middle_point(ent):
    return (ent[1:] + ent[:-1])/2


def read_p_thermo(filepath: Union[Path, str], *,
                  dtype=Union[npt.DTypeLike]):
    with open(filepath, "r") as file:
        for line in file:
            if "p_thermo_init" in line:
                return np.array(line.split()[1], dtype=dtype)


def normalize(x, min, max):
    return (x - min)/(max-min)


def unnormalize(x, min, max):
    return (x * (max - min)) + min


def shape(x):
    return [i.shape for i in x]


ndns = (384, 384, 266)
nles = (48, 48, 50)
path = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_180/"
path_coords = glob(path+"dns_lata_1.sauv.lata.grid_geom2.coord*")
path_coords.sort()
p_thermo = read_p_thermo(
    path+"dns_lata_0.sauv",
    dtype=np.float32
)
rho = np.fromfile(
    path+"dns_lata_1.sauv.lata.0.RHO",
    dtype=np.float32
)
r_air = 287.058
T = p_thermo/(rho*r_air)
f = T.copy()
T = T.reshape(*ndns, order="F")
coord_vertex = [np.fromfile(g, dtype=np.float32) for g in path_coords]
coord = [middle_point(c) for c in coord_vertex]
coord_les = [np.linspace(i.min(), i.max(), n, endpoint=False,
                         # retstep=True
                         ) for i, n in zip(coord, nles)]
min_val = [c.min() for c in coord]
max_val = [c.max() for c in coord]
steps = [np.diff(c) for c in coord_vertex]
steps_scal = [np.diff(c) for c in coord]
les_3 = np.meshgrid(*coord_les)
les_3 = np.concatenate([ss.ravel()[np.newaxis]
                       for ss in les_3]).T


coord_reg = [np.linspace(c.min(), c.max(), s) for c, s in zip(coord, ndns)]
small_coord = [cg20[:20] for cg20 in coord_reg]


interp = RegularGridInterpolator(coord, T)
T_RGI = interp(les_3).reshape(*nles)
# plt.imshow(T_interp[..., 0])
# plt.show()


# interp = RegularGridInterpolator(coord, T, "cubic")
# T_interp = interp(les_3).reshape(*nles, order="F")

interp = interp3d(min_val,
                  max_val,
                  [steps[0].max(), steps[1].max(), steps[2].max()],
                  T, k=3, p=[True, True, False])
xyz = np.meshgrid(*coord_les, indexing="xy")
T_fastinterp = interp(*[c.flatten() for c in xyz]).reshape(*nles)

ax1 = plt.subplot(221)
tfastinterp_plot = plt.imshow(T_fastinterp[..., 0])
# tfastinterp_plot.set_clim(290, 590)
ax1.set_title('Temperature fastinterp')

ax2 = plt.subplot(222)
trgi_plot = plt.imshow(T_RGI[..., 0])
# trgi_plot.set_clim(290, 590)
ax2.set_title("Temperature RegularGridInterpolator")

ax3 = plt.subplot(212)
t_plot = plt.imshow(T[..., 0])
# t_plot.set_clim(290, 590)
ax3.set_title("Temperature real grid")

# cax = plt.axes()
# plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
plt.colorbar()
plt.show()

# def box_filter_k(size, spacing, k):
#     filter = jnp.zeros(size)
#     lenx, leny = np.diff(spacing[:-1], 0)


def _validate_dims(x, dim):
    if isinstance(x, int):
        return tuple([x for _ in range(3)])
    elif len(x) == 3:
        return x
    raise ValueError("The dimensions of your tensor aren't valid")




size = torch.tensor(5)
spacing = [steps[0].max(), steps[1].max(), torch.tensor(steps[-1])]
k = 0
half = torch.div(size, 2, rounding_mode="floor")
boundary_condition = {1: "periodic", 2: "periodic", 3: "dirichlet"}

ker = box_filter_k_3d(size, spacing, k).unsqueeze(0).unsqueeze(0)
padded = pad_global_field(T, pad, boundary_condition)
padded = padded.unsqueeze(0).unsqueeze(0)
T_cold = F.conv3d(padded[..., 0:5], ker)

plt.figure()
ax1 = plt.subplot(211)
plt.imshow(T_cold[0, 0, ..., 0])
ax1.set_title("Filtered temperature")

ax2 = plt.subplot(212)
t_plot = plt.imshow(T[..., 0])
# t_plot.set_clim(290, 590)
ax2.set_title("Temperature real grid")

# cax = plt.axes()
# plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
plt.colorbar()
plt.show()
