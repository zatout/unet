import matplotlib.pyplot as plt
import os
from datahandling.dataplotter import matplotlib_latex_params


def visualize_x_y_z_slices_n(
    field1,
    field2,
    xslice=0,
    yslice=0,
    zslice=0,
    title=None
):
    fig = plt.figure(figsize=(16, 9))
    ax = plt.subplot(321)
    ax.set_title("cut along i axis")
    plt.imshow(field1[xslice])
    ax = plt.subplot(322)
    plt.imshow(field2[xslice])
    ax.set_title("cut along i axis")

    ax = plt.subplot(323)
    plt.imshow(field1[:, yslice])
    ax.set_title("cut along j axis")
    ax = plt.subplot(324)
    plt.imshow(field2[:, yslice])
    ax.set_title("cut along j axis")

    ax = plt.subplot(325)
    plt.imshow(field1[..., zslice])
    ax.set_title("cut along k axis")
    ax = plt.subplot(326)
    plt.imshow(field2[..., zslice])
    ax.set_title("cut along k axis")

    if title:
        fig.suptitle(title, size=16)


def visualize_x_y_z_slices(
    field1,
    xslice=0,
    yslice=0,
    zslice=0,
    title=None
):
    fig = plt.figure(figsize=(16, 9))
    ax = plt.subplot(311)
    ax.set_title("cut along i axis")
    plt.imshow(field1[xslice])

    ax = plt.subplot(312)
    plt.imshow(field1[:, yslice])
    ax.set_title("cut along j axis")

    ax = plt.subplot(313)
    plt.imshow(field1[..., zslice])
    ax.set_title("cut along k axis")

    if title:
        fig.suptitle(title, size=16)


def visualize_x_y_z_slices_3(
    field1,
    field2,
    field3,
    xslice=0,
    yslice=0,
    zslice=0,
    title=None,
    font_size=16,
    extent=None
):
    if extent:
        extent_i, extent_j, extent_k = extent
    else:
        extent = None
        extent_i, extent_j, extent_k = None, None, None
    fig = plt.figure(figsize=(9, 16))
    ax = plt.subplot(331)
    ax.set_title("cut along i axis", fontsize=font_size)
    plt.imshow(field1[xslice], extent=extent_i)
    ax = plt.subplot(332)
    plt.imshow(field2[xslice], extent=extent_i)
    ax.set_title("cut along i axis", fontsize=font_size)
    ax = plt.subplot(333)
    plt.imshow(field3[xslice], extent=extent_i)
    ax.set_title("cut along i axis", fontsize=font_size)

    ax = plt.subplot(334)
    plt.imshow(field1[:, yslice], extent=extent_j)
    ax.set_title("cut along j axis", fontsize=font_size)
    ax = plt.subplot(335)
    plt.imshow(field2[:, yslice], extent=extent_j)
    ax.set_title("cut along j axis", fontsize=font_size)
    ax = plt.subplot(336)
    ax.set_title("cut along j axis", fontsize=font_size)
    plt.imshow(field3[:, yslice], extent=extent_j)

    ax = plt.subplot(337)
    plt.imshow(field1[..., zslice], extent=extent_k)
    ax.set_title("cut along k axis", fontsize=font_size)
    ax = plt.subplot(338)
    plt.imshow(field2[..., zslice], extent=extent_k)
    ax.set_title("cut along k axis", fontsize=font_size)
    ax = plt.subplot(339)
    ax.set_title("cut along k slice", fontsize=font_size)
    plt.imshow(field3[..., zslice], extent=extent_k)

    if title:
        fig.suptitle(title, size=16)
    plt.tight_layout()





def save_dns_vs_cnn_eval(field1, field2, prefix, title, epoch):
    xslice, yslice, zslice = field1.shape[-3:]
    xslice = xslice//2
    yslice = yslice//2
    zslice = zslice//2
    fig = plt.figure(figsize=(16, 9))
    ax = plt.subplot(321)
    ax.set_title("Z Y plane")
    plt.imshow(field1[..., xslice, :, :])
    ax = plt.subplot(322)
    plt.imshow(field2[..., xslice, :, :])
    ax.set_title("Z Y plane")

    ax = plt.subplot(323)
    plt.imshow(field1[..., yslice, :])
    ax.set_title("X Y plane")
    ax = plt.subplot(324)
    plt.imshow(field2[..., yslice, :])
    ax.set_title("X Y plane")

    ax = plt.subplot(325)
    plt.imshow(field1[..., zslice])
    ax.set_title("X Z plane")
    ax = plt.subplot(326)
    plt.imshow(field2[..., zslice])
    ax.set_title("X Z plane")

    fig.suptitle(
        title,
        size=22
    )
    path = os.path.join(prefix, f"dns_les_slices_epoch_{epoch}.pdf")
    plt.savefig(path, dpi=150)


def save_dns_vs_cnn(field1, field2, prefix, title, epoch):
    xslice, yslice, zslice = field1.shape[-3:]
    xslice = xslice//2
    yslice = yslice//2
    zslice = zslice//2
    fig = plt.figure(figsize=(16, 9))
    ax = plt.subplot(321)
    ax.set_title("Z Y plane")
    plt.imshow(field1[..., xslice, :, :])
    ax = plt.subplot(322)
    plt.imshow(field2[..., xslice, :, :])
    ax.set_title("Z Y plane")

    ax = plt.subplot(323)
    plt.imshow(field1[..., yslice, :])
    ax.set_title("X Y plane")
    ax = plt.subplot(324)
    plt.imshow(field2[..., yslice, :])
    ax.set_title("X Y plane")

    ax = plt.subplot(325)
    plt.imshow(field1[..., zslice])
    ax.set_title("X Z plane")
    ax = plt.subplot(326)
    plt.imshow(field2[..., zslice])
    ax.set_title("X Z plane")

    fig.suptitle(
        title,
        size=22
    )
    path = os.path.join(prefix, f"dns_les_slices_epoch_{epoch}.pdf")
    plt.savefig(path, dpi=150)


def save_log_error_profile(
    normalized_k, profile_interpolation, profile_prediction, profile_ss, path
):

    matplotlib_latex_params(size=25)
    fig = plt.figure(figsize=(16, 9))
    plt.semilogy(
        normalized_k,
        profile_interpolation,
        "x",
        markersize=5,
        label=r"Linear interpolation"
    )

    plt.semilogy(
        normalized_k,
        profile_prediction,
        "-o",
        markersize=3,
        color="purple",
        label=r"CNN"
    )

    plt.semilogy(
        normalized_k,
        profile_ss,
        "-o",
        markersize=3,
        label=r"$T_{\text{recon}} = 2\overline{T} - \overline{\overline{T}}$"
    )

    plt.title("Temperature profile comparaison")
    plt.xlabel("y/2h")
    plt.ylabel(
        r"$ \Big|  \frac{\langle T_{\text{recon}}\rangle - \langle  T_{\text{DNS}} \rangle }{ \max(T_{\text{DNS}}) } \Big|  $"
    )
    leg = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                     fancybox=True, shadow=True, ncol=5)
    plt.grid(True)

    savepath = os.path.join(path, "error_profiles.pdf")
    plt.savefig(savepath,
                bbox_extra_artists=(leg,), bbox_inches='tight', dpi=150)


def save_rms_profile(
    normalized_dns_coord,
    rms_profile_lin_interp,
    rms_profile_pred,
    rms_profile_scale_sim,
    rms_profile_dns,
    path
):
    matplotlib_latex_params(size=25)
    fig = plt.figure(figsize=(16, 9))
    plt.plot(
        normalized_dns_coord,
        rms_profile_lin_interp,
        "x",
        markersize=5,
        label=r"Linear interpolation"
    )

    plt.plot(
        normalized_dns_coord,
        rms_profile_pred,
        "-o",
        markersize=3,
        color="purple",
        label=r"CNN"
    )

    plt.plot(
        normalized_dns_coord,
        rms_profile_scale_sim,
        "-o",
        markersize=3,

        label=r"$T_{\text{recon}} = 2\overline{T} - \overline{\overline{T}}$"
    )

    plt.plot(
        normalized_dns_coord,
        rms_profile_dns,
        "-o",
        markersize=3,
        label=r"DNS"
    )

    plt.title("RMS comparaison")
    plt.xlabel("y/2h")
    plt.ylabel(r"$  \sqrt{\langle T^{'2} \rangle}$")
    leg = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                     fancybox=True, shadow=True, ncol=5)

    plt.grid(True)
    savepath = os.path.join(path, "RMS_profiles.pdf")
    plt.savefig(
        savepath,
        bbox_extra_artists=(leg,),
        bbox_inches='tight',
        dpi=150
    )


def save_rms_lin_interp_cnn(
    normalized_dns_coord,
    rms_profile_lin_interp,
    rms_profile_pred,
    path


):
    matplotlib_latex_params(size=25)
    fig = plt.figure(figsize=(16, 9))
    plt.semilogy(
        normalized_dns_coord,
        rms_profile_lin_interp,
        "x",
        markersize=5,
        label=r"Linear interpolation"
    )

    plt.semilogy(
        normalized_dns_coord,
        rms_profile_pred,
        "o",
        markersize=3,
        color="purple",
        label=r"CNN"
    )

    plt.title("RMS comparaison of CNN and linear interpolation")
    plt.xlabel("y/2h")
    plt.ylabel(r"$  \sqrt{\langle T^{'2} \rangle}$")
    leg = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                     fancybox=True, shadow=True, ncol=5)

    plt.grid(True)
    savepath = os.path.join(path, "rms_lin_interp_cnn.pdf")
    plt.savefig(
        savepath,
        bbox_extra_artists=(leg,), bbox_inches='tight', dpi=150
    )


def save_grad_profile(
    normalized_dns_coord,
    gradient_profile_lin_interp,
    gradient_profile_pred,
    gradient_profile_scale_sim,
    path
):

    matplotlib_latex_params(size=25)
    for lin_interp, nn, ss, grad in zip(
        gradient_profile_lin_interp,
        gradient_profile_pred,
        gradient_profile_scale_sim,
        ["x", "z", "y"]
    ):
        fig = plt.figure(figsize=(16, 9))
        plt.semilogy(
            normalized_dns_coord,
            lin_interp,
            "x",
            markersize=4,
            label=r"Linear interpolation"
        )

        plt.semilogy(
            normalized_dns_coord,
            nn,
            "-o",
            markersize=1,
            color="purple",
            label=r"CNN"
        )

        plt.semilogy(
            normalized_dns_coord,
            ss,
            "-o",
            markersize=1,

            label=r"$T_{\text{recon}} = 2\overline{T} - \overline{\overline{T}}$"
        )

        plt.xlabel(r"y/2h")
        plt.ylabel(r"$\Big|  \frac{\langle \frac{\partial}{\partial "+grad+r"} T_{\text{recon}} \rangle _{XZ} - \langle \frac{\partial}{\partial " +
                   grad+r"} T_{\text{DNS}} \rangle _{XZ} }{ \max( \frac{\partial}{\partial "+grad+r"} T_{\text{DNS}} ) } \Big|$")
        plt.grid(True)
        leg = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                         fancybox=True, shadow=True, ncol=5)
        plt.title("Gradient error in the " + grad + " direction")
        savepath = os.path.join(path, f"gradient_error_profile_{grad}.pdf")
        plt.savefig(
            savepath,
            bbox_extra_artists=(leg,),
            bbox_inches='tight',
            dpi=150
        )


def save_rms_grad(
    normalized_dns_coord,
    rms_grad_dns,
    rms_grad_interp,
    rms_grad_prediction,
    rms_grad_ss,
    path
):
    matplotlib_latex_params(size=25)
    for rms_g_dns, lin_interp, nn, ss, grad in zip(
        rms_grad_dns,
        rms_grad_interp,
        rms_grad_prediction,
        rms_grad_ss,
        ["x", "z", "y"]
    ):
        fig = plt.figure(figsize=(16, 9))
        plt.semilogy(
            normalized_dns_coord,
            lin_interp,
            "x",
            markersize=4,
            label=r"Linear interpolation"
        )
        plt.semilogy(
            normalized_dns_coord,
            nn,
            "-o",
            color="purple",
            markersize=1,
            label=r"CNN"
        )

        plt.semilogy(
            normalized_dns_coord,
            ss,
            "-o",
            markersize=1,
            label=r"$T_{\text{recon}} = 2\overline{T} - \overline{\overline{T}}$"
        )

        plt.semilogy(
            normalized_dns_coord,
            rms_g_dns,
            "-o",
            markersize=1,
            label=r"DNS"
        )

        plt.xlabel(r"y/2h")
        plt.ylabel(
            r"$ \langle \sqrt{\frac{\partial}{\partial " +
            grad+r"} T ^{'2} \rangle} $"
        )
        plt.grid(True)
        leg = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                         fancybox=True, shadow=True, ncol=5)
        plt.title("Gradient RMS " + grad+" direction profiles")

        savepath = os.path.join(path, f"rms_grad_{grad}.pdf")
        plt.savefig(
            savepath,
            bbox_extra_artists=(leg,),
            bbox_inches='tight',
            dpi=150
        )
