from .blur import weighted_convolution, weighted_convolution_gaussian
import numpy as np
from scipy.linalg import pascal


def scale_sim_recon(T, coord, filter_size, *, orders=6, filter="box"):
    filter_func = lambda x, y, z: x*y*z
    if filter == "box":
        filter_func = weighted_convolution
    if filter == "gaussian":
        filter_func = weighted_convolution_gaussian
    T_filtered = [T]
    out = np.zeros_like(T)
    pascal_matrix = pascal(orders, "lower").astype("float32")
    pascal_matrix[..., 1::2] = -pascal_matrix[..., 1::2]
    pascal_matrix = pascal_matrix.sum(0)
    for _ in range(orders - 1):
        T_filtered.append(filter_func(T_filtered[-1], coord, filter_size))
    for t, coef in zip(T_filtered, pascal_matrix):
        out += coef * t

    return np.array(out)
