"""
Gaussian blur stuff
"""
# TODO: Change the spacing so as to take
# a local maximum and not just the maximum
# in each directions
# from scipy.ndimage import gaussian_filter
from numba import njit
from .laplace import pad_dirichlet_end
import numpy as np
import torch

# from torch import nn
# from torch.autograd import Function
from torch import Tensor, diff, tensor

# from torch.autograd import forward_ad
from torch.nn import Module
from typing import List, Union, Tuple, Dict
from torch.nn import functional as F
from .laplace import pad_global_field


def _is_tensor_a_torch_image(x: Tensor) -> bool:
    return x.ndim >= 2


def _assert_image_tensor(img: Tensor) -> None:
    if not _is_tensor_a_torch_image(img):
        raise TypeError("Tensor is not a torch image.")


def get_dimensions3d(img: Tensor) -> List[int]:
    _assert_image_tensor(img)
    channels = 1 if img.ndim == 2 else img.shape[-4]
    height, width, depth = img.shape[-3:]
    return [channels, height, width, depth]


def centered_identity_map(sz, spacing, dtype="float32"):
    """
    Returns a centered identity map (with 0 in the middle) if the sz is odd
    Otherwise shifts everything by 0.5*spacing
    :param sz: just the spatial dimensions, i.e., XxYxZ
    :param spacing: list with spacing information [sx,sy,sz]
    :param dtype: numpy data-type ('float32', 'float64', ...)
    :return: returns the identity map of dimension dimxXxYxZ
    """
    # https://github.com/uncbiag/mermaid/blob/master/mermaid/utils.py
    dim = len(sz)
    if dim == 1:
        id = np.mgrid[0 : sz[0]]
    elif dim == 2:
        id = np.mgrid[0 : sz[0], 0 : sz[1]]
    elif dim == 3:
        id = np.mgrid[0 : sz[0], 0 : sz[1], 0 : sz[2]]
    else:
        raise ValueError(
            "Only dimensions 1-3 are currently supported for the identity map"
        )

    # now get it into range [0,(sz-1)*spacing]^d
    id = np.array(id.astype(dtype))
    if dim == 1:
        id = id.reshape(1, sz[0])  # add a dummy first index

    for d in range(dim):
        id[d] *= spacing[d]
        if sz[d] % 2 == 0:
            # even
            id[d] -= spacing[d] * (sz[d] // 2)
        else:
            # odd
            id[d] -= spacing[d] * ((sz[d] + 1) // 2)

    # and now store it in a dim+1 array
    if dim == 1:
        idnp = np.zeros([1, sz[0]], dtype=dtype)
        idnp[0, :] = id[0]
    elif dim == 2:
        idnp = np.zeros([2, sz[0], sz[1]], dtype=dtype)
        idnp[0, :, :] = id[0]
        idnp[1, :, :] = id[1]
    elif dim == 3:
        idnp = np.zeros([3, sz[0], sz[1], sz[2]], dtype=dtype)
        idnp[0, :, :, :] = id[0]
        idnp[1, :, :, :] = id[1]
        idnp[2, :, :, :] = id[2]
    else:
        raise ValueError(
            "Only dimensions 1-3 are "
            "currently supported for the centered identity map"
        )

    return idnp


def centered_identity_map_3d_torch(
    sp_dim: Union[Tuple[int, int, int], List[int], int],
    spacing: Union[Tuple[float, float, float], List[float], float],
    dtype=torch.float32,
) -> Tensor:
    """
    Returns a centered identity map with 0 in the middle if sp_dim is odd.
    Otherwise shifts everything by 0.5 \cdot spacing
    Parameters:
    -----------
    sp_dim: Union[Tuple[float], float]:
        spacial dimensions, number of points you want in each dim
    spacing: Union[Tuple[float], float]
        spacing between each points

    Returns:
    -----------
    mapping: torch.Tensor
        mapping of distance to each point, the center being 0 when the
        number of points is odd
    """

    dim = 3
    # We format the spacial dims and spacing well
    if isinstance(sp_dim, int):
        sp_dim = [sp_dim for _ in range(3)]
    if isinstance(spacing, (float, int)):
        spacing = [spacing for _ in range(3)]
    # We initialize the meshgrid in each of the 3 dimensions
    grid = np.mgrid[0 : sp_dim[0], 0 : sp_dim[1], 0 : sp_dim[2]]
    grid = torch.tensor(grid, dtype=dtype)
    for d in range(dim):
        grid[d] = grid[d] * spacing[d]
        if sp_dim[d] % 2 == 0:
            grid[d] -= spacing[d] * (sp_dim[d] // 2)
        else:
            grid[d] -= spacing[d] * ((sp_dim[d] + 1) // 2)

    final_grid = torch.zeros([3, *sp_dim], dtype=grid.dtype)
    # We now store it in a (3, *grid.shape) array
    final_grid[0] = grid[0]
    final_grid[1] = grid[1]
    final_grid[2] = grid[2]
    return final_grid


def compute_3d_gauss_LES_filter(
    kernel_size: Union[Tuple[int, int, int], List[int], int],
    spacing: Union[Tuple[float, float, float], List[float], float],
    filter_size: Union[Tuple[float, float, float], List[float], float, Tensor],
    gamma: float = 6,
    dtype=torch.float32,
):
    """
    Computes the 3d Gauss LES filter as given in Sagaut's book

    Parameters:
    ----------
    kernel_size: Union[Tuple[int, int, int], int]
        size of the kernel in memory, as (x, y, z) or just (x, x, x)
    spacing: Union[Tuple[float, float, float], float]
        spacing in the 3 dimensions of space
    gamma: float
        constant given in Sagault's book
    filter_size: Union[Tuple[float, float, float], float],
        physical size of cutoff filter in the 3 dimensions
        given as \overbar{\delta} = \max \overbar{\delta_x},
        \overbar{\delta_x}, \overbar{\delta_z}
    dtype:
        dtype to take when generating the filter

    Returns:
    ----------
    filter
    """
    identity_map: Tensor = centered_identity_map_3d_torch(
        sp_dim=kernel_size, spacing=spacing, dtype=dtype
    )
    filter: Tensor = torch.exp(
        -((identity_map**2).sum(axis=0) * gamma) / (filter_size**2)
    )
    filter = filter / filter.sum()
    return filter


# NOTE: Needs more testing/engineering


def compute_3d_gauss_LES_filter_uneven_grid(
    kernel_size: Union[Tuple[int, int, int], List[int], int],
    spacing: Union[Tuple[float, float, float], List[float]],
    filter_size: Union[Tuple[float, float, float], List[float], Tensor],
    gamma: float = 6,
    dtype=torch.float32,
):
    """
    Computes the 3d Gauss LES filter as given in Sagaut's book for an un-even
    grid

    Parameters:
    ----------
    kernel_size: Union[Tuple[int, int, int], int]
        size of the kernel in memory, as (x, y, z) or just (x, x, x)
    spacing: Union[Tuple[float, float, float], float]
        spacing in the 3 dimensions of space
    gamma: float
        constant given in Sagault's book
    filter_size: Union[Tuple[float, float, float], float],
        physical size of cutoff filter in the 3 dimensions
        given as \overbar{\delta} = \max \overbar{\delta_x},
        \overbar{\delta_x}, \overbar{\delta_z}
    dtype:
        dtype to take when generating the filter

    Returns:
    ----------
    filter
    """
    identity_map: Tensor = centered_identity_map_3d_torch(
        sp_dim=kernel_size, spacing=spacing, dtype=dtype
    )
    fltr = -gamma * (
        identity_map[0] ** 2 / filter_size[0] ** 2
        + identity_map[1] ** 2 / filter_size[1] ** 2
        + identity_map[2] ** 2 / filter_size[2] ** 2
    )

    filter: Tensor = torch.exp(fltr)
    filter = filter / filter.sum()
    return filter


def filter_dns_3d(dns_field, filter):
    """
    This function filteres a DNS with a certain filter, according to what's
    written in Sagaut's book. Currently only works for Gaussian filters

    Parameters:
    ----------

    Returns:
    ----------
    """
    return F.conv3d(dns_field, filter)


class GaussianBlur(Module):
    """
    Gaussian blur class: For a family of DNS simulation fields, gives the
    filtered DNS as presented in Sagaut's book (Large Eddy Simulation for
    Incompressible Flows)

    """

    def __init__(
        self,
        kernel_size: Union[Tuple[int, int, int], int],
        spacing: Union[Tuple[float, float, float], float],
        filter_size: Union[Tuple[float, float, float], List[float], float, Tensor],
        gamma: float = 6,
        dtype=torch.float32,
    ) -> None:
        """
        Parameters:
        ----------
        kernel_size: Union[Tuple[int, int, int], int],
            Size of the kernel taken for the convolution
        spacing: Union[Tuple[float, float, float], float],
            Spacial spacing in 3D of the simulation
        filter_size: Union[Tuple[float, float, float], float],
            Size of the spacial filter. This is supposed to be a multiple of
            max(spacing)
        gamma: float
            parameter of the gaussian filter defined as:
            `G(x, \gamma) = \sqrt{\frac{\gamma}{\pi \overbar{\Delta}^2}}
            exp(\frac{-\gamma x^2}{\overbar{\Delta}^2})`

        Returns:
        --------
        None
        """
        super().__init__()
        self.filter = compute_3d_gauss_LES_filter(
            kernel_size=kernel_size,
            spacing=spacing,
            filter_size=filter_size,
            gamma=gamma,
            dtype=dtype,
        )

    def forward(self, dns_field) -> Tensor:
        return filter_dns_3d(dns_field=dns_field, filter=self.filter)


@torch.jit.script
def bordering_k(size: Tensor, lk: int, k: int) -> bool:
    """
    Returns True if filter will go out of image on border
    False otherwise
    Parameters:
    -----------
    size: Union[int, float]
        Size of the filter, supposed to be constant in
        the 3 dimensions of space
    lk: Union[int, float]
        Size of the spacing tensor, or the number of
        points in the axis k
    k: int
        Position in k
    Returns:
    --------
    out: bool
        Whether or not we're out of the axis
    """
    half = torch.div(size, 2, rounding_mode="floor")
    return (k - half < 0) or (k + half + 1 > lk)


@torch.jit.script
def select_k_len(size: Tensor, lk: Tensor, k: int) -> Tensor:
    """
    This function gives the length of the filter for a
    certain given k
    Parameters:
    -----------
    size: Tensor
        Size of the filter, supposed to be constant in
        the 3 dimensions of space
    lk: Tensor
        Size of the spacing tensor, or the number of
        points in the axis k
    k: int
        Position in axis k
    Returns:
    --------
    out: Tensor
        Length of space for axis k
    """
    lenlk = lk.shape[0]
    half = torch.div(size, 2, rounding_mode="floor")
    if not bordering_k(size, lenlk, k):
        return lk[k - half : k + half + 1]
    else:
        if k - half < 0:
            n_outside = int(torch.abs(torch.tensor(k - half)))
            n_inside = size - n_outside
            n_inside = int(n_inside)
            out = torch.empty(int(size))
            out[:n_outside] = lk[0]
            out[n_outside:] = lk[0:n_inside]
        else:
            n_outside = int(-lenlk + k + 1 + half)
            n_inside = size - n_outside
            n_inside = int(n_inside)
            out = torch.empty(int(size))
            out[:n_outside] = lk[0]
            out[n_outside:] = lk[0:n_inside]
            out = torch.flip(out, (0,))
        return out


@torch.jit.script
def box_filter_k_3d(
    size: Tensor,
    spacing: Tuple[
        float,
        float,
        Tensor,
    ],
    k: int,
) -> Tensor:
    """
    Generates the box filter in 3d such that the filter will be the following:
    kernel = (1/volume_space(i, j, k)) * ones((size, size, size))
    Parameters:
    -----------
    size: Union[int, float]
        Size of the filter, supposed to be constant in
        the 3 dimensions of space
    spacing: Tensor
        Spacing in 3d for the 3 axes of space
    k: int
        Position in axis k
    Returns:
    --------
    out: Tensor
        Box filter in 3d
    """
    li = spacing[0]
    lj = spacing[1]
    li = size * li
    lj = size * lj
    lk = select_k_len(size, spacing[-1], k)
    filter_width = torch.pow(torch.tensor((size * li * size * lj * size * lk)), 1 / 3)
    out = torch.ones(size, size, size, dtype=torch.float64) / filter_width
    return out / torch.sum(out)


def box_filter_dorian(size, coord, coord_vertex, spacing, k):
    """
    Filter such that
    \overbar{\phi} = 1/(size**2 * (y_{i+size/2 + 1} - y_{i-size/2})) *
        \sum_{i', j', k'=i-size//2, j-size//2, k-size//2}^{i+size//2, j+size//2, k+size//2}
        \phi(i', j', k')(y_{k'+1} - y_{k'})
    """

    try:
        coord = [torch.from_numpy(c) for c in coord]
    except TypeError:
        coord = [torch.tensor(c) for c in coord]
    half = torch.div(size, 2, rounding_mode="floor")
    li = spacing[0]
    lj = spacing[1]
    li = size * li
    lj = size * lj
    pad = half
    out = torch.ones(size, size, size, dtype=torch.float64)
    if not bordering_k(size, len(coord[-1]), k):
        for i in range(-pad, pad + 1):
            out[..., i + pad] *= coord_vertex[-1][i + k + 1] - coord_vertex[-1][i + k]
        return out / (
            size**2 * (coord_vertex[-1][k + pad + 1] - coord_vertex[-1][k - pad])
        )
    else:
        spacing = [torch.tensor(s) for s in spacing]
        lk = spacing[-1]
        left_pad = [lk[0] for _ in range(int(half))]
        right_pad = [lk[-1] for _ in range(int(half))]
        lk = torch.tensor([*left_pad, *lk, *right_pad])
        if k - half < 0:
            # print("left border")
            n_outside = torch.abs(torch.tensor(k) - half)
            n_inside = size - n_outside
            last_diff = coord_vertex[-1][1] - coord_vertex[-1][0]
            cc = [
                *[coord_vertex[-1][0] - i * last_diff for i in range(1, 1 + n_outside)][
                    ::-1
                ],
                *coord_vertex[-1],
            ]
            # for i in range(-pad, pad + 1):
            #     out[..., i + pad] *= lk[i + pad + k]
            for i in range(-pad, pad + 1):
                out[..., i + pad] *= np.diff(cc[: size + 1])[i + pad]
            return out / (size**2 * (np.sum(np.diff(cc[: size + 1]))))
        else:
            # print("right border")
            n_outside = -len(lk) + 1 + k + half
            n_inside = size - n_outside
            # dist_to_border = len(lk) - k - 1
            last_diff = coord_vertex[-1][1] - coord_vertex[-1][0]
            cc = [
                *coord_vertex[-1],
                *[
                    coord_vertex[-1][-1] + i * last_diff
                    for i in range(1, 1 + n_outside)
                ],
            ]
            # for i in range(-pad, pad + 1):
            #     out[..., i + pad] *= cc[i+pad+kk] - cc[i+pad]
            for i in range(-pad, pad + 1):
                out[..., i + pad] *= np.diff(cc[-size - 1 :])[i + pad]
            # lk[i + pad + kk]
            out = out / (size**2 * (np.sum(np.diff(cc[-size - 1 :]))))

            return out


def convolve_strided(
    img: Tensor,
    filter_size: Tensor,
    coord,
    coord_vertex,
    spacing: Tuple[float, float, Tensor],
) -> Tensor:
    """
    Performs convolution over strided image
    """
    filter_func = box_filter_dorian
    padded = img.unsqueeze(0).unsqueeze(0)
    half = torch.div(filter_size, 2, rounding_mode="floor")
    out = torch.empty_like(img).unsqueeze(0).unsqueeze(0)
    inner = slice(half, -half)
    # We have to add a layer of temperature field equal to the last height
    # if we want to filter the right way
    padded = pad_dirichlet_end(padded, 1, -1)
    for i in range(len(coord[-1])):
        # ii = i + half
        padded_slice = padded[..., i : i + 2 * half + 1]
        filter_ = (
            filter_func(filter_size, coord, coord_vertex, spacing, i)
            .unsqueeze(0)
            .unsqueeze(0)
        )
        filtered_slice = torch.nn.functional.conv3d(padded_slice, filter_)
        out[..., inner, inner, i : i + 1] = filtered_slice
    return out[..., inner, inner, inner]


@torch.jit.script
def box_filter_regular_3d(
    filter_size: int,
):
    """
    Returns 3d box filter for regular grid
    Filter width = \sqrt[3]{\overbar{\Delta_x}\overbar{\Delta_y}\overbar{\Delta_z}}
    """

    filter = torch.ones(
        filter_size, filter_size, filter_size, dtype=torch.float64
    )  # / filter_width
    return filter / torch.sum(filter)


def convolve_box_kernel_regular(
    img: Tensor,
    filter_size: Tensor,
    boundary_cond: Dict = {1: "periodic", 2: "periodic", 3: "dirichlet"},
):
    box_filter = box_filter_regular_3d(filter_size).unsqueeze(0).unsqueeze(0)
    half = torch.div(filter_size, 2, rounding_mode="floor")
    padded = (
        pad_global_field(img, half, boundary_cond).unsqueeze(0).unsqueeze(0).double()
    )
    return torch.nn.functional.conv3d(padded, box_filter)[0, 0].numpy()


def convolve_box_kernel_k(
    img: Tensor,
    filter_size: Tensor,
    coord,
    coord_vertex,
    spacing: Tuple[float, float, Tensor],
    boundary_cond: Dict = {1: "periodic", 2: "periodic", 3: "dirichlet"},
):
    """
    Strides image then convolves it thanks to jitted function
    """
    half = torch.div(filter_size, 2, rounding_mode="floor")
    padded = pad_global_field(img, half, boundary_cond)
    return convolve_strided(padded, filter_size, coord, coord_vertex, spacing)[
        0, 0
    ].numpy()


def filter_k(size, coord_vertex, k):
    """
    Gives the filter in the k direction centered on the k_th cell
    """
    filter_ = np.ones(size, dtype=np.float64)
    half = size // 2
    diff = np.diff(coord_vertex[-1])
    padded_diff = np.pad(diff, (half, half), "edge")
    filter_ *= padded_diff[k : k + 2 * half + 1]
    filter_ /= np.sum(padded_diff[k : k + 2 * half + 1])
    return filter_


def select_centered_k(x, k, size):
    """
    Select slice along k for a certain width centered around k
    """
    return x[..., k - size // 2 - 1 : k + size // 2]


def convolve_numpy_constant_k_pad(
    img,
    filter_size,
    coord_vertex,
    boundary_cond: Dict = {1: "periodic", 2: "periodic", 3: "dirichlet"},
    tmin=283,
    tmax=586,
):
    from scipy.signal import convolve

    if isinstance(filter_size, int):
        filter_size = [filter_size for _ in range(3)]
        filter_size_i, filter_size_j, filter_size_k = filter_size
    else:
        filter_size_i, filter_size_j, filter_size_k = filter_size
    # half = filter_size//2
    pad_i = "wrap" if boundary_cond[1] == "periodic" else "edge"
    pad_j = "wrap" if boundary_cond[2] == "periodic" else "edge"
    padded = np.pad(
        img, ((filter_size_i // 2, filter_size_i // 2), (0, 0), (0, 0)), pad_i
    )
    padded = np.pad(
        padded, ((0, 0), (filter_size_j // 2, filter_size_j // 2), (0, 0)), pad_j
    )
    padded = np.pad(
        padded,
        ((0, 0), (0, 0), (filter_size_k // 2, filter_size_k // 2)),
        "constant",
        constant_values=(tmin, tmax),
    )
    # padded = pad_global_field(img, half, boundary_cond)
    filter_i = 1 / filter_size_i * np.ones(filter_size_i).reshape(filter_size_i, 1, 1)
    filter_j = 1 / filter_size_j * np.ones(filter_size_j).reshape(1, filter_size_j, 1)
    fi = convolve(padded, filter_i, "valid")
    fj = convolve(fi, filter_j, "valid")
    fk = np.empty_like(img)

    for i in range(img.shape[-1]):
        filter__ = filter_k(filter_size_k, coord_vertex, i).reshape(1, 1, filter_size_k)
        fk[..., i : i + filter_size_k // 2] = convolve(
            fj[..., i : i + filter_size_k], filter__, "valid", "direct"
        )

    return fk


def convolve_numpy(
    img,
    filter_size,
    coord_vertex,
    boundary_cond: Dict = {1: "periodic", 2: "periodic", 3: "dirichlet"},
):
    from scipy.signal import convolve

    if isinstance(filter_size, int):
        filter_size = [filter_size for _ in range(3)]
        filter_size_i, filter_size_j, filter_size_k = filter_size
    else:
        filter_size_i, filter_size_j, filter_size_k = filter_size
    # half = filter_size//2
    pad_i = "wrap" if boundary_cond[1] == "periodic" else "edge"
    pad_j = "wrap" if boundary_cond[2] == "periodic" else "edge"
    pad_k = "wrap" if boundary_cond[3] == "periodic" else "edge"
    padded = np.pad(
        img, ((filter_size_i // 2, filter_size_i // 2), (0, 0), (0, 0)), pad_i
    )
    padded = np.pad(
        padded, ((0, 0), (filter_size_j // 2, filter_size_j // 2), (0, 0)), pad_j
    )
    padded = np.pad(
        padded, ((0, 0), (0, 0), (filter_size_k // 2, filter_size_k // 2)), pad_k
    )
    # padded = pad_global_field(img, half, boundary_cond)
    filter_i = 1 / filter_size_i * np.ones(filter_size_i).reshape(filter_size_i, 1, 1)
    filter_j = 1 / filter_size_j * np.ones(filter_size_j).reshape(1, filter_size_j, 1)
    fi = convolve(padded, filter_i, "valid")
    fj = convolve(fi, filter_j, "valid")
    fk = np.empty_like(img)

    for i in range(img.shape[-1]):
        filter__ = filter_k(filter_size_k, coord_vertex, i).reshape(1, 1, filter_size_k)
        fk[..., i : i + filter_size_k // 2] = convolve(
            fj[..., i : i + filter_size_k], filter__, "valid", "direct"
        )

    return fk


def convolve_3_numpy(
    img,
    filter_size,
    coord_vertex,
    boundary_cond: Dict = {1: "periodic", 2: "periodic", 3: "dirichlet"},
):
    half = filter_size // 2
    padded = pad_global_field(img, half, boundary_cond).numpy()
    diff_padded = np.pad(np.diff(coord_vertex[-1]), (1, 1), "edge")
    fi = (padded[2:] + padded[1:-1] + padded[:-2]) / 3
    fj = (fi[:, 2:] + fi[:, 1:-1] + fi[:, :-2]) / 3
    fk = (
        fj[..., 2:] * diff_padded[2:]
        + fj[..., 1:-1] * diff_padded[1:-1]
        + fj[..., :-2] * diff_padded[:-2]
    ) / (diff_padded[2:] + diff_padded[1:-1] + diff_padded[:-2])
    return fk


def pad(x, size):
    sizex, sizey, sizez = size
    x = np.pad(x, ((sizex // 2, sizex // 2), (0, 0), (0, 0)), "wrap")
    x = np.pad(
        x,
        (
            (0, 0),
            (
                sizey // 2,
                sizey // 2,
            ),
            (0, 0),
        ),
        "wrap",
    )
    x = np.pad(x, ((0, 0), (0, 0), (sizez // 2, sizez // 2)), "edge")
    return x


def pad_no_y(x, size):
    sizex, sizey, sizez = size
    x = np.pad(x, ((sizex // 2, sizex // 2), (0, 0), (0, 0)), "wrap")
    x = np.pad(
        x,
        (
            (0, 0),
            (
                sizey // 2,
                sizey // 2,
            ),
            (0, 0),
        ),
        "wrap",
    )
    return x


def weighted_convolution(x, coord_face, size):
    """
    Weighted convolution for filtering DNS time steps
    """
    import scipy.signal as si

    sx, sy, sz = size
    out = np.zeros_like(x)

    padded = pad(x, size)
    filterx = si.get_window("boxcar", sx).reshape(-1, 1, 1)
    filterx /= filterx.sum()
    filtery = si.get_window("boxcar", sy).reshape(1, -1, 1)
    filtery /= filtery.sum()

    padded = si.convolve(padded, filterx, "valid")
    padded = si.convolve(padded, filtery, "valid")
    cell_size = np.diff(coord_face[-1])
    cell_size = np.pad(cell_size, (sz // 2, sz // 2), "edge")
    for k in range(x.shape[-1]):
        kernel = cell_size[k : k + sz] / cell_size[k : k + sz].sum()
        out[..., k : k + 1] = si.convolve(
            padded[..., k : k + sz], kernel[None, None], "valid"
        )
    return out


def parallel_filtering(item):
    """
    Parallel version of above function
    """
    temperature_interp, file_path, coord_face = item
    T_filtered_LES = weighted_convolution(temperature_interp, coord_face, (21, 21, 15))
    T_filtered_LES.tofile("les/les_" + file_path.split("./dns/dns_lata_")[-1])


def get_gaussian_filter_k(k, size, coord_elem, delta, gamma=6):
    coord_window = coord_elem[k : k + size]
    one_over_delta_sq = 1 / (delta**2)
    filter_center = (coord_window[-1] + coord_window[0]) / 2
    return np.exp(-gamma * (filter_center - coord_window) ** 2 * one_over_delta_sq)


def get_gaussian_filter_k_no_pad_y(k, size, coord_elem, delta, gamma=6):
    coord_window = np.unique(coord_elem[k : k + size])
    size_window = coord_window.shape[0]
    one_over_delta_sq = 1 / (delta**2)
    filter_center = (coord_window[-1] + coord_window[0]) / 2
    return np.exp(-gamma * (filter_center - coord_window) ** 2 * one_over_delta_sq)


def shift_left_right_boundary(k, size, nk):
    n_inside_left = k - size // 2
    inside_boundary_left = (n_inside_left < 0) * -n_inside_left
    n_inside_right = ((nk - 1) - k) - size // 2
    inside_boundary_right = (n_inside_right < 0) * -n_inside_right
    return inside_boundary_left, inside_boundary_right


def weighted_convolution_gaussian(
    x, coord_elem, size, *, y_pad=True, gamma=6, kernel_type="gaussian"
):
    import scipy.signal as si

    if isinstance(size, int):
        size = (size,) * 3
    sx, sy, sz = size
    # print(size)
    out = np.zeros_like(x)
    if y_pad:
        gaussian = get_gaussian_filter_k
        padded = pad(x, size)
    else:
        gaussian = get_gaussian_filter_k_no_pad_y
        padded = pad_no_y(x, size)
        padded = np.pad(padded, ((0, 0), (0, 0), (sz // 2, sz // 2)), "constant")
    if kernel_type == "gaussian":
        filter_x = si.windows.gaussian(M=sx, std=sx / np.sqrt(gamma * 2)).reshape(
            -1, 1, 1
        )
        filter_x /= filter_x.sum()
        filter_y = si.windows.gaussian(M=sy, std=sy / np.sqrt(gamma * 2)).reshape(
            1, -1, 1
        )
        filter_y /= filter_y.sum()

    cell_size = np.diff(coord_elem)
    delta_bar = cell_size[cell_size.shape[0] // 2]

    padded = si.convolve(padded, filter_x, "valid")
    padded = si.convolve(padded, filter_y, "valid")
    coord_elem_pad = np.pad(coord_elem, (sz // 2, sz // 2), "edge")

    for k in range(x.shape[-1]):
        kernel = gaussian(k, sz, coord_elem_pad, delta_bar, gamma=gamma)
        kernel /= kernel.sum()
        kernel = kernel.reshape(1, 1, -2)
        window_size = kernel.shape[-1]
        out[..., k : k + 1] = si.convolve(
            padded[..., k : k + window_size], kernel, "valid"
        )

    return out
