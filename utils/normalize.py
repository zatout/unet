from typing import Union, Tuple
from torch import Tensor, nn


def normalize_max(x, max):
    return x / max


class NormalizeVal(nn.Module):
    def __init__(self, val) -> None:
        super().__init__()
        self.val = val

    def forward(self, x: Union[Tuple[Tensor], Tensor]) -> Union[Tuple[Tensor], Tensor]:
        if isinstance(x, tuple):
            out = tuple([normalize_max(n, self.val) for n in x])
            return out
        else:
            return normalize_max(x, self.val)


class NormalizeMax(nn.Module):
    def __init__(self) -> None:
        super().__init__()

    def forward(self, x: Union[Tuple[Tensor], Tensor]) -> Union[Tuple[Tensor], Tensor]:
        if isinstance(x, tuple):
            out = tuple([normalize_max(n, n.max()) for n in x])
            return out
        else:
            return normalize_max(x, x.max())


def normalize(x, min, max):
    return ((x - min) / (max - min) - 0.5) * 2


def unnormalize(x, min, max):
    return (x / 2 + 0.5) * (max - min) + min


def normalize0(x, tmin, tmax):
    return (x - tmin) / (tmax - tmin)


def unnormalize0(x, tmin, tmax):
    return x * (tmax - tmin) + tmin


def mmm(x):
    return torch.min(x), torch.max(x), torch.mean(x)
