"""
Random flips in 3d
Classes are made for pairs or lone images
"""
from torch import Tensor
from torchvision.utils import _log_api_usage_once
import torch
from . import crop


def hflip(img: Tensor) -> Tensor:
    crop._assert_image_tensor(img)
    return img.flip(-3)


def vflip(img: Tensor) -> Tensor:
    crop._assert_image_tensor(img)
    return img.flip(-2)


def dflip(img: Tensor) -> Tensor:
    crop._assert_image_tensor(img)
    return img.flip(-1)


class RandomHorizontalFlip(torch.nn.Module):
    """Vertically flip the given image randomly with a given probability.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading
    dimensions

    Args:
        p (float): probability of the image being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        super().__init__()
        _log_api_usage_once(self)
        self.p = p

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be flipped.

        Returns:
            PIL Image or Tensor: Randomly flipped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            if torch.rand(1) < self.p:
                return hflip(img), hflip(img2)
            return img, img2
        else:
            if torch.rand(1) < self.p:
                return hflip(img)
            return img

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(p={self.p})"


class RandomVerticalFlip(torch.nn.Module):
    """Vertically flip the given image randomly with a given probability.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading
    dimensions

    Args:
        p (float): probability of the image being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        super().__init__()
        _log_api_usage_once(self)
        self.p = p

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be flipped.

        Returns:
            PIL Image or Tensor: Randomly flipped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            if torch.rand(1) < self.p:
                return vflip(img), vflip(img2)
            return img, img2
        else:
            if torch.rand(1) < self.p:
                return vflip(img)
            return img

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(p={self.p})"


class RandomDepthFlip(torch.nn.Module):
    """Vertically flip the given image randomly with a given probability.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading
    dimensions

    Args:
        p (float): probability of the image being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        super().__init__()
        _log_api_usage_once(self)
        self.p = p

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be flipped.

        Returns:
            PIL Image or Tensor: Randomly flipped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            if torch.rand(1) < self.p:
                return dflip(img), dflip(img2)
            return img, img2
        else:
            if torch.rand(1) < self.p:
                return dflip(img)
            return img

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(p={self.p})"
