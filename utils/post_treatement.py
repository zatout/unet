import torch
from scipy.interpolate import CubicSpline
from .data_loading import DnsTemperatureLoaderCoarse, middle_point
from glob import glob
import numpy as np
import os
from datahandling import compute_rms
import matplotlib.pyplot as plt
from torch.utils.data import random_split, DataLoader


@torch.no_grad()
def post_treat_on_validation(net, tloss, vloss, seed=0, path_prefix=None):
    """
    Precise assessment of neural network over validation data.
    This function can be extended test data.
    """
    device = "cuda" if torch.cuda.is_available() else "cpu"
    import socket
    if socket.gethostname() == "compta68":
        images_dir = "/home/zatout/Documents/These_yanis/Reunion_10012022/"
        path = "/home/zatout/Documents/These_yanis/Reunion_10012022/dns_180/"
        dns_dir = os.path.join(images_dir, "dns_coarse_180/48_52_48")
        les_dir = os.path.join(images_dir, "les_180")
    else:
        images_dir = "/mnt/beegfs/home/zatout/solaire/unet"
        path = "/mnt/beegfs/home/zatout/solaire/unet/dns_180"
        dns_dir = "/mnt/beegfs/projects/solaire/unet/dns_coarse_180/48_52_48_nsamples45"
        les_dir = "/mnt/beegfs/projects/solaire/unet/les_180/48_52_48_nsamples45"

    ndns = (384, 384, 266)
    nles = (48, 48, 52)

    path_coords = sorted(glob(path+"/dns_lata_1.sauv.lata.grid_geom2.coord*"))
    coord_vertex = [np.fromfile(g, dtype=np.float32) for g in path_coords]
    coord = [middle_point(c) for c in coord_vertex]
    steps = [np.diff(c) for c in coord]
    steps = [torch.tensor(steps[0][0]), torch.tensor(steps[1][0]), steps[2]]
    cs = CubicSpline(np.arange(ndns[-1]+1)/ndns[-1], coord_vertex[-1])
    coord_face_interp = cs(np.arange(nles[-1]+1)/nles[-1])
    coord_vertex_les = [
        np.linspace(coord_vertex[0].min(), coord_vertex[0].max(), nles[0]+1),
        np.linspace(coord_vertex[1].min(), coord_vertex[1].max(), nles[1]+1),
        coord_face_interp
    ]

    coord_les = [
        middle_point(c) for c in coord_vertex_les
    ]

    tmin, tmax = 293, 586

    dns_dir_val = os.path.join(images_dir, "dns_coarse_180/48_52_48")
    les_dir_val = os.path.join(images_dir, "les_180/48_52_48")
    # dns_dir = os.path.join(images_dir, "dns_180/extra/coarse")
    # les_dir = os.path.join(images_dir, "dns_180/extra/les")

    def normalize(t, tmin, tmax):
        return ((t - tmin)/(tmax-tmin) - 0.5)*2

    def unnormalize(t, tmin, tmax):
        return ((t / 2) + 0.5) * (tmax - tmin) + tmin

    otherdataset = DnsTemperatureLoaderCoarse(
        dir_dns=dns_dir_val,
        dir_les=les_dir_val,
        transform=None,
        device=device,
        re_tau=180,
        nles=[48, 48, 52],
        normalize=normalize,
        r_air=287.058,
        tmax=tmax,
        tmin=tmin,
    )

    seed = seed
    _, val_set = random_split(otherdataset, [0, 17])
    train_batch_size = 45
    validation_batch_size = 17

    loader_args_val = dict(batch_size=validation_batch_size, num_workers=0)

    val_loader = DataLoader(val_set, shuffle=False, **loader_args_val)

    for val in val_loader:
        val = tuple(val)
        break
    net.to(device).float()
    # val[0] = val[0].float().to(device)
    # val[1] = val[1].float().to(device)

    dns = unnormalize(val[0].float().to(device), tmin, tmax)
    dns_recon = unnormalize(
        net(val[1].to(device) + val[1].to(device)), tmin, tmax).cpu()
    recon_nn = dns_recon.cpu()

    dns = dns.squeeze().cpu()
    les = val[1].float().cpu()
    dns_recon = dns_recon.squeeze().cpu()

    def mean(x):
        return np.mean(x, (-3, -2))
    m_recon = [mean(mm.numpy()) for mm in dns_recon]
    m_dns = [mean(mm.numpy()) for mm in dns]
    m_les = [mean(mm.numpy()) for mm in les]

    rms_nn = [np.sqrt(compute_rms(np.squeeze(r.numpy()))) for r in recon_nn]
    rms_dns = [np.sqrt(compute_rms(np.squeeze(d.abs()).numpy())) for d in dns]
    rms_les = [np.sqrt(compute_rms(np.squeeze(l).numpy())) for l in les]

    norm_dns = np.linalg.norm(dns.numpy())
    norm_les = np.linalg.norm(les.numpy())

    plt.semilogy(tloss/norm_dns, label="training")
    plt.semilogy(2*vloss/norm_les,   label="validation")
    plt.xlabel(r"Epochs")
    plt.ylabel(r"Loss")
    plt.title("Normalized training loss")
    leg = plt.legend(loc='upper right', bbox_to_anchor=(
        0.75, -0.2), fancybox=True, shadow=True, ncol=4, fontsize=30)
    plt.grid()
    plt.savefig(os.path.join(path_prefix, "loss.pdf"),
                bbox_extra_artists=(leg,), bbox_inches='tight', pad_inches=0)

    fig, ax = plt.subplots(17, 1, figsize=(27/3, 9*17))
    for i in range(17):
        ax[i].plot(coord_les[-1]/coord_les[-1].max(), np.squeeze(m_dns)[i],)
        ax[i].plot(coord_les[-1]/coord_les[-1].max(), np.squeeze(m_les)[i],)
        ax[i].plot(coord_les[-1]/coord_les[-1].max(),
                   np.squeeze(m_recon[i]),  color="green")
        ax[i].set_xlabel("$y/2h$")
        ax[i].set_ylabel(r"$\langle T \rangle$ ($K$)")
        ax[i].set_title("Mean temperature profile")

    plt.savefig(os.path.join(path_prefix, "mean_temperature_profile.pdf"),
                bbox_extra_artists=(leg,), bbox_inches='tight', pad_inches=0)

    fig, ax = plt.subplots(17, 1, figsize=(27/3, 9*17))
    for i in range(17):
        ax[i].plot(coord_les[-1]/coord_les[-1].max(),
                   rms_dns[i],            label="DNS",)
        ax[i].plot(coord_les[-1]/coord_les[-1].max(),
                   rms_les[i],            label="LES",)
        ax[i].plot(coord_les[-1]/coord_les[-1].max(),
                   np.squeeze(rms_nn[i]), "x", label="CNN", color="green")
        ax[i].grid()
        ax[i].grid()
        ax[i].set_xlabel("$y/2h$")
        ax[i].set_ylabel(r"$\sqrt{\langle T^{'2} \rangle}$ ($K$)")
        ax[i].set_title("RMS profile")

    plt.savefig(os.path.join(path_prefix, "rms_profile"),
                bbox_extra_artists=(leg,), bbox_inches='tight', pad_inches=0)

    extent = [coord[0].min(), coord[0].max(), coord[1].min(), coord[1].max()]
    index_cut_1 = np.argmax(rms_dns[0][:15])
    index_cut_2 = len(rms_dns[0])//2
    index_cut_3 = np.argmax(rms_dns[1][-20:]) + 51 - 20
    fig, ax = plt.subplots(3, 4, figsize=(9*3.0, 10), sharex=True, sharey=True)
    ax = ax.ravel()

    os.makedirs(os.path.join(path_prefix, "spacial_cuts.pdf"), exist_ok=True)
    prefix_cuts = os.path.join(path_prefix, "spacial_cuts.pdf")

    for image_number in range(17):
        cmap = "bwr"
        im = 0
        for idx, i in enumerate([index_cut_1, index_cut_2, index_cut_3]):
            ax[idx*4+0].imshow(np.squeeze(dns[image_number, ..., i]), cmap=cmap, vmin=tmin, vmax=tmax, extent=extent)
            im = ax[idx*4+1].imshow(np.squeeze(les[image_number, ..., i]), cmap=cmap, vmin=tmin, vmax=tmax, extent=extent)
            ax[idx*4+2].imshow(np.squeeze(recon_nn[image_number, ..., i]), cmap=cmap, vmin=tmin, vmax=tmax, extent=extent)
        plt.subplots_adjust(hspace=0.03, wspace=0.0)
        ax[0].set_title(r"$T^c$")
        ax[1].set_title(r"$\overline{T}^c$")
        ax[2].set_title(r"$T_{CNN}^c$", x=.5, y=1.1)
        ax[3].set_title(r"$T_{Scale Sim}^f$", x=.5, y=1.1)
        cbar_ax = fig.add_axes([.25, -.12, 0.5, 0.05])
        cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
        cbar.set_ticks([
            # int((max_ + min_)/2),int(max_-1)
            *np.linspace(int(tmin)+1, int(tmax), 6, dtype=int)
        ])

        cbar.ax.xaxis.set_label_position('top')

        cbar.set_label("$T$ ($K$)", loc="center")
        fig.text(0.07, 0.5, r"$z$ ($m$)", va='center', rotation='vertical')
        fig.text(0.5, 0.0, r"$x$ ($m$)", va='center', rotation='horizontal')

        plt.savefig(os.path.join(prefix_cuts, f"cuts_{i}.pdf"),
                    bbox_extra_artists=(leg,), bbox_inches='tight', pad_inches=0)
