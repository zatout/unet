from collections.abc import Callable
from typing import List, Tuple, Union
import torch
from torch.optim import Optimizer
from torch.optim.lr_scheduler import _LRScheduler
from torch import nn
from torch.functional import Tensor
from torchvision.transforms import Compose
from tqdm import tqdm
import numpy as np
from .data_loading import unnormalize
from .loss import loss_grad_rms, loss_grad_torch
from .transforms import get_transforms_and_coord


def RMSELoss(x: Tensor, y: Tensor) -> Tensor:
    """
    Root mean squared loss
    """
    return torch.sqrt(nn.MSELoss()(x, y))


def mini_batch_training_loss(
    train_loader: Tuple[Tensor, Tensor],
    val_loader: Tuple[Tensor, Tensor],
    net: nn.Module,
    optim: Optimizer,
    scheduler: _LRScheduler,
    steps_per_epoch: int = 500,
    transforms: Union[Compose, None] = None,
    device: str = str(None),
    criterion: Callable = RMSELoss,
    pbar: tqdm = None,
    epoch: int = 1
) -> Tuple[float, float, nn.Module]:
    """
    Training loop
    """
    global_step = 0
    net.train()
    epoch_loss = 0
    for step in range(steps_per_epoch):
        optim.zero_grad()
        dns, les = transforms(tuple(train_loader))

        dns_recon = les + net(les)
        loss = criterion(dns, dns_recon)
        del dns, dns_recon, les
        loss.backward()

        optim.step()

        global_step += 1
        epoch_loss += loss.item()

    train_loss = epoch_loss / steps_per_epoch

    valid_loss = 0
    valid_loss = valid_loss + \
        fast_evaluate(net, val_loader, device, criterion,
                      transforms)  # / len(val_loader)

    pbar.set_postfix(**{'loss (batch)': train_loss,
                     'validation (batch)': valid_loss})
    if epoch % 10 == 0:
        scheduler.step()
    return train_loss, valid_loss, net


@torch.no_grad()
def fast_evaluate(
    net, batch, device, criterion, transforms
):
    """
    Validation loop
    """
    validation_loss = 0
    dns, les = tuple(batch)
    # move images and labels to correct device and type

    dns = dns.to(device)
    les = les.to(device)

    prediction = net(les) + les
    validation_loss += criterion(dns, prediction).item()
    net.train()
    return validation_loss


def training_loop(batch_, val_, net, optim, scheduler,
                  steps_per_epoch, transforms,
                  device, criterion, epochs,
                  save_net=True, additional_save_string=None):
    """
    Training loop
    """
    tl = []
    vl = []
    with tqdm(range(1, epochs+1)) as pbar:
        for epoch in pbar:
            train_loss, validation_loss, net = mini_batch_training_loss(
                train_loader=batch_, val_loader=val_, net=net, optim=optim,
                scheduler=scheduler,
                steps_per_epoch=steps_per_epoch, transforms=transforms,
                device=device, criterion=criterion, pbar=pbar, epoch=epoch
            )
            tl.append(train_loss)
            vl.append(validation_loss)

    return tl, vl, net


def curriculum_learning_growing_domain(
    train_loader, val_loader, net, lr, scheduler, steps_per_epoch, transforms,
    device, criterion=RMSELoss, epochs=150, crop_size=16, mlr=.8, save=True,
    additional_save_string=""
):
    """
    Curriculum learning function
    """
    train_loss = []
    validation_loss = []
    batch = torch.tensor([0])
    val = torch.tensor([0])
    for batch in train_loader:
        batch = tuple(batch)
        break
    for val in val_loader:
        val = tuple(val)
        break
    slices = np.arange(crop_size, batch[0].size(-1)+4, 4)
    n_train = batch[0].size(0)
    additional_examples = np.ceil(
        ((slices)/crop_size - 1) * n_train).astype(dtype=int)

    optim = torch.optim.Adam(net.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim, lr_lambda=lambda epoch: mlr)
    for add, subdomain_slice in zip(additional_examples, slices):

        additional_choises = np.random.choice(n_train, add)
        indexes = np.concatenate([np.arange(n_train), additional_choises])
        dns, les = batch
        val_dns, val_les = val
        batch_ = tuple((dns[indexes, ..., :subdomain_slice],
                       les[indexes, ..., :subdomain_slice]))
        val_ = tuple((val_dns[..., :subdomain_slice],
                     val_les[..., :subdomain_slice]))
        if subdomain_slice == slices[-1]:
            tl, vl, net = training_loop(batch_, val_, net, optim, scheduler,
                                        steps_per_epoch, transforms,
                                        device, criterion, epochs,
                                        save_net=True and save,
                                        additional_save_string="last_stage"
                                        + additional_save_string)
            # print("We save the weights")
        else:
            tl, vl, net = training_loop(batch_, val_, net, optim, scheduler,
                                        steps_per_epoch, transforms,
                                        device, criterion, epochs,
                                        save_net=False,
                                        additional_save_string=additional_save_string)
        train_loss = [*train_loss, *tl]
        validation_loss = [*validation_loss, *vl]

        optim = torch.optim.Adam(net.parameters(), lr=lr)
        scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
            optim, lr_lambda=lambda epoch: mlr)
    return train_loss, validation_loss, net


# @torch.no_grad()
# def fast_evaluate_gradients(
#     net, batch, device, criterion, coord_elem, coord_face
# ):
#     """
#     Validation loop
#     """
#     validation_loss = 0
#     dns, les = tuple(batch)
#     # move images and labels to correct device and type
#
#     dns = dns.to(device)
#     les = les.to(device)
#
#     prediction = net(les) + les
#     validation_loss += criterion(dns, prediction,
#                                  coord_elem, coord_face).item()
#     net.train()
#     return validation_loss
#

@torch.no_grad()
def fast_evaluate_gradients(
    net, batch, device, criterion, coord_elem, coord_face
):
    """
    Validation loop
    """
    validation_loss = 0
    net.to(device)
    for dns, les in batch:
        # move images and labels to correct device and type
        dns = dns.to(device)
        les = les.to(device)
        coord_elem = coord_elem.to(device)
        coord_face = coord_face.to(device)
        # print(dns.min(), les.min())
        # print(dns.max(), les.max())
        prediction = net(les) + les
        validation_loss += criterion(
            dns, prediction,
            [None, None, coord_elem],
            [None, None, coord_face]
        ).item()
    net.train()
    return validation_loss


@torch.no_grad()
def test_quantities(net, les, tmin, tmax):
    out = unnormalize(net.to("cuda")(les.float()).to(
        "cuda") + les.to("cuda").float(), tmin, tmax).to("cpu")

    return (
        out,
        np.mean(out, (-2, -3)),
        np.mean(out**2, (-2, -3)) - np.mean(out, (-2, -3))**2
    )


def curriculum_learning_growing_domain_gradients(
    train_loader, val_loader, coord_elem, coord_face, net, lr, scheduler, steps_per_epoch, transforms,
    device, criterion=RMSELoss, epochs=150, crop_size=16, mlr=.8, save=True,
    additional_save_string="", debug=False
):
    """
    Curriculum learning function
    """
    print(f"epochs: {epochs}, steps_per_epoch: {steps_per_epoch}, crop_size: {crop_size}, mlr: {mlr}")
    train_loss = []
    validation_loss = []
    batch = torch.tensor([0])
    val = torch.tensor([0])
    if debug:
        device = "cuda"
        print(f"DEBUG: device forced: {device}")
        net.to(device)
    for batch in train_loader:
        batch = tuple(batch)
        break
    for val in val_loader:
        val = tuple(val)
        break
    # print(batch[0].shape, batch[1].shape)
    slices = np.arange(crop_size, batch[0].size(-1)+4, 4)
    n_train = batch[0].size(0)

    additional_examples = np.ceil(
        ((slices)/crop_size - 1) * n_train).astype(dtype=int)

    optim = torch.optim.Adam(net.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
        optim, lr_lambda=lambda epoch: mlr)

    coord_elem, coord_face = torch.from_numpy(
        coord_elem), torch.from_numpy(coord_face)

    for add, subdomain_slice in zip(additional_examples, slices):
        additional_choises = np.random.choice(n_train, add)
        indexes = np.concatenate([np.arange(n_train), additional_choises])
        dns, les = batch
        val_dns, val_les = val
        batch_ = tuple((dns[indexes, ..., :subdomain_slice],
                       les[indexes, ..., :subdomain_slice]))
        val_ = tuple((val_dns[..., :subdomain_slice],
                     val_les[..., :subdomain_slice]))
        c_ = coord_elem[:subdomain_slice]
        f_ = coord_face[:subdomain_slice+1]
        transforms = get_transforms_and_coord(
            crop=True, flip=True, rotate=True, crop_size=16, flip_prob=1/3., coord_elem=c_, coord_face=f_, n_rot=4)
        with tqdm(range(1, epochs+1)) as pbar:
            for epoch in pbar:
                global_step = 0
                net.train()
                epoch_loss = 0
                for step in range(steps_per_epoch):
                    optim.zero_grad()
                    dns, les, c, f = transforms(tuple(batch_))
                    # print(c.shape, f.shape)
                    dns = dns.to(device)
                    les = les.to(device)
                    c, f = c.to(device), f.to(device)
                    # print(dns.min(), les.min())
                    # print(dns.max(), les.max())
                    # a = transforms(tuple((batch_)))

                    dns_recon = les + net(les)
                    loss = criterion(dns, dns_recon, [
                                     None, None, c], [None, None, f])
                    del dns, dns_recon, les
                    loss.backward()

                    optim.step()

                    global_step += 1
                    epoch_loss += loss.item()
                with torch.no_grad():
                    net.eval()
                    eval_net = net.to(device)
                    dns, les = batch_
                    dns = dns.to(device)
                    les = les.to(device)
                    c, f = c_.to(device), f_.to(device)

                    dns_recon = les + eval_net(les)
                    tl = criterion(dns, dns_recon, [None, None, c], [None, None, f])
                    del dns, dns_recon, les

                    # tl = epoch_loss / steps_per_epoch

                vl = 0
                vl = vl + fast_evaluate_gradients(
                    net, val_loader, device, criterion, coord_elem, coord_face) / len(val_loader)

                pbar.set_postfix(**{'loss (batch)': tl.item(),
                                 'validation (batch)': vl})
                if epoch % 10 == 0:
                    scheduler.step()
                # return train_loss, valid_loss, net
                train_loss.append(tl)
                validation_loss.append(vl)

        optim = torch.optim.Adam(net.parameters(), lr=lr)
        scheduler = torch.optim.lr_scheduler.MultiplicativeLR(
            optim, lr_lambda=lambda epoch: mlr)
    train_loss = torch.tensor(train_loss).cpu()
    validation_loss = torch.tensor(validation_loss).cpu()
    return train_loss, validation_loss, net
