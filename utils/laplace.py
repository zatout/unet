from typing import Dict, List, Tuple, Union
import numpy as np
# from numpy import typing as npt
from torch import Tensor, tensor
import torch
import numba
from numba import njit, double
from scipy.signal import convolve
from scipy.interpolate import interp1d


def _check_pad(pad):
    if isinstance(pad, tuple):
        return torch.tensor(pad).abs()
    if isinstance(pad, np.ndarray):
        return torch.from_numpy(pad).abs()
    return torch.tensor(pad)


def is_integer(x: Tensor):
    return (not x.dtype.is_floating_point) and (not x.dtype.is_complex)


def pad_periodic_symmetric(
    x: Tensor,
    pad: Union[int, Tuple[int], Tensor],
    axis: int
) -> Tensor:
    """
    For a given ND tensor, pads it to have a periodic boundary coundition

    Parameters:
    -----------
    x: Tensor
        Input tensor
    axis: int
        Axis along which to pad in the front and back

    Returns:
    --------
    out: Tensor
        padded output x
    """
    pad_ = np.abs(pad)
    selection_begin = torch.narrow(x, axis, 0, pad_)
    selection_end = torch.narrow(x, axis, x.shape[axis] - pad_, pad_)
    xpad = torch.cat(
        (
            selection_end, x
        ),
        axis
    )
    xpad = torch.cat(
        (
            xpad, selection_begin
        ),
        axis
    )
    return xpad


def pad_dirichlet_symmetric(
    x: Tensor,
    pad: int,
    axis: int
) -> Tensor:
    pad_ = np.abs(pad)
    selection_begin = torch.narrow(x, axis, 0, pad_)
    selection_end = torch.narrow(x, axis, x.shape[axis] - pad_, pad_)
    xpad = torch.cat(
        (
            selection_begin, x
        ),
        axis
    )
    xpad = torch.cat(
        (
            xpad, selection_end
        ),
        axis
    )
    return xpad


def pad_dirichlet_end(
    x: Tensor,
    pad: int,
    axis: int
) -> Tensor:
    pad_ = np.abs(pad)
    selection_end = torch.narrow(x, axis, x.shape[axis] - pad_, pad_)
    xpad = x
    xpad = torch.cat(
        (
            xpad, selection_end
        ),
        axis
    )
    return xpad


def pad_global_field(
    x: Tensor,
    pad: Union[Tuple[int, int, int], int, List[int], Tensor],
    boundary_cond: Dict = {1: "periodic",
                           2: "periodic",
                           3: "dirichlet"},
):
    """
    For a given tensor, pads it as to have it as a periodic tensor, with one
    limite layer on each periodic side so as to not have a problem when doing
    a ND convolution
    """
    n_axis = len(boundary_cond)
    if isinstance(pad, int) or is_integer(pad):
        pad = [pad for _ in range(n_axis)]
    try:
        xpad = x.clone()
    except AttributeError:
        xpad = torch.tensor(x).clone()
    axes = torch.arange(-n_axis, 0, 1)
    for axis, bc, p in zip(axes, boundary_cond.values(), pad):
        if bc == "periodic":
            xpad = pad_periodic_symmetric(xpad, p, axis)
        if bc == "dirichlet":
            xpad = pad_dirichlet_symmetric(xpad, p, axis)
    return xpad


def pad_circular_nd(x: torch.Tensor, pad: int, dim) -> torch.Tensor:
    """
    :param x: shape [H, W]
    :param pad: int >= 0
    :param dim: the dimension over which the tensors are padded
    :return:
    """

    if isinstance(dim, int):
        dim = [dim]

    for d in dim:
        if d >= len(x.shape):
            raise IndexError(f"dim {d} out of range")

        idx = tuple(slice(0, None if s != d else pad, 1)
                    for s in range(len(x.shape)))
        x = torch.cat([x, x[idx]], dim=d)

        idx = tuple(slice(None if s != d else -2 * pad, None if s !=
                    d else -pad, 1) for s in range(len(x.shape)))
        x = torch.cat([x[idx], x], dim=d)
        pass

    return x

def pad_end_to_end_xz(x):
    dns = x
    padz = torch.cat((dns, dns[..., :-1, :]), -2)
    return torch.cat((padz, padz[..., :-1, :, :]), -3)


def gradient_np(
    q,
    x,
    y,
    z,
):
    qx = np.gradient(q, x, edge_order=2)
    qy = np.gradient(q, y, edge_order=2)
    qz = np.gradient(q, z, edge_order=2)
    return qx, qy, qz

# @torch.jit.script


def variable_laplacian(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,
    z: Tensor,
    dzvertex: Tensor,
) -> Tensor:
    """
    Computes laplacian for fixed h in x and y and for varying h in z direction
    """
    fs = 1
    xx = (x[:-2] - 2 * x[1:-1] + x[2:])/dx**2
    yy = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])/dy**2

    zz = torch.zeros_like(x)
    for i in range(len(dz)-1):
        hr = z[i+1] - z[i]
        hl = z[i] - z[i-1]
        ii = i + fs
        # METHODE 5 given by Adrien Toutant
        # Suppose we have grad(T)_i = (T_{i+1} - T_i)/(x_{i+1} - X_{i})
        # Then we have
        # div(grad(T)) = (grad(T)_{i+1} - grad(T)_i)/(x_{i+1/2} - x_{i-1/2})
        # Where x_{i+1/2} corresponds to the point
        # on the face of the staggered mesh
        # -------------
        # |            |
        # |     ⋅  i+1 |
        # |            |
        # |-----i+1/2--|    # gradient is computed here
        # |            |
        # |     ⋅ i    |    # laplacian is computed here
        # |            |
        # |-----i-1/2--|    # gradient is computed here
        # |            |
        # |     ⋅ i-1  |
        # |            |
        # --------------

        grad_T_i_p_1 = (x[..., ii+1] - x[..., ii])/(hr)
        grad_T_i = (x[..., ii] - x[..., ii - 1])/(hl)
        # z[i+1/2] - z[i-1/2]
        dzv = dzvertex[i]
        zz[..., ii] = (grad_T_i_p_1 - grad_T_i)/(dzv)

    xx = xx[...,
            fs: -fs,
            fs: -fs]
    yy = yy[fs: -fs,
            :,
            fs: -fs]
    zz = zz[fs: -fs,
            fs: -fs,
            fs: -fs]

    return xx + yy + zz


@torch.jit.script
def cell_dependant_laplacian(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,
    z: Tensor,
    dzvertex: Tensor,
) -> Tensor:
    fs = 1
    xx = (x[:-2] - 2 * x[1:-1] + x[2:])
    yy = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])

    zz = torch.zeros_like(x)
    for i in range(len(dz)-1):
        hr = z[i+1] - z[i]
        hl = z[i] - z[i-1]
        ii = i + fs
        # METHOD given by Adrien Toutant
        # Suppose we have grad(T)_i = (T_{i+1} - T_i)/(x_{i+1} - X_{i})
        # Then we have
        # div(grad(T)) = (grad(T)_{i+1} - grad(T)_i)/(x_{i+1/2} - x_{i-1/2})
        # Where x_{i+1/2} corresponds to the point
        # on the face of the staggered mesh
        # -------------
        # |            |
        # |     ⋅  i+1 |
        # |            |
        # |-----i+1/2--|    # gradient is computed here
        # |            |
        # |     ⋅ i    |    # laplacian is computed here
        # |            |
        # |-----i-1/2--|    # gradient is computed here
        # |            |
        # |     ⋅ i-1  |
        # |            |
        # --------------

        grad_T_i_p_1 = (x[..., ii+1] - x[..., ii])/(hr)
        grad_T_i = (x[..., ii] - x[..., ii - 1])/(hl)
        # z[i-1/2]
        dzv = dzvertex[i]
        zz[..., ii] = dzv * (grad_T_i_p_1 - grad_T_i)

    xx = xx[...,
            fs: -fs,
            fs: -fs]
    yy = yy[fs: -fs,
            :,
            fs: -fs]
    zz = zz[fs: -fs,
            fs: -fs,
            fs: -fs]

    return xx + yy + zz


@torch.jit.script
def cell_dependant_laplacian_filter_width(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,
    z: Tensor,
    dzvertex: Tensor,
    filter_width_x: float,
    filter_width_y: float,
    filter_width_z: float,

) -> Tensor:
    fs = 1
    xx = (x[:-2] - 2 * x[1:-1] + x[2:])/(dx**2)
    yy = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])/(dy**2)

    zz = torch.zeros_like(x)
    for i in range(len(dz)-1):
        hr = z[i+1] - z[i]
        hl = z[i] - z[i-1]
        ii = i + fs

        grad_T_i_p_1 = (x[..., ii+1] - x[..., ii])/(hr)
        grad_T_i = (x[..., ii] - x[..., ii - 1])/(hl)
        # z[i-1/2]
        dzv = dzvertex[i]
        zz[..., ii] = (grad_T_i_p_1 - grad_T_i)/(dzv)

    xx = xx[...,
            fs: -fs,
            fs: -fs]
    yy = yy[fs: -fs,
            :,
            fs: -fs]
    zz = zz[fs: -fs,
            fs: -fs,
            fs: -fs]

    return (filter_width_x**2) * xx + (filter_width_y**2) * yy + (filter_width_z**2) * zz


# @torch.jit.script
# def dorian_laplacian(
#     x: Tensor,
#     dx: float,
#     dy: float,
#     dz: Tensor,
#     z: Tensor,
#     z_vertex: Tensor,
#     filter_width_x: float,
#     filter_width_y: float,
#     filter_width_z: float,
#
# ) -> Tensor:
#     fs = 1
#     xx = (x[:-2] - 2 * x[1:-1] + x[2:])/(dx**2)
#     yy = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])/(dy**2)
#
#     zz = torch.zeros_like(x)
#     for i in range(len(dz)-1):
#         hr = z[i+1] - z[i]
#         hl = z[i] - z[i-1]
#         ii = i + fs
#
#         grad_T_i_p_1 = (x[..., ii+1] - x[..., ii])/(hr)
#         grad_T_i = (x[..., ii] - x[..., ii - 1])/(hl)
#         # z[i-1/2]
#         dzv = dzvertex[i]
#         zz[..., ii] = (grad_T_i_p_1 - grad_T_i)/(dzv)
#
#     xx = xx[...,
#             fs: -fs,
#             fs: -fs]
#     yy = yy[fs: -fs,
#             :,
#             fs: -fs]
#     zz = zz[fs: -fs,
#             fs: -fs,
#             fs: -fs]
#
#     return (filter_width_x**2) * xx + (filter_width_y**2) * yy + (filter_width_z**2) * zz
def compute_variable_laplacian(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,
    z: Tensor,
    dzvertex: Tensor,
    boundary_cond: Dict = {1: "periodic",
                           2: "periodic",
                           3: "dirichlet"}
) -> Tensor:
    xpad = pad_global_field(x, 1, boundary_cond)
    return variable_laplacian(xpad, dx, dy, dz, z, dzvertex).numpy()


def compute_direction_cell_dependant_laplacian(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,
    z: Tensor,
    dzvertex: Tensor,
    boundary_cond: Dict = {1: "periodic",
                           2: "periodic",
                           3: "dirichlet"}
) -> Tensor:
    """
    Computes the cell dependant directional laplacian such that:
    \Delta T = (\Delta_x^2 \frac{\partial ^2}{\partial x}
        + \Delta_y^2 \frac{\partial ^2}{\partial y}
        + \Delta_z^2 \frac{\partial ^2}{\partial z}) T
    """
    xpad = pad_global_field(x, 1, boundary_cond)
    return cell_dependant_laplacian(xpad, dx, dy, dz, z, dzvertex).numpy()


@torch.jit.script
def lap(x: Tensor, dx: float, dy: float, dz: Tensor):
    xx = torch.zeros_like(x)
    yy = torch.zeros_like(x)
    zz = torch.zeros_like(x)
    #  X and Y correspond to periodic boundary conditions
    xx[1:-1] = (x[:-2] - 2 * x[1:-1] + x[2:])/dx**2
    xx[0] = (x[-1] - 2 * x[0] + x[1]) / dx**2
    xx[-1] = (x[-2] - 2 * x[-1] + x[0]) / dx**2

    yy[:, 1:-1] = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])/dy**2
    yy[:, 0] = (x[:, -1] - 2 * x[:, 0] + x[:, 1]) / dy**2
    yy[:, -1] = (x[:, -2] - 2 * x[:, -1] + x[:, 0]) / dy**2

    #  Z corresponds to a dirichlet boundary conditions
    for i in range(1, len(dz)):
        zz[..., i] = (
            x[..., i-1] - 2 * x[..., i] + x[..., i+1]
        ) / dz[i-1] * dz[i]
    zz[..., 0] = (x[..., 0] - 2 * x[..., 0] + x[..., 1])/dz[0]**2
    zz[..., -1] = (x[..., -1] - 2 * x[..., -1] + x[..., -2])/dz[-1]**2

    return xx + yy + zz


@torch.jit.script
def fixed_laplacian(
    x: Tensor,
    dx: Tensor,
    dy: Tensor,
    dz: Tensor,
):
    """
    Computes fixed spacing laplacian
    """
    xx = (x[:-2] - 2 * x[1:-1] + x[2:])/dx**2
    yy = (x[:, :-2] - 2 * x[:, 1:-1] + x[:, 2:])/dy**2
    zz = (x[..., :-2] - 2 * x[..., 1:-1] + x[..., 2:])/dz**2

    xx = xx[...,
            1: -1,
            1: -1]
    yy = yy[1: -1,
            ...,
            1: -1]
    zz = zz[1: -1,
            1: -1]
    return xx + yy + zz


def compute_fixed_laplacian(
    x: Tensor,
    dx: float,
    dy: float,
    dz: Tensor,

    boundary_cond: Dict = {1: "periodic",
                           2: "periodic",
                           3: "dirichlet"}
):

    xpad = pad_global_field(x, 1, boundary_cond=boundary_cond)
    return fixed_laplacian(xpad, dx, dy, dz).numpy()


@torch.jit.script
def compute_cell_volume(
        di: Tensor,
        dj: Tensor,
        dk: Tensor,
        k: int,
        filter_size: int
):
    """
    Computes the volume of a filter in the k direction
    """
    vij = (filter_size ** 2) * di * dj
    return dk[k - filter_size//2: k + filter_size // 2 + 1].sum() * vij


@torch.jit.script
def compute_volume_filter_mask(
    di: Tensor,
    dj: Tensor,
    dk: Tensor,
    filter_size: int,
    domain_shape: Tuple[int, int, int]
) -> Tensor:
    out = torch.empty(domain_shape, dtype=torch.float64)
    # half = torch.div(filter_size, 2, rounding_mode="floor")
    half = filter_size//2
    for k in range(half, domain_shape[-1] - half):
        out[..., k] = compute_cell_volume(di, dj, dk, k, filter_size)
    for k in range(half):
        n_outside_cells = half - k
        n_inside_cells = filter_size - n_outside_cells
        out[..., k] = (dk[0] * n_outside_cells + torch.sum(dk[:n_inside_cells])) \
            * filter_size ** 2 * di * dj
        out[..., -k-1] = (dk[0] * n_outside_cells + torch.sum(dk[:n_inside_cells])) \
            * filter_size ** 2 * di * dj
    return out


@torch.jit.script
def compute_volume_filter_mask_cube_root(
    di: Tensor,
    dj: Tensor,
    dk: Tensor,
    filter_size: int,
    domain_shape: Tuple[int, int, int]
) -> Tensor:
    out = torch.empty(domain_shape, dtype=torch.float64)
    pow_1_3 = 1/3
    half = filter_size//2
    for k in range(half, domain_shape[-1] - half):
        out[..., k] = compute_cell_volume(di, dj, dk, k, filter_size)**pow_1_3
    for k in range(half):
        n_outside_cells = half - k
        n_inside_cells = filter_size - n_outside_cells
        out[..., k] = ((dk[0] * n_outside_cells
                        + torch.sum(dk[:n_inside_cells]))
                       * filter_size ** 2 * di * dj)**pow_1_3
        out[..., -k-1] = ((dk[0] * n_outside_cells
                           + torch.sum(dk[:n_inside_cells]))
                          * filter_size ** 2 * di * dj)**pow_1_3
    return out


def mult_along_axis(A, B, axis):

    # ensure we're working with Numpy arrays
    A = np.array(A)
    B = np.array(B)

    # shape check
    if axis >= A.ndim:
        raise np.core._internal.AxisError(axis, A.ndim)
    if A.shape[axis] != B.size:
        raise ValueError(
            "Length of 'A' along the given axis must be the same as B.size"
        )

    # np.broadcast_to puts the new axis as the last axis, so
    # we swap the given axis with the last one, to determine the
    # corresponding array shape. np.swapaxes only returns a view
    # of the supplied array, so no data is copied unnecessarily.
    shape = np.swapaxes(A, A.ndim-1, axis).shape

    # Broadcast to an array with the shape as above. Again,
    # no data is copied, we only get a new look at the existing data.
    B_brc = np.broadcast_to(B, shape)

    # Swap back the axes. As before, this only changes our "point of view".
    B_brc = np.swapaxes(B_brc, A.ndim-1, axis)

    return A * B_brc

# Numpy laplacian in the wall normal direction


def laplacian_z(x, y):
    x = np.asarray(x)
    y = np.asarray(y)
    hb = x[1:-1] - x[:-2]
    hf = x[2:] - x[1:-1]
    y_hb = y[..., :-2]
    y_hf = y[..., 2:]
    hb_hf = hb / hf
    hf_hb = hf / hb
    return (y_hf*(1+hb_hf) - y[..., 1:-1]*(2+hb_hf+hf_hb) +
            y_hb*(1+hf_hb)) / 2 / hb / hf


def laplacian_y(x, y):
    x = np.asarray(x)
    y = np.asarray(y)
    hb = x[1:-1] - x[:-2]
    hf = x[2:] - x[1:-1]
    y_hb = y[:, :-2]
    y_hf = y[:, 2:]
    hb_hf = hb / hf
    hf_hb = hf / hb
    o = mult_along_axis(y_hf, (1+hb_hf), 1) - \
        mult_along_axis(y[:, 1:-1], (2+hb_hf+hf_hb), 1) + \
        mult_along_axis(y_hb, (1+hf_hb), 1)
    o = mult_along_axis(o, 1 / 2 / hb / hf, 1)
    return o


def laplacian_x(x, y):
    x = np.asarray(x)
    y = np.asarray(y)
    hb = x[1:-1] - x[:-2]
    hf = x[2:] - x[1:-1]
    y_hb = y[:-2]
    y_hf = y[2:]
    hb_hf = hb / hf
    hf_hb = hf / hb
    o = mult_along_axis(y_hf, (1+hb_hf), 0) - \
        mult_along_axis(y[1:-1], (2+hb_hf+hf_hb), 0) + \
        mult_along_axis(y_hb, (1+hf_hb), 0)
    o = mult_along_axis(o, 1 / 2 / hb / hf, 0)
    return o


def laplacian_numpy(x, y):
    """
    X: point coordinates
    Y: field points
    """
    lapx = laplacian_x(x[0], y)
    lapy = laplacian_y(x[1], y)
    lapz = laplacian_z(x[2], y)
    lapx = lapx[..., 1:-1, 1:-1]
    lapy = lapy[1:-1, ..., 1:-1]
    lapz = lapz[1:-1, 1:-1, ...]
    return lapx + lapy + lapz


def compute_deriv_xx(qtty, i, j, k, ni, nj, nk, coord, coord_vertex, index):
    x = coord[0]
    xv = coord_vertex[0]
    # Grad T_{i} = (T_{i+1} - T_{i})/(x_{i+1} - x_{i})
    gradtip1 = (qtty[index[i+1, j, k]] - qtty[index[i, j, k]])/(x[i+1] - x[i])
    gradti = (qtty[index[i, j, k]] - qtty[index[i-1, j, k]])/(x[i] - x[i-1])
    lap_x = (gradtip1 - gradti) / (xv[i+1] - xv[i])
    # -> Lap_x = gradtip1/(xv[i+1] - xv[i]) + gradti / (xv[i+1] - xv[i])
    # Lap_x[index[i, j, k]] = T[index[i+1]]/((x[i+1] - x[i])*(xv[i+1] - xv[i]))
    #       + T[index[i-1]]/((x[i] - x[i-1])*(xv[i+1] - xv[i]))
    #       + (T[index[i]]/(x[i] - x[i-1]) T[index[i]]/(x[i+1] - x[i])) /(xv[i+1] - xv[i])
    return lap_x


def compute_deriv_yy(qtty, i, j, k, ni, nj, nk, coord, coord_vertex, index):
    x = coord[1]
    xv = coord_vertex[1]
    gradtip1 = (qtty[index[i, j+1, k]] - qtty[index[i, j, k]])/(x[j+1] - x[j])
    gradti = (qtty[index[i, j, k]] - qtty[index[i, j-1, k]])/(x[j] - x[j-1])
    lap_x = (gradtip1 - gradti) / (xv[j+1] - xv[j])
    return lap_x


def compute_deriv_zz(qtty, i, j, k, ni, nj, nk, coord, coord_vertex, index):
    x = coord[2]
    xv = coord_vertex[2]
    gradtip1 = (qtty[index[i, j, k+1]] - qtty[index[i, j, k]])/(x[k+1] - x[k])
    gradti = (qtty[index[i, j, k]] - qtty[index[i, j, k-1]])/(x[k] - x[k-1])
    lap_x = (gradtip1 - gradti) / (xv[k+1] - xv[k])
    return lap_x


@njit(
    numba.types.Tuple((double, double, double))(
        numba.int64,
        double[:],
        double[:]
    )
)
def compute_coeff_deriv(i, coord, coord_vertex):
    x = coord
    xv = coord_vertex
    d_vertex = xv[i+1] - xv[i]
    d_x_right = x[i+1] - x[i]
    d_x_left = x[i] - x[i-1]
    T_i_m_1_coeff = 1/(d_vertex*d_x_left)
    T_i_p_1_coeff = 1/(d_vertex*d_x_right)
    T_i_coeff = T_i_m_1_coeff + T_i_p_1_coeff
    # -> Lap_x = gradtip1/(xv[i+1] - xv[i]) + gradti / (xv[i+1] - xv[i])
    # Lap_x[index[i, j, k]] = T[index[i+1]]/((x[i+1] - x[i])*(xv[i+1] - xv[i]))
    #       + T[index[i-1]]/((x[i] - x[i-1])*(xv[i+1] - xv[i]))
    #       + (T[index[i]]/(x[i] - x[i-1]) T[index[i]]
    #         /(x[i+1] - x[i])) /(xv[i+1] - xv[i])
    return (T_i_m_1_coeff, T_i_coeff, T_i_p_1_coeff)


def compute_sparse_coeff_deriv(i, coord, coord_vertex):
    x = np.array([coord[0], *coord, coord[-1]])
    xv = np.array([coord_vertex[0], *coord_vertex, coord_vertex[-1]])
    d_vertex = xv[i+1] - xv[i]
    d_x_right = x[i+1] - x[i]
    d_x_left = x[i] - x[i-1]

    d_vertex[d_vertex == 0] = np.infty
    d_x_right[d_x_right == 0] = np.infty
    d_x_left[d_x_left == 0] = np.infty

    T_i_m_1_coeff = 1/(d_vertex*d_x_left)
    T_i_p_1_coeff = 1/(d_vertex*d_x_right)
    T_i_m_1_coeff[T_i_m_1_coeff == 0] = 0
    T_i_p_1_coeff[T_i_p_1_coeff == 0] = 0
    T_i_coeff = T_i_m_1_coeff + T_i_p_1_coeff

    return (T_i_m_1_coeff, T_i_coeff, T_i_p_1_coeff)


@njit
def ijk(idx, nx, ny):
    z = idx//(nx * ny)
    idx = idx - z*nx*ny
    y = idx//nx
    x = idx % nx
    return x, y, z


@njit
def laplace_linear_operator(coord, coord_vertex):
    """
    Compute sparse Laplace linear operator such that
    A \times T = Lap(T)
    """
    coord = [c.astype(np.float64) for c in coord]
    coord_vertex = [c.astype(np.float64) for c in coord_vertex]
    nx, ny, nz = [c.shape[0] for c in coord]
    n = nx * ny * nz

    index_c = np.arange(n).reshape(nx, ny, nz)

    tijk = np.zeros(n, dtype=np.float64)

    tip1 = np.zeros(n, dtype=np.float64)
    tim1 = np.zeros(n, dtype=np.float64)

    tjp1 = np.zeros(n, dtype=np.float64)
    tjm1 = np.zeros(n, dtype=np.float64)

    tkp1 = np.zeros(n, dtype=np.float64)
    tkm1 = np.zeros(n, dtype=np.float64)

    for row in range(n):
        i, j, k = ijk(row, nx, ny)
        if i + 1 < nx and j + 1 < ny and k + 1 < nz:
            i_coefs = compute_coeff_deriv(i, coord[0], coord_vertex[0])
            j_coefs = compute_coeff_deriv(j, coord[1], coord_vertex[1])
            k_coefs = compute_coeff_deriv(k, coord[2], coord_vertex[2])

            tijk[index_c[i, j, k]] = i_coefs[1] + j_coefs[1] + k_coefs[1]

            tip1[index_c[i+1, j, k]] = i_coefs[2]
            tim1[index_c[i-1, j, k]] = i_coefs[0]

            tjp1[index_c[i, j+1, k]] = j_coefs[2]
            tjm1[index_c[i, j-1, k]] = j_coefs[0]

            tkp1[index_c[i, j, k+1]] = k_coefs[2]
            tkm1[index_c[i, j, k-1]] = k_coefs[0]
    return tijk, tip1, tim1, tjp1, tjm1, tkp1, tkm1


def vectorized_sparse_laplacian(coord, coord_vertex):
    coord = [c.astype(np.float64) for c in coord]
    coord_vertex = [c.astype(np.float64) for c in coord_vertex]
    nx, ny, nz = [c.shape[0] for c in coord]
    n = nx * ny * nz

    idx = np.arange(n).reshape(nx, ny, nz, order="F").reshape(-1)

    i, j, k = ijk(idx, nx, ny)
    i_coefs = compute_sparse_coeff_deriv(i, coord[0], coord_vertex[0])
    j_coefs = compute_sparse_coeff_deriv(j, coord[1], coord_vertex[1])
    k_coefs = compute_sparse_coeff_deriv(k, coord[2], coord_vertex[2])
    tijk = - i_coefs[1] - j_coefs[1] - k_coefs[1]
    tip1 = i_coefs[2]
    tim1 = i_coefs[0]
    tjp1 = j_coefs[2]
    tjm1 = j_coefs[0]
    tkp1 = k_coefs[2]
    tkm1 = k_coefs[0]

    from scipy.sparse import dia_array
    elements = np.array([tijk, tip1, tim1, tjp1, tjm1, tkp1, tkm1])
    offsets = np.array([0, 1, -1, nx, - nx, nx*ny, -nx*ny])
    return dia_array((elements, offsets), shape=(n, n))


def laplacian_k(f, x, y, z, coord_vertex):
    dx = x[1] - x[0]
    dy = y[1] - y[0]
    f_x = (f[:-2] - 2 * f[1:-1] + f[2:])/dx**2
    f_x = f_x[..., 1:-1, 1:-1]
    f_y = (f[:, :-2] - 2 * f[:, 1:-1] + f[:, 2:])/dy**2
    f_y = f_y[1:-1, ..., 1:-1]

    f_z = np.zeros_like(f)
    # dzvertex = np.diff(coord_vertex[-1])
    coord_vertex = coord_vertex[-1]
    for i in range(1, len(z)-1):
        hr = z[i+1] - z[i]
        hl = z[i] - z[i-1]

        grad_T_i_p_1_2 = (f[..., i+1] - f[..., i])/(hr)
        grad_T_i_m_1_2 = (f[..., i] - f[..., i - 1])/(hl)
        # z[i+1/2] - z[i-1/2]
        dzv = coord_vertex[i+1]-coord_vertex[i]
        f_z[..., i] = (grad_T_i_p_1_2 - grad_T_i_m_1_2)/(dzv)
    f_z = f_z[1:-1, 1:-1, 1:-1]

    return f_x+f_y+f_z


def lap_recon(f, filter_size, x, y, z, coord_vertex):
    """
    Reconstruct temperature field like so:
    T = (Id - (delta^2)/24)\overbar{T}
    """
    dx = x[1]-x[0]
    dy = y[1]-y[0]
    diff_z = np.diff(coord_vertex[-1])
    window_z_laplacian = convolve(diff_z, np.ones(filter_size), mode="valid")
    laplacian_f = laplacian_k(f, x, y, z, coord_vertex)
    delta = np.cbrt(filter_size**2 * dx * dy * window_z_laplacian)
    recon = f[1:-1, 1:-1, 1:-1] - (delta**2/24)*laplacian_f
    return recon


def lap_recon_non_padded(f, filter_size, x, y, z, coord_vertex):
    """
    Reconstruct temperature field like so:
    T = (Id - (delta^2)/24)\overbar{T}
    Uses the filtered DNS's bordering values to complete
    """
    recon = lap_recon(f, filter_size, x, y, z, coord_vertex)
    recon = np.concatenate(
        [f[0:1, 1:-1, 1:-1], recon, f[-2:-1, 1:-1, 1:-1]], axis=0)
    recon = np.concatenate([f[:, 0:1, 1:-1], recon, f[:, -2:-1, 1:-1]], axis=1)
    recon = np.concatenate([f[..., 0:1], recon, f[..., -2:-1]], axis=2)
    recon[..., 1] = f[..., 1]
    recon[..., -2] = f[..., -2]
    return recon


def lap_recon_padded(f, filter_size, x, y, z, coord_vertex, h, tmin, tmax):
    """
    Reconstruct temperature field like so:
    T = (Id - (delta^2)/24)\overbar{T}
    With padding on sides
    """
    dx = x[1]-x[0]
    dy = y[1]-y[0]
    f = np.pad(f, ((1, 1), (1, 1), (0, 0)), "wrap")
    f = np.pad(f, ((0, 0), (0, 0), (1, 1)), "constant",
               constant_values=(tmin, tmax))
    diff_z = np.diff(coord_vertex[-1])
    padded_diff = np.pad(diff_z, (1, 1), "edge")
    windowed_z = convolve(padded_diff, np.ones(filter_size), mode="valid")
    first_diff_ = last_diff_ = z[1] - z[0]
    coord_z = np.array([z[0] - first_diff_, *z, z[-1] + last_diff_])
    first_diff = last_diff = coord_vertex[-1][1] - coord_vertex[-1][0]
    coord_vertex_ = np.array(
        [[coord_vertex[-1][0] - first_diff, *coord_vertex[-1], coord_vertex[-1][-1]+last_diff]])
    laplacian_f = laplacian_k(f, x, y, coord_z, coord_vertex_)
    delta = np.cbrt(filter_size**2*dx*dy*windowed_z)
    recon = f[1:-1, 1:-1, 1:-1] - (delta**2/24)*laplacian_f
    return recon


def lap_recon_fine_non_padded_(
    f, filter_size, coord_les, coord_vertex_les, coord, coord_vertex
):
    """
    Reconstruct the temperature field like so:
    T = (Id - (delta^2)/24)\overbar{T}
    Reconstruction of laplacian using interpolated delta
    """
    x, y, _ = coord_les
    dx = x[1] - x[0]
    dy = y[1] - y[0]
    diff_z = np.diff(coord_vertex_les[-1])
    window_z_laplacian = convolve(diff_z, np.ones(filter_size), mode="valid")
    laplacian_f = laplacian_k(f, *coord, coord_vertex)
    delta = np.cbrt(filter_size**2 * dx * dy * window_z_laplacian)
    interpolator = interp1d(
        coord_les[-1][1:-1], delta, fill_value="extrapolate")
    delta_ = interpolator(coord[-1][1:-1])
    recon = f[1:-1, 1:-1, 1:-1] - (delta_**2/24)*laplacian_f
    return recon


def lap_recon_fine_non_padded(
    f, filter_size, coord_les, coord_vertex_les, coord, coord_vertex
):
    recon = lap_recon_fine_non_padded_(
        f, filter_size, coord_les, coord_vertex_les, coord, coord_vertex
    )
    recon = np.concatenate(
        [f[0:1, 1:-1, 1:-1], recon, f[-2:-1, 1:-1, 1:-1]], axis=0)
    recon = np.concatenate([f[:, 0:1, 1:-1], recon, f[:, -2:-1, 1:-1]], axis=1)
    recon = np.concatenate([f[..., 0:1], recon, f[..., -2:-1]], axis=2)
    recon[..., 1] = f[..., 1]
    recon[..., -2] = f[..., -2]
    return recon


def recon_adrien_coarse(T_les, T_filtered_twice, coord_les, coord_vertex_les):
    """
    Reconstruction of T such that:
    T = (Id - C)\overbar{T}
    Where
    C = \frac{\overbar{T} - \overbar{\overbar{T}}}{\frac{\partial^2 \overbar{\overbar{T}}}{\partial y^2}}
    Example use:
    >>> T_lap_adrien_coarse = recon_adrien_coarse(T_les, T_2_filtered_gros, coord_les, coord_vertex_les)
    """
    lap_t_les = lap_recon_non_padded(T_les, 3, *coord_les, coord_vertex_les)
    lap_t_filter_twice = lap_recon_non_padded(T_filtered_twice, 3, *coord_les, coord_vertex_les)
    c_coarse = (T_les - T_filtered_twice)/lap_t_filter_twice

    T_lap_adrien_coarse = T_les - c_coarse * lap_t_les
    return T_lap_adrien_coarse
