"""
Cropping in 3 dimensions as well as random patch crops
"""
from typing import List, Tuple, Sequence, Union
import numbers
from torch import Tensor
import numpy as np
from torch.nn import Module
from torchvision.utils import _log_api_usage_once
import torch


def _setup_size(size):
    if isinstance(size, numbers.Number):
        return int(size), int(size), int(size)

    if isinstance(size, Sequence) and len(size) == 1:
        return size[0], size[0], size[0]

    return size


def _is_tensor_a_torch_image(x: Tensor) -> bool:
    return x.ndim >= 2


def _is_all_tensor_a_torch_image(x: Union[Tensor, tuple]) -> bool:
    if isinstance(x, tuple):
        return torch.all(
            torch.tensor([_is_tensor_a_torch_image(i)
                         for i in x], dtype=torch.bool)
        ).item()
    else:
        return _is_tensor_a_torch_image(x)


def _assert_image_tensor(img: Tensor) -> None:
    if not _is_all_tensor_a_torch_image(img):
        raise TypeError("Tensor is not a torch image.")


def get_dimensions3d(img: Tensor) -> List[int]:
    _assert_image_tensor(img)
    channels = 1 if img.ndim == 3 else img.shape[1]
    height, width, depth = img.shape[-3:]
    return [channels, height, width, depth]


def crop3d(img: Tensor,
           top: int,
           left: int,
           front: int,
           height: int,
           width: int,
           depth: int) -> Tensor:
    """Crop the given image at specified location and output size.
    If the image is torch Tensor, it is expected
    to have [..., H, W, D] shape, where ... means an arbitrary number of
    leading dimensions. If image size is smaller than output size along
    any edge, image is padded with 0 and then cropped.

    Parameters:
    -----------
        img (PIL Image or Tensor): Image to be cropped. (0,0) denotes the
            top left corner of the image
        top (int): Vertical component of the top left corner of the crop box
        left (int): Horizontal component of the top left corner of the crop box
        front (int): Depth component of the closest point to the camera when
            looking at the crop box
        height (int): Height of the crop box.
        width (int): Width of the crop box.
        depth (int): Depth of the crop box.

    Returns:
    --------
        PIL Image or Tensor: Cropped image.

    """
    _assert_image_tensor(img)

    # _, h, w, d = get_dimensions3d(img)
    right = left + width
    bottom = top + height
    back = front + depth

    # if left < 0 or top < 0 or front < 0 or right > w or bottom > h or back > d:
    #     padding_ltrb = [
    #         # Left <=> Right
    #         max(-left, 0),
    #         max(right - w, 0),
    #         # Top <=> Bottom
    #         max(-top, 0),
    #         max(bottom - h, 0),
    #         # Front <=> Back
    #         max(-front, 0),
    #         max(back - d, 0)
    #     ]
    #     return pad(
    #         img[..., max(top, 0): bottom,
    #             max(left, 0): right,
    #             max(front, 0): back],
    #         padding_ltrb
    #     )
    return img[..., top:bottom, left:right, front:back]


class UniformCrop(Module):
    @staticmethod
    def get_params(
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size):
        super().__init__()
        _log_api_usage_once(self)

        self.size = tuple(_setup_size(size))

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """

        i, j, k, h, w, d = self.get_params(img, self.size)

        return crop3d(img, i, j, k, h, w, d)

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}(kernel_size={self.kernel_size},"
        "sigma={self.sigma})"
        return s

# TODO: Create a class that does a random crop but random
# for each image of the batch


class PairCrop(Module):
    @staticmethod
    def get_params(
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size):
        super().__init__()
        _log_api_usage_once(self)
        self.size = tuple(_setup_size(size))

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), \
                crop3d(img2, i, j, k, h, w, d)
        else:
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d)

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}({self.size})"
        "sigma={self.sigma})"
        return s


class QuasiMonteCarloCrop(Module):
    def get_params(
        self,
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = np.floor(self.sobol.draw(1).item() * (d - td + 1)).astype(int)
        # k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size):
        super().__init__()
        _log_api_usage_once(self)
        self.size = tuple(_setup_size(size))
        self.sobol = torch.quasirandom.SobolEngine(1, scramble=True)

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), \
                crop3d(img2, i, j, k, h, w, d)
        else:
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d)

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}({self.size})"
        "sigma={self.sigma})"
        return s


class QuasiMonteCarloCrop_tracable_k(Module):
    def get_params(
        self,
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = np.floor(self.sobol.draw(1).item() * (d - td + 1)).astype(int)
        # k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size, coord_elem=None, coord_face=None):
        super().__init__()
        _log_api_usage_once(self)
        self.size = tuple(_setup_size(size))
        self.sobol = torch.quasirandom.SobolEngine(1, scramble=True)
        self.coord_elem = coord_elem
        self.coord_face = coord_face
        assert self.coord_face is not None and self.coord_elem is not None, """
        You need to provide the coordinates at the elements and the faces.
        """

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), \
                crop3d(img2, i, j, k, h, w, d), \
                self.coord_elem[k:k+d], \
                self.coord_face[k:k+d+1]
        else:
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), \
                self.coord_elem[k:k+d], \
                self.coord_face[k:k+d+1]

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}({self.size})"
        "sigma={self.sigma})"
        return s


class IJCrop(Module):
    def get_params(
        self,
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = 0
        # k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size):
        super().__init__()
        _log_api_usage_once(self)
        self.size = tuple(_setup_size(size))
        self.sobol = torch.quasirandom.SobolEngine(1, scramble=True)

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            i, j, _, h, w, _ = self.get_params(img, self.size)
            return crop3d(img, i, j, 0, h, w, img.size(-1)), \
                crop3d(img2, i, j, 0, h, w, img.size(-1))
        else:
            i, j, _, h, w, _ = self.get_params(img, self.size)
            return crop3d(img, i, j, 0, h, w, img.size(-1))

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}({self.size})"
        "sigma={self.sigma})"
        return s


class TracableCrop(Module):
    @staticmethod
    def get_params(
        img: Tensor,
        output_size: Tuple[int, int, int]
    ) -> Tuple[int, int, int, int, int, int]:
        """Get parameters for ``crop`` for a random crop.

        Args:
            img (PIL Image or Tensor): Image to be cropped.
            output_size (tuple): Expected output size of the crop.

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop
        """
        _, h, w, d = get_dimensions3d(img)
        th, tw, td = output_size

        if h + 1 < th or w + 1 < tw or d + 1 < td:
            raise ValueError(
                f"Required crop size {(th, tw, td)} is larger than input image"
                "size {(h, w, d)}")

        if w == tw and h == th and d == td:
            return 0, 0, 0, h, w, d

        i = torch.randint(0, h - th + 1, size=(1,)).item()
        j = torch.randint(0, w - tw + 1, size=(1,)).item()
        k = torch.randint(0, d - td + 1, size=(1,)).item()
        return i, j, k, th, tw, td

    def __init__(self, size):
        super().__init__()
        _log_api_usage_once(self)
        self.size = tuple(_setup_size(size))

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped.

        Returns:
            PIL Image or Tensor: Cropped image.
        """
        if isinstance(img, tuple):
            img, img2 = img
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), \
                crop3d(img2, i, j, k, h, w, d), \
                i, j, k
        else:
            i, j, k, h, w, d = self.get_params(img, self.size)
            return crop3d(img, i, j, k, h, w, d), i, j, k

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}({self.size})"
        "sigma={self.sigma})"
        return s


def get_params(
    img: Tensor,
    output_size: Tuple[int, int, int]
) -> Tuple[int, int, int]:
    """Get parameters for ``crop`` for a random crop.
    Args:
        img (PIL Image or Tensor): Image to be cropped.
        output_size (tuple): Expected output size of the crop.
    Returns:
        tuple: params (i, j, k, h, w, d) to be passed to ``crop`` for random
        crop
    """
    _, h, w, d = get_dimensions3d(img)
    bs = img.shape[0]
    th, tw, td = _setup_size(output_size)
    if w == tw and h == th and d == td:
        return 0, 0, 0, h, w, d
    i = torch.randint(0, h - th + 1, size=(bs,))
    j = torch.randint(0, w - tw + 1, size=(bs,))
    k = torch.randint(0, d - td + 1, size=(bs,))
    return i, j, k


def get_end_(
    img: Tensor,
    output_size: Tuple[int, int, int]
) -> Tuple[int, int, int, int, int, int, ]:
    left, top, front = get_params(img, output_size)
    # os = get_dimensions3d(output_size)
    os = output_size
    right = left + os[0]
    bottom = top + os[1]
    back = front + os[2]

    return left, right, top, bottom, front, back


# def get_indexes(
#     img: Tensor,
#     output_size: Tuple[int, int, int]
# ):
#     left, right, top, bottom, front, back = get_end_(img, output_size)
#     idx = [torch.arange(ll.item(), rr.item()) for ll, rr in zip(left, right)]
#     idy = [torch.arange(tt.item(), bb.item()) for tt, bb in zip(top, bottom)]
#     idz = [torch.arange(ff.item(), bb.item()) for ff, bb in zip(front, back)]


if __name__ == "__main__":
    img = torch.randn(1, 1, 5, 5, 5)
    cropped = crop3d(img, 0, 0, 0, 2, 2, 2)
    cropped2 = crop3d(img, -2, -2, -2, 3, 3, 3)
    print(cropped.shape)
    print(cropped2.shape)
