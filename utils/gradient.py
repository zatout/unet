import numpy as np
import torch


def gradi(x):
    return torch.gradient(x, dim=0)[0]


def gradj(x):
    return torch.gradient(x, dim=1)[0]


def gradk(x, coord):
    return torch.gradient(x, spacing=(coord,), dim=2)[0]


def dtdy(T, coord_les):
    k = np.arange(0, T.shape[-1] - 1)
    _, _, z = coord_les
    dtdy = np.zeros([*T.shape[:-1], T.shape[-1] + 1])
    dtdy[..., 1:-1] = (T[..., k+1] - T[..., k])/(z[k+1] - z[k])
    d1, d2 = z[1] - z[0], z[2] - z[0]
    d12 = z[2] - z[1]
    dtdy[..., 0] = - d1*d2/d12 * (T[..., 2] / (d2**2) - T[..., 1] / (
        d1**2) - T[..., 0] * ((d1**2 - d2**2)/((d1**2) * (d2**2))))
    dtdy[..., -1] = d1*d2/d12 * (T[..., -3] / (d2**2) - T[..., -2] / (
        d1**2) - T[..., -1] * ((d1**2 - d2**2)/((d1**2) * (d2**2))))
    return dtdy


def dtdy_torch(T, coord_les):
    device = T.device
    k = np.arange(0, T.shape[-1] - 1)
    _, _, z = coord_les
    z = torch.tensor(z).to(device)
    dtdy = torch.zeros([*T.shape[:-1], T.shape[-1] + 1]).to(device)
    dtdy[..., 1:-1] = (T[..., k+1] - T[..., k])/(z[k+1] - z[k])
    d1, d2 = z[1] - z[0], z[2] - z[0]
    d12 = z[2] - z[1]
    dtdy[..., 0] = - d1*d2/d12 * (T[..., 2] / (d2**2) - T[..., 1] / (
        d1**2) - T[..., 0] * ((d1**2 - d2**2)/((d1**2) * (d2**2))))
    dtdy[..., -1] = d1*d2/d12 * (T[..., -3] / (d2**2) - T[..., -2] / (
        d1**2) - T[..., -1] * ((d1**2 - d2**2)/((d1**2) * (d2**2))))
    return dtdy


def d2tdy2(T, dtdy, coord_les):
    _, _, z = coord_les
    k = np.arange(T.shape[-1])
    out = (dtdy[..., k+1] - dtdy[..., k])/(z[k+1] - z[k])
    return out


def d2tdy2_torch(T, dtdy, coord_les):
    _, _, z = coord_les
    k = np.arange(T.shape[-1])
    out = (dtdy[..., k+1] - dtdy[..., k])/(z[k+1] - z[k])
    return out
