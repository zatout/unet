from typing import Type, Union, Tuple, List
from torchvision.transforms import Compose


from .crop import QuasiMonteCarloCrop, IJCrop, QuasiMonteCarloCrop_tracable_k
from .flip import RandomHorizontalFlip, RandomVerticalFlip
from .rotate import Random90RotationTemperatureSymmetry


def get_transforms_no_depth_flip(
    crop: bool = True,
    flip: bool = True,
    rotate: bool = True,
    *,
    crop_size: Union[int, Tuple] = (16, 16, 16),
    flip_prob: float = 1/3.,
    n_rot: int = 4
) -> Type[Compose]:
    """
    Function that creates transforms: Cropping, flipping and rorating a 3D
    image
    Parameters:
    ----------
    crop: bool
        Whether to crop or not
    flip:bool
        Whether to flip or not
    rotate
        Whether to rotate or not

    kwargs:
    crop_size: Union[int, Tuple]
        Size of the crop taken in 3D
    flip_prob: float
        The probability to flip your image
    n_rot: int
        Number of maximum rotations to take
    Returns:
    -------
    scripted: Callable
    """
    transform_list = []
    if crop:
        transform_list.append(QuasiMonteCarloCrop(crop_size))
    if flip:
        transform_list.append(RandomHorizontalFlip(flip_prob))
        transform_list.append(RandomVerticalFlip(flip_prob))
    if rotate:
        transform_list.append(Random90RotationTemperatureSymmetry(n_rot))

    return Compose(transform_list)


def get_transforms_no_y_sampling(
    crop: bool = True,
    flip: bool = True,
    rotate: bool = True,
    *,
    crop_size: Union[int, Tuple] = (16, 16, 16),
    flip_prob: float = 1/3.,
    n_rot: int = 4
) -> Type[Compose]:
    """
    Function that creates transforms: Cropping, flipping and rorating a 3D
    image
    Parameters:
    ----------
    crop: bool
        Whether to crop or not
    flip:bool
        Whether to flip or not
    rotate
        Whether to rotate or not

    kwargs:
    crop_size: Union[int, Tuple]
        Size of the crop taken in 3D
    flip_prob: float
        The probability to flip your image
    n_rot: int
        Number of maximum rotations to take
    Returns:
    -------
    scripted: Callable
    """
    transform_list = []
    if crop:
        transform_list.append(IJCrop(crop_size))
    if flip:
        transform_list.append(RandomHorizontalFlip(flip_prob))
        transform_list.append(RandomVerticalFlip(flip_prob))
    if rotate:
        transform_list.append(Random90RotationTemperatureSymmetry(n_rot))

    return Compose(transform_list)


def get_transforms_and_coord(
    crop: bool = True,
    flip: bool = True,
    rotate: bool = True,
    *,
    crop_size: Union[int, Tuple] = (16, 16, 16),
    coord_elem: Union[None, List] = None,
    coord_face: Union[None, List] = None,
    flip_prob: float = 1/3.,
    n_rot: int = 4
) -> Type[Compose]:
    transform_list = []
    if flip:
        transform_list.append(RandomHorizontalFlip(flip_prob))
        transform_list.append(RandomVerticalFlip(flip_prob))
    if rotate:
        transform_list.append(Random90RotationTemperatureSymmetry(n_rot))
    if crop:
        transform_list.append(QuasiMonteCarloCrop_tracable_k(
            crop_size, coord_elem=coord_elem, coord_face=coord_face))

    return Compose(transform_list)
