import numpy as np
from matplotlib import pyplot as plt


def plot_slices(dns_, les_, nn_pre_trained, nn_post_trained, scale_sim, *, slices=[1, 8, 13], subslice=-16):
    for i, sl in enumerate(slices):
        fig, ax = plt.subplots(1, 5, figsize=(9*3.0, 10/3), sharex=True, sharey=True)
        vmin = np.    min(dns_           [0].squeeze()[..., subslice:][..., sl])
        vmax = np.    max(dns_           [0].squeeze()[..., subslice:][..., sl])
        im = ax[0].imshow(dns_           [0].squeeze()[..., subslice:][..., sl], cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[1]     .imshow(les_           [0].squeeze()[..., subslice:][..., sl], cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[2]     .imshow(nn_pre_trained [0].squeeze()[..., subslice:][..., sl], cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[3]     .imshow(nn_post_trained[0].squeeze()[..., subslice:][..., sl], cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[4]     .imshow(scale_sim      [0].squeeze()[..., subslice:][..., sl], cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        
        plt.subplots_adjust(hspace=0.03, wspace=0.1)
        # plt.subplots_adjust(hspace=0.1, wspace=0.0)
        ax[0].set_title(r"DNS (aim)")
        ax[1].set_title(r"LES (input)")
        ax[2].set_title(r"Neural net (RMSE)") # , x=.5, y=1.1)
        ax[3].set_title(r"Neural net (2 pts cor)") #  , x=.5, y=1.1)
        ax[4].set_title(r"Van Cittert (litterature)") #  , x=.5, y=1.1)
        cbar_ax = fig.add_axes([.25, -0.05, 0.5, 0.05])
        cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
        cbar.set_ticks([
            *np.linspace(int(vmin)+1, int(vmax), 6, dtype=int) #int((max_ + min_)/2),int(max_-1)
        ])
        fig.text(0.075, 0.5, r"$z$ ($m$)", va='center', rotation='vertical')
        fig.text(0.5,  0.05, r"$x$ ($m$)", va='center', rotation='horizontal')
        cbar.ax.xaxis.set_label_position('top')
        fig.text(0.5, -0.05+0.050/2, r"$T$ ($K$)", va='center', rotation='horizontal')

def plot_slices_diff(dns_, les_, nn_pre_trained, nn_post_trained, scale_sim, *, slices=[1, 8, 13], subslice=-16):
    for i, sl in enumerate(slices):
        fig, ax = plt.subplots(1, 3, figsize=(9*3.0, 10/3), sharex=True, sharey=True)
        vmin = np    .min((dns_[0].squeeze()[..., subslice:][..., sl]-nn_pre_trained [0].squeeze()[..., subslice:][..., sl]))
        vmax = np    .max((dns_[0].squeeze()[..., subslice:][..., sl]-nn_pre_trained [0].squeeze()[..., subslice:][..., sl]))
        im = ax[0]   .imshow((dns_[0].squeeze()[..., subslice:][..., sl]-nn_pre_trained [0].squeeze()[..., subslice:][..., sl]), cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[1]        .imshow((dns_[0].squeeze()[..., subslice:][..., sl]-nn_post_trained[0].squeeze()[..., subslice:][..., sl]), cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        ax[2]        .imshow((dns_[0].squeeze()[..., subslice:][..., sl]-scale_sim      [0].squeeze()[..., subslice:][..., sl]), cmap="bwr", vmin=vmin, vmax=vmax, extent=[0, 2, 0, 1])
        
        plt.subplots_adjust(hspace=0.03, wspace=0.1)
        # plt.subplots_adjust(hspace=0.1, wspace=0.0)
        ax[0].set_title(r"Neural net (RMSE)") # , x=.5, y=1.1)
        ax[1].set_title(r"Neural net (2 pts cor)") #  , x=.5, y=1.1)
        ax[2].set_title(r"Van Cittert (litterature)") #  , x=.5, y=1.1)
        cbar_ax = fig.add_axes([.25, -0.05, 0.5, 0.05])
        cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
        cbar.set_ticks([
            *np.linspace(int(vmin)+1, int(vmax), 6, dtype=int) #int((max_ + min_)/2),int(max_-1)
        ])
        fig.text(0.075, 0.5, r"$z$ ($m$)", va='center', rotation='vertical')
        fig.text(0.5,  0.05, r"$x$ ($m$)", va='center', rotation='horizontal')
        cbar.ax.xaxis.set_label_position('top')
        fig.text(0.5, -0.05+0.050/2, r"$T$ ($K$)", va='center', rotation='horizontal')
