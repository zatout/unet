import matplotlib.pyplot as plt
from functools import lru_cache
from typing import Union, Tuple
from collections.abc import Sequence
import numbers
import torch
from torch import Tensor
from torch.nn import functional as F
import numpy as np
from matplotlib.colors import LinearSegmentedColormap

# from .data_loading import read_p_thermo
# from torchvision.utils import _log_api_usage_once


def select_patch(data: Tensor, patch_size: Union[int, Tuple]):
    """
    From the given data, randomly selects a patch of data in 3D
    Parameters:
    ----------
    data: Tensor
        Data to select the patch from
    patch_size: Union[int, Tuple]
        Size of the patch in each of the 3 directions of space
    Returns:
    ----------
    patch: Tensor
        Patch in 3D space of the shape (..., *patch_size)
    """
    if isinstance(patch_size, int):
        patch_size = (patch_size, patch_size, patch_size)
    shape = data.shape
    slices_begin = torch.tensor([0, 0, 0])
    for i, s in enumerate(shape[-3:]):
        slices_begin[i] = torch.randint(low=0, high=s - patch_size[i], size=(1,))
    return data[
        ...,
        slices_begin[0] : slices_begin[0] + patch_size[0],
        slices_begin[1] : slices_begin[1] + patch_size[1],
        slices_begin[2] : slices_begin[2] + patch_size[2],
    ]


def get_dimensions(img):
    c, x, y, z = img.shape[-2:]
    return [c, x, y, z]


def _setup_size3(size, error_msg):
    if isinstance(size, numbers.Number):
        return size, size, size

    if isinstance(size, Sequence) and len(size) == 1:
        return size[0], size[0], size[0]

    if len(size) != 3:
        raise ValueError(error_msg)

    return size


def crop(img: Tensor, _from: Tuple[int, int, int], size: Tuple[int, int, int]):
    _, x, y, z = get_dimensions(img)
    xto = x + size[0]
    yto = y + size[1]
    zto = z + size[2]
    xfrom, yfrom, zfrom = _from
    if x < 0 or y < 0 or z < 0 or xto > x or yto > y or zto > z:
        padding_ltrb = [
            max(-xfrom, 0),
            max(-yfrom, 0),
            max(-zfrom, 0),
            max(xto - x, 0),
            max(yto - y, 0),
            max(zto - z, 0),
        ]
        return F.pad(
            img[..., max(xfrom, 0) : xto, max(yfrom, 0) : yto, max(zfrom, 0) : zto],
            padding_ltrb,
        )
    pass


def _is_tensor_a_torch_image(x: Tensor) -> bool:
    return x.ndim >= 2


def _assert_image_tensor(img: Tensor) -> None:
    if not _is_tensor_a_torch_image(img):
        raise TypeError("Tensor is not a torch image.")


def vflip3d(img: Tensor) -> Tensor:
    _assert_image_tensor(img)
    return img.flip(-3)


def hflip3d(img: Tensor) -> Tensor:
    _assert_image_tensor(img)
    return img.flip(-2)


def dflip3d(img: Tensor) -> Tensor:
    _assert_image_tensor(img)
    return img.flip(-1)


def shape(x):
    return [i.shape for i in x]


def load_fortran_to_c(path, shape):
    """
    Loads a file with the given path name, and converts it from C to F indexing
    """
    out = np.fromfile(path, dtype=np.float32)
    out = out.reshape(*shape, order="F")
    out = out.reshape(-1)
    return out


def get_coordinates_n_by_3(coord):
    """
    translates coordinates from a 3 array form to a number of points * 3 forms
    """
    coord_3 = np.meshgrid(*coord, indexing="ij")
    coord_3 = np.concatenate([ss.ravel()[np.newaxis] for ss in coord_3]).T

    return coord_3


@lru_cache
def map_3_to_1(ni: int, nj: int, nk: int, order="F"):
    if order == "F":
        return np.arange(ni * nj * nk).reshape(ni, nj, nk, order="F")
    else:
        return np.arange(ni * nj * nk).reshape(ni, nj, nk)


def load_patch_f(file, i, j, k, sizei, sizej, sizek, ni, nj, nk, dtype=np.float32):
    out = np.empty((sizei, sizej, sizek), dtype=dtype)
    indexer = map_3_to_1(ni, nj, nk, order="F")
    for ek, kk in enumerate(range(k, k + sizek)):
        for ej, jj in enumerate(range(j, j + sizej)):
            offset = indexer[i, jj, kk] * (out.dtype.itemsize)
            out[..., ej, ek] = np.fromfile(
                file, dtype=dtype, offset=offset, count=sizei
            )
    return out


def load_patch_c(file, i, j, k, sizei, sizej, sizek, ni, nj, nk, dtype=np.float64):
    out = np.empty((sizei, sizej, sizek), dtype=dtype)
    indexer = map_3_to_1(ni, nj, nk, order="C")
    for ei, ii in enumerate(range(i, i + sizei)):
        for ej, jj in enumerate(range(j, j + sizej)):
            offset = indexer[ii, jj, k] * (out.dtype.itemsize)
            out[ei, ej] = np.fromfile(file, dtype=dtype, offset=offset, count=sizek)
    return out


def load_patch(
    file, i, j, k, sizei, sizej, sizek, ni, nj, nk, dtype=np.float32, order="F"
):
    if order == "F":
        return load_patch_f(file, i, j, k, sizei, sizej, sizek, ni, nj, nk, dtype=dtype)
    else:
        return load_patch_c(file, i, j, k, sizei, sizej, sizek, ni, nj, nk, dtype=dtype)


def test_patch():
    size = 100
    tensor = torch.randn(1, 1, size, size, size)
    patch = select_patch(tensor, 16)
    assert patch.shape[-3:] == (16, 16, 16), "Error while trying to take patch"
    tensor = torch.randn(1, 3, size, size, size)
    patch = select_patch(tensor, 16)


def create_smooth_colormap():
    colors = ["blue", "red"]  # Define the colors for the gradient

    cmap = LinearSegmentedColormap.from_list("CustomGradient", colors)
    cmap.set_bad("white")  # Set the color for invalid/missing values

    return cmap


def show(img, cmap: str = "RdBu_r", h: float = 0, title=None):
    extent = [0, 2 * np.pi * h, 0, 4 * np.pi * h]
    plt.figure()
    plt.imshow(img, cmap=cmap, extent=extent)
    plt.xlabel(r"$x$ ($m$)")
    plt.ylabel(r"$y$ ($m$)")
    plt.title(title)


def show_images(*images, cmap: str = "RdBu_r", h: float = 0.0, title=None):
    extent = [0, 2 * np.pi * h, 0, 4 * np.pi * h]
    if h == 0:
        extent = None
    fig, axs = plt.subplots(1, len(images))  # , figsize=(10, 5*len(images)))
    vmin = np.min(images)
    vmax = np.max(images)
    for i, image in enumerate(images):
        axs[i].imshow(image, extent=extent, cmap=cmap, vmin=vmin, vmax=vmax)
        axs[i].axis("off")
    plt.tight_layout()
    plt.show()


def memory_check():
    print(
        "torch.cuda.memory_allocated: %fGB"
        % (torch.cuda.memory_allocated(0) / 1024 / 1024 / 1024)
    )
    print(
        "torch.cuda.memory_reserved: %fGB"
        % (torch.cuda.memory_reserved(0) / 1024 / 1024 / 1024)
    )
    print(
        "torch.cuda.max_memory_reserved: %fGB"
        % (torch.cuda.max_memory_reserved(0) / 1024 / 1024 / 1024)
    )


def mmm(x):
    return torch.min(x), torch.max(x), torch.mean(x)


def seed_everything(seed: int):
    # https://gist.github.com/ihoromi4/b681a9088f348942b01711f251e5f964
    import random, os
    import numpy as np
    import torch
    
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True
