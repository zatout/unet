import torch
from torch.nn import functional as F
import numpy as np
from .laplace import pad_end_to_end_xz


def cor_k(x, x_pad, k):
    return F.conv2d(x_pad[..., k].swapaxes(0, 1), x[..., k], groups=len(x)).sum(1)[0]

def rms_k(x, x_pad, k, npts):
    return F.conv2d(x_pad[..., k].swapaxes(0, 1), x[..., k], groups=len(x)).sum(1)[0]/npts - x[..., k].mean()**2

def rms_k_fixed_height(x, x_pad, k, npts):
    return F.conv2d(x_pad.swapaxes(0, 1), x, groups=len(x)).sum(1)[0]/npts - x[..., k].mean()**2

def cor(x, x_pad, npts):
    return torch.stack([F.conv2d(x_pad[..., k].swapaxes(0, 1), x[..., k], groups=len(x)).sum(1)[0] for k in range(x.shape[-1])], -1)/npts - x.squeeze().mean((0, 1, 2)) ** 2

def rms(x, x_pad, npts):
    return torch.stack([
        F.conv2d(x_pad[..., k].swapaxes(0, 1), x[..., k], groups=len(x)).sum(1)[0] for k in range(x.shape[-1])
    ], -1)/npts - x.squeeze().mean((0, 1, 2)) ** 2

def check_grad_norm(model):
    grads = []
    for p in model.parameters():
        grads.append(torch.norm(p.grad).detach().cpu().numpy())
    return np.linalg.norm(grads)


@torch.no_grad()
def compute_2_pt_cor(x):
    try:
        x = torch.from_numpy(x).to("cuda").squeeze().unsqueeze(1)
    except:
        pass
    x_pad = pad_end_to_end_xz(x)
    npts = np.prod(x.squeeze().shape[:-1])
    return rms(x, x_pad, npts).cpu().squeeze()
