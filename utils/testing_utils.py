from pathlib import Path
from typing import Union, List
import torch
import numpy as np
from datahandling.error_quantification import (
    compute_rms, compute_all_errors
)
import os
from glob import glob
from .data_loading import load_les, load_dns, load_coarse_les, unnormalize
from .reconstruction import scale_sim_recon
from .visualisation.visual_utils import (
    save_grad_profile,
    save_log_error_profile,
    save_rms_profile,
    save_rms_lin_interp_cnn,
    save_grad_profile,
    save_rms_grad
)
from .data_loading import (
    load_les,
    load_dns,
    load_coarse_les,
    unnormalize,
    load_dns_coord
)
from .reconstruction import scale_sim_recon


def evaluate_on_whole_image(
        les, model, crop_size=16, tmin=293, tmax=586, device="cpu"
):
    # device = "cuda" if torch.cuda.is_available() else "cpu"
    model.eval()
    i, j, k = les.shape[-3:]
    ri, rj, rk = i//crop_size, j//crop_size, k//crop_size

    out = torch.empty_like(les).to(device)
    les = les.to(device)
    with torch.no_grad():
        for i in range(ri):
            for j in range(rj):
                for k in range(rk):
                    islice = slice(i*crop_size, (i+1) * crop_size)
                    jslice = slice(j*crop_size, (j+1) * crop_size)
                    kslice = slice(k*crop_size, (k+1) * crop_size)
                    out[..., islice, jslice, kslice] = unnormalize(
                        les[..., islice, jslice, kslice]
                        + model(les[..., islice, jslice, kslice]), tmin, tmax)
        if i % crop_size:
            for j in range(rj):
                for k in range(rk):
                    islice = slice(-16, None)
                    jslice = slice(j*crop_size, (j+1) * crop_size)
                    kslice = slice(k*crop_size, (k+1) * crop_size)
                    out[..., islice, jslice, kslice] = unnormalize(
                        les[..., islice, jslice, kslice]
                        + model(les[..., islice, jslice, kslice]), tmin, tmax)
        if j % crop_size:
            for i in range(ri):
                for k in range(rk):
                    islice = slice(i*crop_size, (i+1) * crop_size)
                    jslice = slice(-16, None)
                    kslice = slice(k*crop_size, (k+1) * crop_size)
                    out[..., islice, jslice, kslice] = unnormalize(
                        les[..., islice, jslice, kslice]
                        + model(les[..., islice, jslice, kslice]), tmin, tmax)
        if k % crop_size:
            for i in range(ri):
                for j in range(rj):
                    islice = slice(i*crop_size, (i+1) * crop_size)
                    jslice = slice(j*crop_size, (j+1) * crop_size)
                    kslice = slice(-16, None)
                    out[..., islice, jslice, kslice] = unnormalize(
                        les[..., islice, jslice, kslice]
                        + model(les[..., islice, jslice, kslice]), tmin, tmax)
    return out.to("cpu").numpy()[0, 0]


def test_net(
    net,
    dns_path,
    dns_shape,
    les_path,
    les_shape,
    les_coarse_path,
    les_coarse_shape,
    tmin,
    tmax,
    savepath,
):
    suffix = dns_path.split(".sauv.lata.0")[0].split("dns_lata_")[1]
    # glob(les_path+f"/T_*{suffix}.npy")[0]
    T_dns, coord_dns = load_dns(dns_path, dns_shape)
    T_les_interpolated = load_les(les_path, dns_shape)
    T_les_coarse, coord_les, steps_les = load_coarse_les(
        les_coarse_path, coord_dns, les_coarse_shape)
    T_dns_rms = compute_rms(T_dns)
    grad_T_dns = np.gradient(T_dns, *coord_dns, edge_order=2)
    gradient_rms_profile_dns = [compute_rms(gradi) for gradi in grad_T_dns]
    # T_dns_gradient = np.gradient(T_dns, *coord_dns, edge_order=2)

    T_les_torch = T_les_interpolated.unsqueeze(0).unsqueeze(0)
    reconstructed = evaluate_on_whole_image(
        T_les_torch, net, crop_size=16, tmin=tmin, tmax=tmax)
    # T_les, coord_les, spacing, dns_3, nles, ndns, filter_size = 3

    dns_3 = np.meshgrid(*coord_dns, indexing="ij")
    dns_3 = np.concatenate([ss.ravel()[np.newaxis]
                           for ss in dns_3]).T
    nles = les_shape
    ndns = dns_shape
    T_bar_bar = scale_sim_recon(
        T_les_coarse, coord_les, steps_les, dns_3, nles, ndns, filter_size=3
    )
    T_scale_sim = 2*T_les_interpolated.numpy() - T_bar_bar

    error_profile_lin_interp, \
        rms_profile_lin_interp, \
        gradient_profile_lin_interp, \
        gradient_rms_profile_lin_interp = compute_all_errors(
            T_dns,
            T_les_interpolated.numpy(),
            coord_dns
        )
    error_profile_pred, \
        rms_profile_pred, \
        gradient_profile_pred, \
        gradient_rms_profile_pred = compute_all_errors(
            T_dns,
            reconstructed,
            coord_dns
        )
    error_profile_scale_sim, \
        rms_profile_scale_sim, \
        gradient_profile_scale_sim, \
        gradient_rms_profile_scale_sim = compute_all_errors(
            T_dns,
            T_scale_sim,
            coord_dns
        )
    os.makedirs(os.path.join(savepath, "error_profile_pred"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "rms_profile_pred"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "gradient_profile_pred"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "gradient_rms_profile_pred"), exist_ok=True)
    error_profile_pred.tofile(os.path.join(
        savepath, "error_profile_pred", suffix+".npy"))
    rms_profile_pred.tofile(os.path.join(
        savepath, "rms_profile_pred", suffix+".npy"))
    for gp, gp_rms, dir in zip(
        gradient_profile_pred,
        gradient_rms_profile_pred,
        ["x", "y", "z"]
    ):
        gp.tofile(
            os.path.join(
                savepath, "gradient_profile_pred", suffix+f"_{dir}_.npy")
        )
        gp_rms.tofile(
            os.path.join(
                savepath, "gradient_rms_profile_pred", suffix+f"_{dir}_.npy"
            )
        )
    os.makedirs(os.path.join(
        savepath, "error_profile_scale_sim"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "rms_profile_scale_sim"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "gradient_profile_scale_sim"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "gradient_rms_profile_scale_sim"), exist_ok=True)
    error_profile_scale_sim.tofile(os.path.join(
        savepath, "error_profile_scale_sim", suffix+".npy"))
    rms_profile_scale_sim.tofile(
        os.path.join(
            savepath, "rms_profile_scale_sim", suffix+".npy"
        )
    )
    for gp, rms_g, dir in zip(
        gradient_profile_scale_sim,
        gradient_rms_profile_scale_sim,
        ["x", "y", "z"]
    ):
        gp.tofile(
            os.path.join(
                savepath, "gradient_profile_scale_sim", suffix+f"_{dir}_.npy")
        )
        rms_g.tofile(
            os.path.join(
                savepath, "gradient_rms_profile_scale_sim", suffix+f"_{dir}_.npy")
        )

    os.makedirs(os.path.join(
        savepath, "error_profile_lin_interp"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "rms_profile_lin_interp"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "gradient_profile_lin_interp"), exist_ok=True)
    os.makedirs(os.path.join(
        savepath, "gradient_rms_profile_lin_interp"), exist_ok=True)
    error_profile_lin_interp.tofile(
        os.path.join(
            savepath, "error_profile_lin_interp", suffix+".npy"))
    rms_profile_lin_interp.tofile(
        os.path.join(
            savepath, "rms_profile_lin_interp", suffix+".npy"
        )
    )
    for gp_scale_lin_interp, gp_rms, dir in zip(
        gradient_profile_lin_interp,
        gradient_rms_profile_lin_interp,
        ["x", "y", "z"]
    ):
        gp_scale_lin_interp.tofile(
            os.path.join(
                savepath, "gradient_profile_lin_interp", suffix+f"_{dir}_.npy"
            )
        )
        gp_rms.tofile(
            os.path.join(
                savepath, "gradient_rms_profile_lin_interp", suffix +
                f"_{dir}_.npy"
            )
        )

    os.makedirs(os.path.join(savepath, "profiles"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "rms"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "grad"), exist_ok=True)
    normalized_dns_coord = coord_dns[-1]/coord_dns[-1].max()
    save_log_error_profile(
        normalized_dns_coord,
        error_profile_lin_interp,
        error_profile_pred,
        error_profile_scale_sim,
        os.path.join(savepath, "profiles")
    )
    save_rms_profile(
        normalized_dns_coord,
        rms_profile_lin_interp,
        rms_profile_pred,
        rms_profile_scale_sim,
        T_dns_rms,
        os.path.join(savepath, "rms")
    )
    save_rms_lin_interp_cnn(
        normalized_dns_coord,
        rms_profile_lin_interp,
        rms_profile_pred,
        os.path.join(savepath, "rms")
    )
    save_grad_profile(
        normalized_dns_coord,
        gradient_profile_lin_interp,
        gradient_profile_pred,
        gradient_profile_scale_sim,
        os.path.join(savepath, "grad")
    )
    save_rms_grad(
        normalized_dns_coord,
        gradient_rms_profile_dns,
        gradient_rms_profile_lin_interp,
        gradient_rms_profile_pred,
        gradient_rms_profile_scale_sim,
        os.path.join(savepath, "rms")
    )

    # return :
    # Error profiles on T
    # RMS profiles \sqrt{T^{'2}}
    # Error on gradients \nabla{T}
    # RMS of gradients \sqrt{\nabla T^{'2}}
    return (
        error_profile_lin_interp,
        error_profile_pred,
        error_profile_scale_sim
    ), (
        rms_profile_lin_interp,
        rms_profile_pred,
        rms_profile_scale_sim,
        T_dns_rms
    ), (
        gradient_profile_lin_interp,
        gradient_profile_pred,
        gradient_profile_scale_sim,
    ), (
        gradient_rms_profile_lin_interp,
        gradient_rms_profile_pred,
        gradient_rms_profile_scale_sim,
        gradient_rms_profile_dns
    )


def error_profiles_manager(
    error_profile,
    rms_profile,
    grad_profile,
    rms_grad_profile
):
    error_lin_interp, error_pred, error_scale_sim = [], [], []
    rms_lin_interp, rms_pred, rms_scale_sim, rms_dns = [], [], [], []
    grad_lin_interp, grad_pred, grad_scale_sim = [], [], []
    rms_g_lin_interp, rms_g_pred, rms_g_scale_sim, rms_g_dns = [], [], [], []
    for e, rms, g, rms_g in zip(
        error_profile,
        rms_profile,
        grad_profile,
        rms_grad_profile
    ):
        error_lin_interp.append(e[0])
        error_pred.append(e[1])
        error_scale_sim.append(e[2])
        rms_lin_interp.append(rms[0])
        rms_pred.append(rms[1])
        rms_scale_sim.append(rms[2])
        rms_dns.append([3])
        grad_lin_interp.append(g[0])
        grad_pred.append(g[1])
        grad_scale_sim.append(g[2])
        rms_g_lin_interp.append(rms_g[0])
        rms_g_pred.append(rms_g[1])
        rms_g_scale_sim.append(rms_g[2])
        rms_g_dns.append(rms_g[3])
    mean_error_lin_interp = np.mean(np.stack(error_lin_interp), axis=0)
    mean_error_pred = np.mean(np.stack(error_pred), axis=0)
    mean_error_scale_sim = np.mean(np.stack(error_scale_sim), axis=0)

    mean_rms_lin_interp = np.mean(np.stack(rms_lin_interp), axis=0)
    mean_rms_pred = np.mean(np.stack(rms_pred), axis=0)
    mean_rms_scale_sim = np.mean(np.stack(rms_scale_sim), axis=0)
    mean_rms_dns = np.mean(np.stack(rms_dns), axis=0)

    mean_grad_lin_interp = np.mean(np.stack(grad_lin_interp), axis=0)
    mean_grad_pred = np.mean(np.stack(grad_pred), axis=0)
    mean_grad_scale_sim = np.mean(np.stack(grad_scale_sim), axis=0)

    mean_rms_g_lin_interp = np.mean(np.stack(rms_g_lin_interp), axis=0)
    mean_rms_g_pred = np.mean(np.stack(rms_g_pred), axis=0)
    mean_rms_g_scale_sim = np.mean(np.stack(rms_g_scale_sim), axis=0)
    mean_rms_g_dns = np.mean(np.stack(rms_g_dns), axis=0)

    return (
        mean_error_lin_interp,
        mean_error_pred,
        mean_error_scale_sim
    ), (
        mean_rms_lin_interp,
        mean_rms_pred,
        mean_rms_scale_sim,
        mean_rms_dns
    ),    (
        mean_grad_lin_interp,
        mean_grad_pred,
        mean_grad_scale_sim
    ),   (
        mean_rms_g_lin_interp,
        mean_rms_g_pred,
        mean_rms_g_scale_sim,
        mean_rms_g_dns
    )


def get_statistics(
    net,
    test_dns_dir,
    test_les_dir,
    test_les_interpolated_dir,
    ndns_test,
    nles_test,
    tmin,
    tmax,
    prefix
):
    # Error profiles on T
    # RMS profiles \sqrt{T^{'2}}
    # Error on gradients \nabla{T}
    # RMS of gradients \sqrt{\nabla T^{'2}}
    # test_dns_dir = os.path.jo
    # test_les_dir = os.path.jo
    # test_les_interpolated_dir

    err_profile = []
    rms_profile = []
    err_grad = []
    rms_grad = []
    test_dns_dir = sorted(glob(os.path.join(test_dns_dir, "*.RHO")))
    test_les_dir = sorted(glob(os.path.join(test_les_dir, "T_*.npy")))
    test_les_interpolated_dir = sorted(
        glob(
            os.path.join(
                test_les_interpolated_dir, "T_interpolated*.npy"
            )
        )
    )
    for test_dns, test_les, test_les_interp in zip(
        test_dns_dir, test_les_dir, test_les_interpolated_dir
    ):

        e, rms, e_g, rms_g = test_net(
            net,
            test_dns,
            ndns_test,
            test_les_interp,
            ndns_test,
            test_les,
            nles_test,
            tmin,
            tmax,
            prefix,
        )

        err_profile.append(e)
        rms_profile.append(rms)
        err_grad.append(e_g)
        rms_grad.append(rms_g)
    mean_err, mean_rms, mean_err_grad, mean_rms_grad = error_profiles_manager(
        err_profile,
        rms_profile,
        err_grad,
        rms_grad
    )
    savepath = os.path.join(prefix, "mean_figures")
    os.makedirs(savepath, exist_ok=True)

    os.makedirs(os.path.join(savepath, "profiles"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "rms"), exist_ok=True)
    os.makedirs(os.path.join(savepath, "grad"), exist_ok=True)
    coord_dns = load_dns_coord(test_dns)
    normalized_dns_coord = coord_dns[-1]/coord_dns[-1].max()

    save_log_error_profile(
        normalized_dns_coord,
        *mean_err,
        os.path.join(savepath, "profiles")
    )
    save_rms_profile(
        normalized_dns_coord,
        *mean_rms,
        os.path.join(savepath, "rms")
    )
    save_rms_lin_interp_cnn(
        normalized_dns_coord,
        mean_rms[0],
        mean_rms[1],
        os.path.join(savepath, "rms")
    )
    save_grad_profile(
        normalized_dns_coord,
        *mean_err_grad,
        os.path.join(savepath, "grad")
    )
    save_rms_grad(
        normalized_dns_coord,
        *mean_rms_grad,
        os.path.join(savepath, "rms")
    )
    return mean_err, mean_rms, mean_err_grad, mean_rms_grad


def testing(model, dns, les_interpolated, les, ndns, nles, n_test, tmin, tmax):
    mean_err_lin_interp = np.zeros(ndns[-1])
    mean_err_pred = np.zeros(ndns[-1])
    mean_err_scale_sim = np.zeros(ndns[-1])
    mean_rms_lin_interp = np.zeros(ndns[-1])
    mean_rms_pred = np.zeros(ndns[-1])
    mean_rms_scale_sim = np.zeros(ndns[-1])
    mean_rms_dns = np.zeros(ndns[-1])

    mean_grad_lin_interp = np.zeros((3, ndns[-1]))
    mean_grad_pred = np.zeros((3, ndns[-1]))
    mean_grad_scale_sim = np.zeros((3, ndns[-1]))
    mean_rms_grad_profile_lin_interp = np.zeros((3, ndns[-1]))
    mean_rms_grad_profile_pred = np.zeros((3, ndns[-1]))
    mean_rms_grad_profile_scale_sim = np.zeros((3, ndns[-1]))
    mean_rms_grad_dns = np.zeros((3, ndns[-1]))

    for d, l, l_i in zip(dns[-3:], les[-3:], les_interpolated[-3:]):
        DNS, coord = load_dns(d, ndns)
        dns_3 = np.meshgrid(*coord, indexing="ij")
        dns_3 = np.concatenate([ss.ravel()[np.newaxis]
                               for ss in dns_3]).T
        LES, coord_les, steps_les = load_coarse_les(l, coord, nles)
        LES = LES
        LES_interp = load_les(l_i, ndns).unsqueeze(0).unsqueeze(0).to("cuda")

        rms_DNS = compute_rms(DNS)
        grad_DNS = np.gradient(DNS)
        rms_grad_DNS = [compute_rms(gradi) for gradi in grad_DNS]

        T_bar_bar = scale_sim_recon(
            LES, coord_les, steps_les, dns_3, nles, ndns, filter_size=3)
        SCALE_SIM = (2 * LES_interp.cpu().numpy() - T_bar_bar)[0, 0]

        reconstructed = evaluate_on_whole_image(
            LES_interp, model.to("cuda"), crop_size=16, tmin=tmin, tmax=tmax)

        error_profile_lin_interp, \
            rms_profile_lin_interp, \
            gradient_profile_lin_interp, \
            gradient_rms_profile_lin_interp = compute_all_errors(
                DNS,
                LES_interp.cpu().numpy()[0, 0],
                coord
            )
        error_profile_pred, \
            rms_profile_pred, \
            gradient_profile_pred, \
            gradient_rms_profile_pred = compute_all_errors(
                DNS,
                reconstructed,
                coord
            )
        error_profile_scale_sim, \
            rms_profile_scale_sim, \
            gradient_profile_scale_sim, \
            gradient_rms_profile_scale_sim = compute_all_errors(
                DNS,
                SCALE_SIM,
                coord
            )
        mean_err_lin_interp = mean_err_lin_interp + error_profile_lin_interp/n_test
        mean_err_pred = mean_err_pred + error_profile_pred/n_test
        mean_err_scale_sim = mean_err_scale_sim + error_profile_scale_sim/n_test

        mean_rms_lin_interp = mean_rms_lin_interp + rms_profile_lin_interp / n_test
        mean_rms_pred = mean_rms_pred + rms_profile_pred / n_test
        mean_rms_scale_sim = mean_rms_scale_sim + rms_profile_scale_sim / n_test
        mean_rms_dns = rms_DNS

        mean_grad_lin_interp = [
            m + g / n_test for m, g in zip(mean_grad_lin_interp, gradient_profile_lin_interp)]
        mean_grad_pred = [m + g / n_test for m,
                          g in zip(mean_grad_pred, gradient_profile_pred)]
        mean_grad_scale_sim = [
            m + g / n_test for m, g in zip(mean_grad_scale_sim, gradient_profile_scale_sim)]

        mean_rms_grad_profile_lin_interp = [
            m + g/n_test for m, g in zip(mean_rms_grad_profile_lin_interp, gradient_rms_profile_lin_interp)]
        mean_rms_grad_profile_pred = [
            m + g/n_test for m, g in zip(mean_rms_grad_profile_pred, gradient_rms_profile_pred)]
        mean_rms_grad_profile_scale_sim = [
            m + g/n_test for m, g in zip(mean_rms_grad_profile_scale_sim, gradient_rms_profile_scale_sim)]
        mean_rms_grad_dns = [m + g/n_test for m,
                             g in zip(mean_rms_grad_dns, rms_grad_DNS)]


def test(
    model,
    n_test: int = 20,
    device: str = "cpu",
    images_dir: Union[str, List,
                      Path] = "",
    *,
    ndns_test=(384, 384, 266),
    nles_test=(48, 48, 50),
    tmin=586,
    tmax=293,
    re_tau_test=180,
    suffix=None
):

    test_dns_dir = os.path.join(images_dir, "test_net", "test_dns_180")
    test_les_dir = os.path.join(
        images_dir, "test_net", "test_les_interpolated_180")
    test_les_coarse_dir = os.path.join(
        images_dir, "test_net", "test_les_180")

    test_loader = TestLoader(
        test_dns_dir,
        test_les_dir,
        test_les_coarse_dir,
        re_tau=re_tau_test,
        nles=nles_test,
        normalize=True,
        device=device,
        tmin=tmin,
        tmax=tmax
    )

    ndns = ndns_test
    nles = nles_test

    mean_err_lin_interp = np.zeros((n_test, ndns[-1]))
    mean_err_pred = np.zeros((n_test, ndns[-1]))
    mean_err_scale_sim = np.zeros((n_test, ndns[-1]))

    mean_rms_lin_interp = np.zeros((n_test, ndns[-1]))
    mean_rms_pred = np.zeros((n_test, ndns[-1]))
    mean_rms_scale_sim = np.zeros((n_test, ndns[-1]))
    mean_rms_dns = np.zeros((n_test, ndns[-1]))

    mean_grad_lin_interp = np.zeros((n_test, 3, ndns[-1]))
    mean_grad_pred = np.zeros((n_test, 3, ndns[-1]))
    mean_grad_scale_sim = np.zeros((n_test, 3, ndns[-1]))

    mean_rms_grad_profile_lin_interp = np.zeros((n_test, 3, ndns[-1]))
    mean_rms_grad_profile_pred = np.zeros((n_test, 3, ndns[-1]))
    mean_rms_grad_profile_scale_sim = np.zeros((n_test, 3, ndns[-1]))
    mean_rms_grad_dns = np.zeros((n_test, 3, ndns[-1]))

    coord_les = test_loader.coord_les
    steps_les = test_loader.steps_les

    steps_les = [np.linspace(i.min(), i.max(), n, retstep=True)
                 for i, n in zip(test_loader.coord_dns, nles)]
    steps_les = [s[1] for s in steps_les]
    dns_3 = test_loader.coord_3_by_3_dns

    for dns, les, les_coarse in test_loader:
        rms_DNS = compute_rms(dns[0, 0].numpy())
        grad_DNS = np.gradient(dns[0, 0].numpy())
        rms_grad_DNS = [compute_rms(gradi) for gradi in grad_DNS]

        T_bar_bar = scale_sim_recon(les_coarse[0, 0].numpy(
        ), coord_les, steps_les, dns_3, nles, ndns, filter_size=3)
