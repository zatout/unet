from ..utils.laplace import pad_global_field
import numpy as np
import torch


def test_symmetric_field_flipping():
    x, y, z = 10, 10, 10
    bc = {1: "periodic", 2: "periodic", 3: "periodic"}
    pad = 2
    tt = torch.randn(1, 1, x, y, z)
    assert torch.allclose(
        pad_global_field(tt, pad, bc),
        torch.tensor(np.pad(tt[0, 0], pad, "wrap"))
    )


def test_symmetric_field_padding_arbitrary_padding():
    x, y, z = 30, 30, 30
    bc = {1: "periodic", 2: "periodic", 3: "periodic"}
    pad = 20
    tt = torch.randn(1, 1, x, y, z)
    assert torch.allclose(
        pad_global_field(tt, pad, bc),
        torch.tensor(np.pad(tt[0, 0], pad, "wrap"))
    )


def test_asymmetric_field_padding():
    """
    Test of asymmetric field padding to see if
    the results are the same as the ones given for numpy pad
    """
    x, y, z = 10, 10, 10
    bc = {1: "periodic", 2: "dirichlet", 3: "periodic"}
    pad = 2
    tt = torch.randn(1, 1, x, y, z)
    pad_np = ([2, 2], [0, 0], [2, 2])

    assert torch.allclose(
        pad_global_field(tt, pad, bc),
        torch.tensor(np.pad(tt[0, 0], pad_np, "wrap"))
    )


def visual_cosx_cosy_cosz_fixed_space_steps():
    x, sx = np.linspace(0, 1, 100, retstep=True)
    y, sy = np.linspace(0, 1, 100, retstep=True)
    z, sz = np.linspace(0, 1, 100, retstep=True)
    XX, YY, ZZ = np.meshgrid(x, y, z)
    f = np.cos(XX) + np.cos(YY) + np.cos(ZZ)
    filter_size = fs = 5
    fixed_lap = fixed_laplacian(torch.from_numpy(f), sx, sy, sz, filter_size)
    plt.figure()
    ax = plt.subplot(321)
    ax.set_title("x direction")
    plt.imshow(f[0])
    ax = plt.subplot(322)
    plt.imshow(-fixed_lap[0])
    ax.set_title("x direction")

    ax = plt.subplot(323)
    plt.imshow(f[:, 0])
    ax.set_title("y direction")
    ax = plt.subplot(324)
    plt.imshow(-fixed_lap[:, 0])
    ax.set_title("y direction")

    ax = plt.subplot(325)
    plt.imshow(f[..., 0])
    ax.set_title("z direction")
    ax = plt.subplot(326)
    plt.imshow(-fixed_lap[..., 0])
    ax.set_title("z direction")

    plt.show()
