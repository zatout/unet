import torch
from .helper import generate_tensors
from ..utils import flip


def test_hflip():
    rhf = flip.RandomHorizontalFlip(p=.5)
    i1, i2 = generate_tensors((10, 10, 10))
    torch.manual_seed(42)
    o1, o2 = rhf(i1), rhf(i2)
    assert torch.all(o1 == o2)


def test_vflip():
    rhf = flip.RandomVerticalFlip(p=.5)
    i1, i2 = generate_tensors((10, 10, 10))
    torch.manual_seed(42)
    o1, o2 = rhf(i1), rhf(i2)
    assert torch.all(o1 == o2)


def test_dflip():
    rhf = flip.RandomDepthFlip(p=.5)
    i1, i2 = generate_tensors((10, 10, 10))
    torch.manual_seed(42)
    o1, o2 = rhf(i1), rhf(i2)
    assert torch.all(o1 == o2)
