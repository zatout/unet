import torch
# from unet.test.helper import generate_tensors
from .helper import generate_tensors
# import unet
# from ..utils.crop import PairCrop
from ..utils import crop


def test_paircrop():
    crop_size = (4, 4, 4)
    image1, image2 = generate_tensors((10, 10, 10))
    pc = crop.PairCrop(crop_size)

    out1, out2 = pc(image1, image2)
    assert torch.all(out1 == out2)
    print("test passed")
