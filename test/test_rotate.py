from ..utils.rotate import Random90Rotation as RR90
import numpy as np
import torch
from .helper import generate_tensors


class TestRotations:
    @staticmethod
    def test_rotation():
        def comparaison(r_t, r_np):
            return torch.all(
                r_t == torch.from_numpy(r_np.copy())
            )

        x, y, z = 10, 10, 10
        t1, t2 = generate_tensors((x, y, z))

        seeds = torch.arange(0, 100)
        rr = RR90(n_rot_max=3)
        for s_t in seeds:
            torch.manual_seed(s_t)
            out_t = rr(t1)

            out_np = np.rot90(t1, k=rr.n_rot, axes=rr.axes[rr.axis_flip])
            comparaison(out_t, out_np)
            assert comparaison(out_t, out_np)
